package com.wimsaas.wimapp.wimfile.controller;

import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.wimapp.wimfile.dao.repositories.FleFileRepository;
import com.wimsaas.wimapp.wimfile.services.criteria.FleFileCriteria;
import com.wimsaas.wimapp.wimfile.services.dto.FleFileDto;
import com.wimsaas.wimapp.wimfile.services.scheduler.catalog.FleFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FleFileController {

    @Autowired
    private FleFileService fleFileService;

    @Autowired
    private FleFileRepository fleFileRepository;


    @GetMapping(value = "/fleFile/fileCategoryId/{fileCategoryId}")
    public List<FleFileDto> getfleFileListByFileCategoryId(@PathVariable Long fileCategoryId) throws WimBaseException {
        return fleFileService.getfleFileListByFileCategoryId(fileCategoryId);
    }
    @GetMapping(value = "/fleFile/getFileNameByHash")
    public String getfleFileByHash(@RequestParam(name = "hash") String hash) {
        return fleFileRepository.getFleFileByHash(hash);
    }
    @GetMapping(value = "/fleFile")
    public ResponseEntity<?> findFleFileByCriteria(@RequestParam(name = "id", required = false) Long id,
                                                   @RequestParam(name = "name", required = false) String name,
                                                   @RequestParam(name = "size", required = false) Long size,
                                                   @RequestParam(name = "path", required = false) String path,
                                                   @RequestParam(name = "type", required = false) String type,
                                                   @RequestParam(name = "websiteSectionId", required = false) Long websiteSectionId,
                                                   @RequestParam(name = "websiteId", required = false) Long websiteId,
                                                   @RequestParam(name = "storeId", required = false) Long storeId,
                                                   @RequestParam(name = "moduleId", required = false) Long moduleId,
                                                   @RequestParam(name = "structureId", required = false)Long structureId,
                                                   @RequestParam(name = "filecategoryId", required = false) Long filecategoryId) throws Exception {
        FleFileCriteria fleFileCriteria = new FleFileCriteria();
        fleFileCriteria.setId(id);
        fleFileCriteria.setName(name);
        fleFileCriteria.setSize(size);
        fleFileCriteria.setPath(path);
        fleFileCriteria.setType(type);
        fleFileCriteria.setWebsiteSectionId(websiteSectionId);
        fleFileCriteria.setWebsiteId(websiteId);
        fleFileCriteria.setStoreId(storeId);
        fleFileCriteria.setStructureId(structureId);
        fleFileCriteria.setModuleId(moduleId);
        fleFileCriteria.setFileCategoryId(filecategoryId);
        return ResponseEntity.ok(fleFileService.findFleFileByCriteria(fleFileCriteria));
    }
    @PostMapping(value = "/fleFile")
    public ResponseEntity<?> persistFleFile(@RequestParam(value = "file") MultipartFile file,@RequestParam(value = "websiteId") Long websiteId,@RequestParam(value = "websiteSectionId") Long websiteSectionId ) throws Exception {
        return ResponseEntity.ok(fleFileService.persistFleFile(file,websiteId,websiteSectionId));
    }

    @GetMapping(value = "/fleFile/{id}")
    public ResponseEntity<?> findfleFileById(@PathVariable Long id) throws Exception {
        return ResponseEntity.ok(fleFileService.findfleFileById(id));
    }

    @PutMapping(value = "/fleFile/{id}")
    public ResponseEntity<?> updatefleFile(@PathVariable Long id, @RequestBody FleFileDto fleFileDto) throws Exception {
        return ResponseEntity.ok(fleFileService.updateFleFile(id, fleFileDto));
    }

    @DeleteMapping(value = "/fleFile/{id}")
    public ResponseEntity<?> deletefleFile(@PathVariable Long id) throws Exception {
        return ResponseEntity.ok(fleFileService.
                deleteFleFileById(id));
    }

    @Transactional
    @DeleteMapping(value = "/fleFile/deleteByhash")
    public ResponseEntity<?> deletefleFileByHash(@RequestParam(value = "hash") String hash) throws Exception {
        return ResponseEntity.ok(fleFileService.deleteFleFileByHash(hash));
    }
    @GetMapping(value = "/fleFile/getByhash")
    public ResponseEntity<?> findfleFileByHash(@RequestParam(value = "hash") String hash) throws WimBaseException {
        return ResponseEntity.ok(fleFileService.findfleFileByHash(hash));
    }
}
