package com.wimsaas.wimapp.wimfile.controller;

import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.wimapp.wimfile.services.criteria.FleFileCategoryCriteria;
import com.wimsaas.wimapp.wimfile.services.dto.FleFileCategoryDto;
import com.wimsaas.wimapp.wimfile.services.scheduler.catalog.FleFileCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class fleFileCategoryController {

    @Autowired
    private FleFileCategoryService fleFileCategoryService;


    @GetMapping(value = "/fleFileCategory")
    public ResponseEntity<?> findfleFileCategoryByCriteria(@RequestParam(name = "id", required = false) Long id,
                                                     @RequestParam(name = "name", required = false) String name) throws WimBaseException {
        FleFileCategoryCriteria fleFileCategoryCriteria = new FleFileCategoryCriteria();
        fleFileCategoryCriteria.setId(id);
        fleFileCategoryCriteria.setName(name);
        return ResponseEntity.ok(fleFileCategoryService.findfleFileCategoryByCriteria(fleFileCategoryCriteria));
    }

    @PostMapping(value = "/fleFileCategory")
    public ResponseEntity<?> persistfleFileCategory(@RequestBody FleFileCategoryDto fleFileCategoryDto) throws WimBaseException {
        return ResponseEntity.ok(fleFileCategoryService.persistfleFileCategory(fleFileCategoryDto));
    }

    @PutMapping(value = "/fleFileCategory/{id}")
    public ResponseEntity<?> updatefleFileCategory(@PathVariable Long id, @RequestBody FleFileCategoryDto fleFileCategoryDto) throws WimBaseException {
        return ResponseEntity.ok(fleFileCategoryService.updatefleFileCategory(id, fleFileCategoryDto));
    }

    @DeleteMapping(value = "/fleFileCategory/{id}")
    public ResponseEntity<?> deletefleFileCategory(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(fleFileCategoryService.deletefleFileCategoryById(id));
    }

   /* @GetMapping(value="/taskkkk1")
    public ResponseEntity<?> findCategorie() throws WimBaseException {
        return ResponseEntity.ok(categoryService.geAll());
    }*/


}

