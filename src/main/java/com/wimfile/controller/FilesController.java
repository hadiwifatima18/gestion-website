package com.wimsaas.wimapp.wimfile.controller;

import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.wimapp.wimfile.services.scheduler.catalog.FleFileService;
import com.wimsaas.wimapp.wimfile.services.scheduler.catalog.FilesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@RestController
@RequestMapping("api/files")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FilesController {
    @Autowired
    private FilesService filesService;

    @Autowired
    private FleFileService fleFileService;

    @PostMapping("/uploadFile")
    public ResponseEntity<?> uploadFile(@RequestParam(name = "file") MultipartFile file, @RequestParam(name = "BaseFolder") String BaseFolder, @RequestParam(name = "websiteId") Long websiteId, @RequestParam(name = "websiteSectionId") Long websiteSectionId) throws WimBaseException {
        return ResponseEntity.ok(filesService.uploadFile(file, BaseFolder,websiteId,websiteSectionId));
    }
    @GetMapping("/folder")
    public List<String> listeFolder() throws WimBaseException {
        return filesService.listFolderNames();
    }
    @GetMapping("/liste")
    public List<String> listeFile() throws WimBaseException {
        return filesService.listeFile();
    }

    @GetMapping("/download")
    public ResponseEntity<ByteArrayResource> downloadFile(@RequestParam(name = "hash") String hash, @RequestParam(name = "path") String path) throws WimBaseException {
        return filesService.downloadFile(hash, path);
    }

    @Transactional
    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteFile(@RequestParam(name = "fileName") String fileName, @RequestParam(name = "path") String path) throws WimBaseException {
        return ResponseEntity.ok(filesService.deleteFile(fileName, path));
    }


    @PostMapping("/createFolder")
    public ResponseEntity<?> createFolder(@RequestParam(name = "folderName") String folderName) throws WimBaseException {
        return ResponseEntity.ok(filesService.createFolder(folderName));
    }


}
