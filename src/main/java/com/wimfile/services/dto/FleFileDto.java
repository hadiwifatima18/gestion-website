package com.wimsaas.wimapp.wimfile.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FleFileDto {
    private Long id;
    private String name;
    private Long size;
    private String path;
    private String type;
    private String hash;
    private String host;
    private Long fileCategoryId;
    private LocalDateTime date;
    private Long websiteId;
    private Long websiteSectionId;
    private Long storeId;
    private Long moduleId;
    private Long structureId;

    public FleFileDto(Long id, String name, Long size, String path, String type, String hash, String host, LocalDateTime date) {
        this.name = name;
        this.path = path;
        this.size = size;
        this.type = type;
        this.hash = hash;
        this.host = host;
        this.date = date;
    }
}

