package com.wimsaas.wimapp.wimfile.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FleFileCategoryDto {
    private Long id;
    private String name;

    public FleFileCategoryDto(Long id, String name) {
        this.name = name;
    }


}