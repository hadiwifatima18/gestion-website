package com.wimsaas.wimapp.wimfile.services.criteria;

import lombok.Data;

import javax.persistence.Column;
import java.time.LocalDateTime;

@Data
public class FleFileCriteria {

    private Long id;
    private String name;
    private Long size;
    private String path;
    private String type;
    private String hash;
    private String host;
    private Long fileCategoryId;
    private LocalDateTime date;
    private Long websiteId;
    private Long websiteSectionId;
    private Long storeId;
    private Long moduleId;
    private Long structureId;
}

