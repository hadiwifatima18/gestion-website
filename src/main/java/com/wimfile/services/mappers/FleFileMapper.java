package com.wimsaas.wimapp.wimfile.services.mappers;

import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimfile.dao.entities.FleFile;
import com.wimsaas.wimapp.wimfile.services.dto.FleFileDto;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

@Component
public class FleFileMapper extends AbstractDtoMapper<FleFileDto, FleFile> {


    @Override
    public void addConfiguration() {
        TypeMap<FleFile, FleFileDto> toDtoTypeMap = modelMapper.getTypeMap(
                FleFile.class,
                FleFileDto.class);
    }

}

