package com.wimsaas.wimapp.wimfile.services.mappers;

import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimfile.dao.entities.FleFileCategory;
import com.wimsaas.wimapp.wimfile.services.dto.FleFileCategoryDto;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;


@Component
public class FleFileCategoryMapper extends AbstractDtoMapper<FleFileCategoryDto, FleFileCategory> {

    @Override
    public void addConfiguration() {
        TypeMap<FleFileCategory, FleFileCategoryDto> toDtoTypeMap = modelMapper.getTypeMap(
                FleFileCategory.class,
                FleFileCategoryDto.class);
    }


}

