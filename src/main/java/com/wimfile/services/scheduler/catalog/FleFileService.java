package com.wimsaas.wimapp.wimfile.services.scheduler.catalog;

import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.wimapp.wimdata.dao.entities.tsk.TskTask;
import com.wimsaas.wimapp.wimdata.services.criteria.tsk.TskTaskCriteria;
import com.wimsaas.wimapp.wimdata.services.dtos.tsk.TskTaskDto;
import com.wimsaas.wimapp.wimfile.config.properties.space.SpaceProperties;
import com.wimsaas.wimapp.wimfile.dao.entities.FleFile;
import com.wimsaas.wimapp.wimfile.dao.repositories.FleFileRepository;
import com.wimsaas.wimapp.wimfile.services.criteria.FleFileCriteria;
import com.wimsaas.wimapp.wimfile.services.dto.FleFileDto;
import com.wimsaas.wimapp.wimfile.services.mappers.FleFileMapper;
import com.wimsaas.wimapp.wimsite.services.WbsWebsiteService;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsWebsiteCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteDto;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class FleFileService {

    @Autowired
    private FleFileRepository fleFileRepository;
    @Autowired
    private FleFileMapper fleFileMapper;
    @Autowired
    private WbsWebsiteService wbsWebsiteService;
    @Autowired
    private SpaceProperties spaceProperties;
    @Autowired
    private FilesService filesService;


    public List<FleFileDto> getfleFileListByFileCategoryId(Long categoryId) throws WimBaseException {
        try {
            return fleFileRepository.getfleFileListByFileCategoryId(categoryId);
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (getfleFileListByFilecategoryId)  categoryId (" + categoryId + ")", new RuntimeException(e));
        }
    }
    public String getFleFileByHash(String hash) throws WimBaseException {
        try {
            return fleFileRepository.getFleFileByHash(hash);
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (getFleFileByHash)  hash (" + hash + ")", new RuntimeException(e));
        }
    }
    public List<FleFileDto> findFleFileByCriteria(FleFileCriteria fleFileCriteria) throws WimBaseException {
        try {
            List<FleFile> fleFileList = fleFileRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {
                List<Predicate> predicateList = new ArrayList<>();

                if (fleFileCriteria.getId() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("id"), fleFileCriteria.getId()));
                }
                if (fleFileCriteria.getName() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("name"), fleFileCriteria.getName()));
                }
                if (fleFileCriteria.getSize() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("size"), fleFileCriteria.getSize()));
                }
                if (fleFileCriteria.getPath() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("path"), fleFileCriteria.getPath()));
                }
                if (fleFileCriteria.getType() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("type"), fleFileCriteria.getType()));
                }
                if (fleFileCriteria.getHash() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("hash"), fleFileCriteria.getHash()));
                }
                if (fleFileCriteria.getHost() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("host"), fleFileCriteria.getHost()));
                }
                if (fleFileCriteria.getFileCategoryId() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("fileCategoryId"), fleFileCriteria.getFileCategoryId()));
                }
                if (fleFileCriteria.getDate() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("date"), fleFileCriteria.getDate()));
                }
                if (fleFileCriteria.getModuleId() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("moduleId"), fleFileCriteria.getModuleId()));
                }
                if (fleFileCriteria.getStoreId() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("storeId"), fleFileCriteria.getStoreId()));
                }
                if (fleFileCriteria.getStructureId() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("structureId"), fleFileCriteria.getStructureId()));
                }
                if (fleFileCriteria.getWebsiteId() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("websiteId"), fleFileCriteria.getWebsiteId()));
                }
                if (fleFileCriteria.getWebsiteSectionId() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("websiteSectionId"), fleFileCriteria.getWebsiteSectionId()));
                }
                return criteriaBuilder.and(predicateList.toArray(new Predicate[0]));
            });
            List<FleFileDto> fleFileDtoList =fleFileMapper.toDtos(fleFileList);
            return fleFileDtoList;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findFleFileByCriteria)  fleFileCriteria (" + fleFileCriteria + ")", new RuntimeException(e));
        }
    }
    public FleFileDto findfleFileById(Long id) throws WimBaseException {
        try {
            FleFileCriteria fleFileCriteria = new FleFileCriteria();
            fleFileCriteria.setId(id);
            List<FleFileDto> fleFileDtoList = findFleFileByCriteria(fleFileCriteria);
            if (fleFileDtoList != null && !fleFileDtoList.isEmpty()) {
                FleFileDto fleFileDto = fleFileDtoList.get(0);
                // enrechir ici
                return fleFileDto;
            } else {
                throw new FunctionalException("globale.element.notFount.error");
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findDocumentById)  id (" + id + ")", new RuntimeException(e));
        }
    }
    public FleFileDto findfleFileByHash(String hash) throws WimBaseException {
        try {
            FleFileCriteria fleFileCriteria = new FleFileCriteria();
            fleFileCriteria.setHash(hash);
            List<FleFileDto> fleFileDtoList = findFleFileByCriteria(fleFileCriteria);
            if (fleFileDtoList != null && !fleFileDtoList.isEmpty()) {
                FleFileDto fleFileDto = fleFileDtoList.get(0);
                // enrechir ici
                return fleFileDto;
            } else {
                throw new FunctionalException("globale.element.notFount.error");
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findfleFileByHash)  hash (" + hash + ")", new RuntimeException(e));
        }
    }
    public FleFileDto persistFleFile(MultipartFile file,Long websiteId,Long websiteSectionId) throws WimBaseException {
        FleFileDto fleFileDto = new FleFileDto();
        String host = "https://wimfile.ams3.digitaloceanspaces.com";
        Long i = Long.valueOf(1);
        try {
            WbsWebsiteDto wbsWebsiteDto= wbsWebsiteService.findWbsWebsiteById(websiteId);
            fleFileDto.setId(i);
            fleFileDto.setHost(host);
            fleFileDto.setType(file.getContentType());
            fleFileDto.setSize(file.getSize());
            fleFileDto.setName(file.getOriginalFilename());
            fleFileDto.setDate(LocalDateTime.now());
            fleFileDto.setWebsiteSectionId(websiteSectionId);
            fleFileDto.setWebsiteId(websiteId);
            fleFileDto.setStoreId(wbsWebsiteDto.getStoreId());
            fleFileDto.setStructureId(wbsWebsiteDto.getStructureId());
            fleFileDto.setModuleId(wbsWebsiteDto.getModuleId());
            i++;
            return fleFileMapper.toDto(fleFileRepository.save(fleFileMapper.toEntity(fleFileDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (persistFleFile)  fleFileDto (" + fleFileDto + ")", new RuntimeException(e));
        }
    }
   public FleFileDto updateFleFile(Long id, FleFileDto fleFileDto) throws WimBaseException {
        try {
            WbsWebsiteDto wbsWebsiteDto= wbsWebsiteService.findWbsWebsiteById(fleFileDto.getWebsiteId());
            FleFileDto fleFileOldDto = findfleFileById(id);
            fleFileOldDto.setName(fleFileDto.getName());
            fleFileOldDto.setSize(fleFileDto.getSize());
            fleFileOldDto.setPath(fleFileDto.getPath());
            fleFileOldDto.setType(fleFileDto.getType());
            fleFileOldDto.setHash(fleFileDto.getHash());
            fleFileOldDto.setWebsiteId(fleFileDto.getWebsiteId());
            fleFileOldDto.setStoreId(wbsWebsiteDto.getStoreId());
            fleFileOldDto.setModuleId(wbsWebsiteDto.getModuleId());
            fleFileOldDto.setStructureId(wbsWebsiteDto.getStructureId());
            fleFileOldDto.setWebsiteSectionId(fleFileDto.getWebsiteSectionId());
            return fleFileMapper.toDto(fleFileRepository.save(fleFileMapper.toEntity(fleFileOldDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (updateFleFile)  fleFileDto (" + fleFileDto + ")", new RuntimeException(e));
        }
   }
    public ResponseDto deleteFleFileById(Long id) throws WimBaseException {
        try {
            // pour vérifier que l'element exisits et il apartient au module de l'utilisateur connecté

            boolean result = validateBeforeDeleteFleFileById(id);
            if (result == false) {
                throw new FunctionalException("globale.element.used.error");
            }
            fleFileRepository.deleteById(id);
            ResponseDto responseDto = new ResponseDto();
            responseDto.setMessage("globale.element.deleted.message");
            return responseDto;
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (deleteFleFileById)  id (" + id + ")", new RuntimeException(e));
        }
    }
    public boolean validateBeforeDeleteFleFileById(Long id) throws WimBaseException {
        try {
            FleFileDto fleFileDto = findfleFileById(id);
            return fleFileDto != null;

        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (validateBeforeDeleteFleFileById)  id (" + id + ")", new RuntimeException(e));
        }
    }
    public ResponseDto deleteFleFileByHash(String hash) throws WimBaseException {
        try {
            // pour vérifier que l'element exisits et il apartient au module de l'utilisateur connecté

            boolean result = validateBeforeDeleteFleFileByHash(hash);
            if (result == false) {
                throw new FunctionalException("globale.element.used.error");
            }
            fleFileRepository.deleteByHash(hash);
            ResponseDto responseDto = new ResponseDto();
            responseDto.setMessage("globale.element.deleted.message");
            return responseDto;
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (deleteFleFileByHash)  id (" + hash + ")", new RuntimeException(e));
        }
    }
    public boolean validateBeforeDeleteFleFileByHash(String hash) throws WimBaseException {
        try {
            FleFileDto fleFileDto = findfleFileByHash(hash);
            return fleFileDto != null;

        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (validateBeforeDeleteFleFileByHash)  id (" + hash + ")", new RuntimeException(e));
        }
    }
}

