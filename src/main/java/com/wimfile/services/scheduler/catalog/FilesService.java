package com.wimsaas.wimapp.wimfile.services.scheduler.catalog;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.wimapp.wimfile.config.properties.space.SpaceProperties;
import com.wimsaas.wimapp.wimfile.utils.Hashing.FileHasher;
import com.wimsaas.wimapp.wimsite.utils.WimSiteConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
@PropertySource(value = {"classpath:spaces_conf.yml"})
public class FilesService {
    FileHasher fileHasher = new FileHasher();
    @Autowired
    private SpaceProperties spaceProperties;
    @Autowired
    private FleFileService fleFileService;
    @Autowired
    private FleFileCategoryService fleFileCategoryService;
    private AmazonS3 space;

    public AmazonS3 spaceS3() {
        AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(
                new BasicAWSCredentials(spaceProperties.getAccessKey(), spaceProperties.getSecretKey())
        );
        space = AmazonS3ClientBuilder
                .standard()
                .withCredentials(awsCredentialsProvider)
                .withEndpointConfiguration(
                        new AwsClientBuilder.EndpointConfiguration(spaceProperties.getEndpoint(), spaceProperties.getRegion())
                )
                .build();
        return space;
    }

    public ResponseDto uploadFile(MultipartFile file, String defaultBaseFolder,Long websiteId,Long websiteSectionId) throws WimBaseException {
        try {
            String completBaseFolder= WimSiteConstants.SPACE_WIMSTE_PREFIX +"/"+defaultBaseFolder ;
            String baseFolder;
            if (completBaseFolder.equals("0")) {
                baseFolder = "";
            } else {
                baseFolder = completBaseFolder + "/";
            }
            String hash = FileHasher.generateHash(file.getOriginalFilename());
            InputStream input = file.getInputStream();
            ObjectMetadata metadata = new ObjectMetadata();

            PutObjectRequest request = new PutObjectRequest(spaceProperties.getBucketName(), baseFolder + hash, input, metadata).withCannedAcl(CannedAccessControlList.PublicRead);

            spaceS3().putObject(request);
            fleFileService.persistFleFile(file,websiteId,websiteSectionId);
            ResponseDto responseDto = new ResponseDto();
            responseDto.setMessage("file uploaded");
            return responseDto;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (uploadFile)", new RuntimeException(e));
        }
    }
    public List<String> listeFile() throws WimBaseException {
        try {
            ListObjectsV2Result result = spaceS3().listObjectsV2(spaceProperties.getBucketName());
            List<S3ObjectSummary> objects = result.getObjectSummaries();
            return objects.stream()
                    .map(S3ObjectSummary::getKey).collect(Collectors.toList());
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (listeFile) ", new RuntimeException(e));
        }
    }
    public ResponseEntity<ByteArrayResource> downloadFile(String hash, String path) throws WimBaseException {

        try {
            S3Object s30bject = spaceS3().getObject(spaceProperties.getBucketName(), path + hash);
            S3ObjectInputStream inputStream = s30bject.getObjectContent();

            byte[] content = IOUtils.toByteArray(inputStream);
            ByteArrayResource resource = new ByteArrayResource(content);
            String fileName = fleFileService.getFleFileByHash(hash);
            return ResponseEntity
                    .ok()
                    .contentLength(content.length)
                    .header("Content-type", "application/octet-stream")
                    .header("Content-disposition", "attachment; filename=\"" + fileName + "\"")
                    .body(resource);
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (downloadFile) fileName : " + hash, new RuntimeException(e));
        }
    }
    public ResponseDto deleteFile(String fileName, String path) throws WimBaseException {
        try {
            spaceS3().deleteObject(spaceProperties.getBucketName(), path + fileName);
            fleFileService.deleteFleFileByHash(fileName);
            ResponseDto responseDto = new ResponseDto();
            responseDto.setMessage("file deleted");
            return responseDto;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (deleteFile)", new RuntimeException(e));
        }
    }
    public ResponseDto createFolder(String folderName) throws WimBaseException {
        try {
            String content = "";
            String fullPath = WimSiteConstants.SPACE_WIMSTE_PREFIX + "/" + folderName + "/";
            InputStream inputStream = new ByteArrayInputStream(content.getBytes());
            ObjectMetadata metadata = new ObjectMetadata();
            PutObjectRequest request = new PutObjectRequest(spaceProperties.getBucketName(), fullPath, inputStream, metadata).withCannedAcl(CannedAccessControlList.PublicRead);
            spaceS3().putObject(request);
            ResponseDto responseDto = new ResponseDto();
            responseDto.setMessage("folder created");
            return responseDto;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (createFolder)", new RuntimeException(e));
        }
    }
    public List<String> listFolderNames() throws WimBaseException {
        try {
            ListObjectsV2Result result = spaceS3().listObjectsV2(spaceProperties.getBucketName());
            List<S3ObjectSummary> objectSummaries = result.getObjectSummaries();

            List<String> folderNames = new ArrayList<>();
            for (S3ObjectSummary objectSummary : objectSummaries) {
                String key = objectSummary.getKey();
                if (key.endsWith("/")) {
                    String folderName = key.substring(0, key.length() - 1);
                    folderNames.add(folderName);
                }
            }
            return folderNames;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (listFolderNames) ", new RuntimeException(e));
        }
    }
}

