package com.wimsaas.wimapp.wimfile.services.scheduler.catalog;

import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.wimapp.wimfile.dao.entities.FleFileCategory;
import com.wimsaas.wimapp.wimfile.dao.repositories.FleFileCategoryRepository;
import com.wimsaas.wimapp.wimfile.services.criteria.FleFileCategoryCriteria;
import com.wimsaas.wimapp.wimfile.services.dto.FleFileCategoryDto;
import com.wimsaas.wimapp.wimfile.services.mappers.FleFileCategoryMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class FleFileCategoryService {
    
    @Autowired
    private FleFileCategoryMapper fleFileCategoryMapper;
    @Autowired
    private FleFileCategoryRepository fleFileCategoryRepository;

    @Autowired
    private FilesService filesService;

    public List<FleFileCategoryDto> findfleFileCategoryByCriteria(FleFileCategoryCriteria fleFileCategoryCriteria) throws WimBaseException {
        try {
            List<FleFileCategory> fleFileCategoryList = fleFileCategoryRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {
                List<Predicate> predicateList = new ArrayList<>();

                if (fleFileCategoryCriteria.getId() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("id"), fleFileCategoryCriteria.getId()));
                }
                if (fleFileCategoryCriteria.getName() != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("name"), fleFileCategoryCriteria.getName()));
                }
                return criteriaBuilder.and(predicateList.toArray(new Predicate[0]));
            });
            return fleFileCategoryMapper.toDtos(fleFileCategoryList);
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findfleFileCategoryByCriteria)  fleFileCategoryCriteria (" + fleFileCategoryCriteria + ")", new RuntimeException(e));
        }
    }
    public FleFileCategoryDto findCategorieById(Long id) throws WimBaseException {
        try {
            FleFileCategoryCriteria fleFileCategoryCriteria = new FleFileCategoryCriteria();
            fleFileCategoryCriteria.setId(id);
            List<FleFileCategoryDto> fleFileCategoryDtoList = findfleFileCategoryByCriteria(fleFileCategoryCriteria);
            if (fleFileCategoryDtoList != null && !fleFileCategoryDtoList.isEmpty()) {
                FleFileCategoryDto fleFileCategoryDto = fleFileCategoryDtoList.get(0);
                return fleFileCategoryDto;
            } else {
                throw new FunctionalException("globale.element.notFount.error");
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findCategorieById)  id (" + id + ")", new RuntimeException(e));
        }

    }
    public FleFileCategoryDto persistfleFileCategory(FleFileCategoryDto fleFileCategoryDto) throws WimBaseException {
        try {
            fleFileCategoryDto.setName(fleFileCategoryDto.getName());
            fleFileCategoryDto.setId(fleFileCategoryDto.getId());
            filesService.createFolder(fleFileCategoryDto.getName());
            return fleFileCategoryMapper.toDto(fleFileCategoryRepository.save(fleFileCategoryMapper.toEntity(fleFileCategoryDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (persistfleFileCategory)  categoryDto (" + fleFileCategoryDto + ")", new RuntimeException(e));
        }
    }
    public FleFileCategoryDto updatefleFileCategory(Long id, FleFileCategoryDto fleFileCategoryDto) throws WimBaseException {
        try {
            FleFileCategoryDto fleFileCategoryDto1 = findCategorieById(id);
            fleFileCategoryDto1.setName(fleFileCategoryDto.getName());
            return fleFileCategoryMapper.toDto(fleFileCategoryRepository.save(fleFileCategoryMapper.toEntity(fleFileCategoryDto1)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (updatefleFileCategory)  categoryDto (" + fleFileCategoryDto + ")", new RuntimeException(e));
        }
    }

    public ResponseDto deletefleFileCategoryById(Long id) throws WimBaseException {
        try {
            // pour vérifier que l'element exisits et il apartient au module de l'utilisateur connecté

            boolean result = validateBeforeDeletefleFileById(id);
            if (result == false) {
                throw new FunctionalException("globale.element.used.error");
            }
            fleFileCategoryRepository.deleteById(id);
            ResponseDto responseDto = new ResponseDto();
            responseDto.setMessage("globale.element.deleted.message");
            return responseDto;
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (deletefleFileCategoryById)  id (" + id + ")", new RuntimeException(e));
        }
    }

    public boolean validateBeforeDeletefleFileById(Long id) throws WimBaseException {
        try {
            FleFileCategoryDto fleFileCategoryDto = findCategorieById(id);
            return fleFileCategoryDto != null;

        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (validateBeforeDeletefleFileById)  id (" + id + ")", new RuntimeException(e));
        }
    }
}