package com.wimsaas.wimapp.wimfile.dao.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "FLE_FILE", catalog = "wimdrive_file")
public class FleFile implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "SIZE")
    private Long size;
    @Column(name = "PATH")
    private String path;
    @Column(name = "TYPE")
    private String type;
    @Column(name = "HASH")
    private String hash;
    @Column(name = "HOST")
    private String host;
    @Column(name = "DATE")
    private LocalDateTime date;
    @Column(name = "FILE_CATEGORY_ID")
    private Long fileCategoryId;
    @Column(name = " WEBSITE_ID")
    private Long websiteId;
    @Column(name = " WEBSITE_SECTION_ID")
    private Long websiteSectionId;
    @Column(name = "STORE_ID")
    private Long storeId;
    @Column(name = "MODULE_ID")
    private Long moduleId;
    @Column(name = "STRUCTURE_ID")
    private Long structureId;
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "CATEGORIE_CODE", referencedColumnName = "CODE", insertable = false, updatable = false)
//    @NotFound(action = NotFoundAction.IGNORE)
//    private Category categorie;


}

