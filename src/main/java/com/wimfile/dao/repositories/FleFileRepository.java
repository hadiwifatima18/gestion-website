package com.wimsaas.wimapp.wimfile.dao.repositories;

import com.wimsaas.wimapp.wimfile.dao.entities.FleFile;
import com.wimsaas.wimapp.wimfile.services.dto.FleFileDto;
import org.bson.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;


@Repository
public interface FleFileRepository extends JpaRepository<FleFile, Long>, JpaSpecificationExecutor<FleFile> {
    @Query(value = " select fleFile.name from FleFile fleFile " +
            " where fleFile.hash = :hash ")
    String getFleFileByHash(String hash);


    int deleteByHash(String hash);


    @Query(value = " select fleFile.size  from FleFile fleFile " +
            " where fleFile.name = :name " +
            "and fleFile.path = :path")
    Long getDocumentSizeByDocumentNameAndDocumentPath(
            @Param("name") String name
            , @Param("path") String path
    );

    @Query(value = " select fleFile.path from FleFile fleFile  " +
            " where fleFile.name = :name")
    String getDocumentPathByDocumentName(
            @Param("name") String name
    );


    //com.wimsaas.wimsaas.wimfile.services.dto.CategoryDto
    @Query(value = " select new com.wimsaas.wimapp.wimfile.services.dto.FleFileDto(" +
            " fleFile.id,fleFile.name,fleFile.size,fleFile.path," +
            " fleFile.type, fleFile.hash,fleFile.host ,fleFile.date) from FleFile fleFile " +
            "WHERE (:id IS NULL OR fleFile.id = :id )" +
            "and (:name IS NULL OR fleFile.name = :name ) " +
            "and (:size IS NULL OR fleFile.size = :size ) " +
            "and (:path IS NULL OR fleFile.path = :path ) " +
            "and (:type IS NULL OR fleFile.type = :type ) " +
            "and (:hash IS NULL OR fleFile.hash = :hash ) " +
            "and (:host IS NULL OR fleFile.host = :host ) "
    )
    List<FleFileDto> getDocumentListByCriteria(
            @Param("id") Long id
            , @Param("name") String name
            , @Param("size") Long size
            , @Param("path") String path
            , @Param("type") String type
            , @Param("hash") String hash
    );
    @Query(value = " select new com.wimsaas.wimapp.wimfile.services.dto.FleFileDto(" +
            " fleFile.id,fleFile.name,fleFile.size,fleFile.path," +
            " fleFile.type, fleFile.hash,fleFile.host,fleFile.date ) from FleFile fleFile " +
            "WHERE fleFile.fileCategoryId = :fileCategoryId"

    )
    List<FleFileDto> getfleFileListByFileCategoryId(
            @Param("fileCategoryId") Long fileCategoryId
    );


}

