package com.wimsaas.wimapp.wimfile.dao.repositories;

import com.wimsaas.wimapp.wimfile.dao.entities.FleFileCategory;
import com.wimsaas.wimapp.wimfile.services.dto.FleFileCategoryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Component
@Repository
public interface FleFileCategoryRepository extends JpaRepository<FleFileCategory, Long>, JpaSpecificationExecutor<FleFileCategory> {

    @Query(value = "select new com.wimsaas.wimapp.wimfile.services.dto.FleFileCategoryDto( " +
            " fleFileCategory.id,fleFileCategory.name ) from FleFileCategory fleFileCategory" +
            " WHERE (:id IS NULL OR fleFileCategory.id = :id ) " +
            " and (:categoryName IS NULL OR fleFileCategory.name = :name ) "

    )
    List<FleFileCategoryDto> findfleFileCategoryByCriteria(
            @Param("id") Long id
            , @Param("name") String name
    );
}

