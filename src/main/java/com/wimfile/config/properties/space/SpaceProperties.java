package com.wimsaas.wimapp.wimfile.config.properties.space;

import com.wimsaas.wimapp.utils.property.YamlPropertySourceFactory;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


@Configuration
@ConfigurationProperties(prefix = "space1.credentials")
@PropertySource(value = "classpath:spaces_conf.yml", factory = YamlPropertySourceFactory.class)
@Getter
@Setter
public class SpaceProperties {
    @Value("${accessKey}")
    private String accessKey;
    @Value("${secretKey}")
    private String secretKey;
    @Value("${endpoint}")
    private String endpoint;
    @Value("${region}")
    private String region;
    @Value("${backetName}")
    private String bucketName;

}

