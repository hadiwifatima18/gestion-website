package com.wimsaas.wimapp.wimfile.utils.Hashing;

import com.wimsaas.starter.exceptions.business.InternalErrorException;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class FileHasher {


    public static String generateHash(String fileName) throws InternalErrorException {
        try {

            // Generate the MD5 hash
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] messageDigest = md.digest(fileName.getBytes());
            BigInteger bigInt = new BigInteger(1, messageDigest);
            String hash = bigInt.toString(16);


            // Get the file extension from the fileName
            int dotIndex = fileName.lastIndexOf(".");
            String extension = "";
            if (dotIndex >= 0) {
                extension = fileName.substring(dotIndex);
            }

            String hashedFileName = hash + extension;

            return hashedFileName;
        } catch (NoSuchAlgorithmException e) {
            throw new InternalErrorException("Error while executing this method (generateHsh) ", new RuntimeException(e));

        }
    }
}
