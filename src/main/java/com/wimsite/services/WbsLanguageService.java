package com.wimsaas.wimapp.wimsite.services;

import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.starter.messages.MessageComponent;
import com.wimsaas.wimapp.wimnotify.services.SequenceGeneratorService;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsLanguage;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsLanguageRepository;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsLanguageCriteria;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsWebsiteSectionCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.WbsLanguageDto;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteSectionDto;
import com.wimsaas.wimapp.wimsite.services.mappers.WbsLanguageMapper;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WbsLanguageService {
    @Autowired
    private WbsLanguageRepository wbsLanguageRepository;
    @Autowired
    private WbsLanguageMapper wbsLanguageMapper;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    private MessageComponent messageComponent;

    public WbsLanguageDto findWbsLanguageByCode(String code) throws WimBaseException {
        try {
            List<WbsLanguageDto> wbsLanguageDtos = findWbsLanguageByCriteria(WbsLanguageCriteria.builder()
                    .code(code)
                    .build());
            if (wbsLanguageDtos != null && !wbsLanguageDtos.isEmpty()) {
                WbsLanguageDto wbsLanguageDto = wbsLanguageDtos.get(0);
                return wbsLanguageDto;
            } else {
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsLanguageByCode)  id (" + code + ")", new RuntimeException(e));
        }
    }

    public List<WbsLanguageDto> findWbsLanguageByCriteria(WbsLanguageCriteria wbsLanguageCriteria) throws WimBaseException {
        List<Document> criterias = new ArrayList<Document>();
        if (wbsLanguageCriteria.getName() != null) {
            criterias.add(new Document("name", wbsLanguageCriteria.getName()));
        }
        if (wbsLanguageCriteria.getCode() != null) {
            criterias.add(new Document("code", wbsLanguageCriteria.getCode()));
        }
        if (criterias.isEmpty()) {
            criterias.add(new Document());
        }
        return wbsLanguageMapper.toDtos(wbsLanguageRepository.findByQuery(new Document("$and", criterias)));
    }

    public WbsLanguageDto persisteWbsLanguage(WbsLanguageDto wbsLanguageDto) {
        return wbsLanguageMapper.toDto(wbsLanguageRepository.save(wbsLanguageMapper.toEntity(wbsLanguageDto)));
    }

    public WbsLanguageDto updateWbsLanguage(String code, WbsLanguageDto wbsLanguageDto) throws WimBaseException {
        try {
            WbsLanguageDto oldwbsLanguageDto = findWbsLanguageByCode(code);
            wbsLanguageDto.setCode(oldwbsLanguageDto.getCode());
            return wbsLanguageMapper.toDto(wbsLanguageRepository.save(wbsLanguageMapper.toEntity(wbsLanguageDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing updateWbsLanguage for id (" + code + ")", new RuntimeException(e));
        }
    }
    public ResponseDto deleteWbsLanguage(String code) throws WimBaseException {
        try {
            List<WbsLanguageDto> wbsLanguageDto = findWbsLanguageByCriteria(WbsLanguageCriteria.builder()
                    .code(code)
                    .build());
            if (wbsLanguageDto != null && !wbsLanguageDto.isEmpty()) {
                WbsLanguageDto wbsLanguageDtos = wbsLanguageDto.get(0);
                WbsLanguage newLanguage =new WbsLanguage();
                newLanguage.setCode(wbsLanguageDtos.getCode());
                newLanguage.setName(wbsLanguageDtos.getName());
                wbsLanguageRepository.delete(newLanguage);
                ResponseDto responseDto = new ResponseDto();
                responseDto.setMessage(messageComponent.get("globale.element.deleted.message"));
                return responseDto;
             }
            else {
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing deleteWbsLanguage for id (" + code + ")", new RuntimeException(e));
        }

    }
}
