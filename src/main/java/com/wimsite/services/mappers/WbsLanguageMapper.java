package com.wimsaas.wimapp.wimsite.services.mappers;

import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsLanguage;
import com.wimsaas.wimapp.wimsite.services.dto.WbsLanguageDto;
import org.springframework.stereotype.Component;

@Component
public class WbsLanguageMapper extends AbstractDtoMapper<WbsLanguageDto, WbsLanguage> {
}
