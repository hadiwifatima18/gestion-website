package com.wimsaas.wimapp.wimsite.services.mappers;

import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsLayout;
import com.wimsaas.wimapp.wimsite.services.dto.WbsLayoutDto;
import org.springframework.stereotype.Component;

@Component
public class WbsLayoutMapper extends AbstractDtoMapper<WbsLayoutDto, WbsLayout> {
}
