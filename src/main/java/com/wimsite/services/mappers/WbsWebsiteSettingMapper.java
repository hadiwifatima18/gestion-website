package com.wimsaas.wimapp.wimsite.services.mappers;

import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteSetting;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteSettingDto;
import org.springframework.stereotype.Component;

@Component
public class WbsWebsiteSettingMapper extends AbstractDtoMapper<WbsWebsiteSettingDto, WbsWebsiteSetting> {
}
