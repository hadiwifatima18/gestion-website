package com.wimsaas.wimapp.wimsite.services.mappers;

import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsFont;
import com.wimsaas.wimapp.wimsite.services.dto.WbsFontDto;
import org.springframework.stereotype.Component;

@Component
public class WbsFontMapper extends AbstractDtoMapper<WbsFontDto, WbsFont> {
}
