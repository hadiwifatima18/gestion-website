package com.wimsaas.wimapp.wimsite.services.mappers;
import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsSettingCategory;
import com.wimsaas.wimapp.wimsite.services.dto.WbsSettingCategoryDto;
import org.springframework.stereotype.Component;

@Component
public class WbsSettingCategoryMapper extends AbstractDtoMapper<WbsSettingCategoryDto, WbsSettingCategory>{
}
