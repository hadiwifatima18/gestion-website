package com.wimsaas.wimapp.wimsite.services.mappers;

import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsSetting;
import com.wimsaas.wimapp.wimsite.services.dto.WbsSettingDto;
import org.springframework.stereotype.Component;

@Component
public class WbsSettingMapper extends AbstractDtoMapper<WbsSettingDto, WbsSetting> {
}
