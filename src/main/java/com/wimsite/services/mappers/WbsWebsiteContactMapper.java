package com.wimsaas.wimapp.wimsite.services.mappers;

import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteContact;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteContactDto;
import org.springframework.stereotype.Component;

@Component
public class WbsWebsiteContactMapper extends AbstractDtoMapper<WbsWebsiteContactDto, WbsWebsiteContact> {
}
