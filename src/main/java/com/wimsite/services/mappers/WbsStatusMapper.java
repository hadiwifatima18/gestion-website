package com.wimsaas.wimapp.wimsite.services.mappers;

import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsStatus;
import com.wimsaas.wimapp.wimsite.services.dto.WbsStatusDto;
import org.springframework.stereotype.Component;

@Component
public class WbsStatusMapper extends AbstractDtoMapper<WbsStatusDto, WbsStatus> {
}
