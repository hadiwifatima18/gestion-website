package com.wimsaas.wimapp.wimsite.services.mappers;

import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteMenu;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteMenuDto;
import org.springframework.stereotype.Component;



@Component
public class WbsWebsiteMenuMapper extends AbstractDtoMapper<WbsWebsiteMenuDto, WbsWebsiteMenu> {
}

