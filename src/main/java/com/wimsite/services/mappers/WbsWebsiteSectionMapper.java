package com.wimsaas.wimapp.wimsite.services.mappers;

import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteSection;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteSectionDto;
import org.springframework.stereotype.Component;

@Component
public class WbsWebsiteSectionMapper extends AbstractDtoMapper<WbsWebsiteSectionDto, WbsWebsiteSection> {


}
