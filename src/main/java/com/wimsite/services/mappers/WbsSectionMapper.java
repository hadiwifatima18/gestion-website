package com.wimsaas.wimapp.wimsite.services.mappers;

import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsSection;
import com.wimsaas.wimapp.wimsite.services.dto.WbsSectionDto;
import org.springframework.stereotype.Component;


@Component
public class WbsSectionMapper extends AbstractDtoMapper<WbsSectionDto, WbsSection> {
}
