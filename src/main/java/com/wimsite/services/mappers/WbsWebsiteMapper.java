package com.wimsaas.wimapp.wimsite.services.mappers;

import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsite;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteDto;
import org.springframework.stereotype.Component;

@Component
public class WbsWebsiteMapper extends AbstractDtoMapper<WbsWebsiteDto, WbsWebsite> {
}
