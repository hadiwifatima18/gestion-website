package com.wimsaas.wimapp.wimsite.services.mappers;

import com.wimsaas.starter.mappers.AbstractDtoMapper;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsitePage;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsitePageDto;
import org.springframework.stereotype.Component;

@Component
public class WbsWebsitePageMapper extends AbstractDtoMapper<WbsWebsitePageDto, WbsWebsitePage> {
}





