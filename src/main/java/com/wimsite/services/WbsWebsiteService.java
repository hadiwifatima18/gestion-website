package com.wimsaas.wimapp.wimsite.services;

import com.amazonaws.services.s3.model.*;
import com.wimsaas.security.components.JwtTokenComponent;
import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.starter.messages.MessageComponent;
import com.wimsaas.wimapp.wimfile.config.properties.space.SpaceProperties;
import com.wimsaas.wimapp.wimfile.services.scheduler.catalog.FleFileService;
import com.wimsaas.wimapp.wimfile.services.scheduler.catalog.FilesService;
import com.wimsaas.wimapp.wimnotify.services.SequenceGeneratorService;
import com.wimsaas.wimapp.wimnotify.services.dto.NtfMessageDto;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsite;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteContact;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteMenu;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsWebsiteContactRepository;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsWebsiteMenuRepository;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsWebsiteRepository;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsWebsiteSectionRepository;
import com.wimsaas.wimapp.wimsite.services.component.SiteHashComponent;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsWebsiteCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.*;
import com.wimsaas.wimapp.wimsite.services.mappers.WbsWebsiteMapper;
import com.wimsaas.wimapp.wimsite.services.mappers.WbsWebsiteSectionMapper;
import com.wimsaas.wimapp.wimsite.utils.WimSiteConstants;
import org.springframework.beans.factory.annotation.Value;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import static com.wimsaas.wimapp.wimsite.utils.WimSiteValidator.isValidDomainName;
import static com.wimsaas.wimapp.wimsite.utils.WimSiteValidator.isValidSubDomainName;

@Service
public class WbsWebsiteService {

    @Autowired
    private WbsWebsiteRepository wbsWebsiteRepository;
    @Autowired
    private WbsWebsiteMapper wbsWebsiteMapper;
    @Autowired
    private WbsLanguageService wbsLanguageService;
    @Autowired
    private WbsWebsiteContactRepository wbsWebsiteContactRepository ;
    @Autowired
    private SpaceProperties spaceProperties;
    @Autowired
    private JwtTokenComponent jwtTokenComponent;
    @Autowired
    private WbsFontService wbsFontService;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    private WbsStatusService wbsStatusService;
    @Autowired
    private WbsWebsiteMenuRepository wbsWebsiteMenuRepository;
    @Autowired
    private WbsWebsiteSectionMapper wbsWebsiteSectionMapper;
    @Autowired
    private WbsWebsiteSectionRepository wbsWebsiteSectionRepository;
    @Autowired
    private WbsWebsiteSectionService WbsWebsiteSectionService;
    @Autowired
    private FilesService filesService;
    @Autowired
    private FleFileService fleFileService;
    @Autowired
    private MessageComponent messageComponent;
    @Autowired
    private SiteHashComponent siteHashComponent;
    @Autowired
    private SettingConfigService settingConfigService;

    private static final Logger logger = LoggerFactory.getLogger(WbsWebsiteService.class);

    public List<WbsWebsiteDto> findWbsWebsiteByCriteria(WbsWebsiteCriteria wbsWebsiteCriteria) throws WimBaseException {
        List<Document> criterias = new ArrayList<Document>();
        if (wbsWebsiteCriteria.getId() != null) {
            criterias.add(new Document("id", wbsWebsiteCriteria.getId()));
        }
        if (wbsWebsiteCriteria.getDomainType() != null) {
            criterias.add(new Document("domain_type", wbsWebsiteCriteria.getDomainType()));
        }
        if (wbsWebsiteCriteria.getDomainName() != null) {
            criterias.add(new Document("domain_name", wbsWebsiteCriteria.getDomainName()));
        }
        if (wbsWebsiteCriteria.getName() != null) {
            criterias.add(new Document("name", wbsWebsiteCriteria.getName()));
        }
        if (wbsWebsiteCriteria.getCodeLanguage() != null) {
            criterias.add(new Document("language_code", wbsWebsiteCriteria.getCodeLanguage()));
        }
        if (wbsWebsiteCriteria.getFontLatinId() != null) {
            criterias.add(new Document("latin_font_id", wbsWebsiteCriteria.getFontLatinId()));
        }
        if (wbsWebsiteCriteria.getFontArabicId() != null) {
            criterias.add(new Document("arabic_font_id", wbsWebsiteCriteria.getFontArabicId()));
        }
        if (wbsWebsiteCriteria.getStatusId() != null) {
            criterias.add(new Document("status_id", wbsWebsiteCriteria.getStatusId()));
        }
        if (wbsWebsiteCriteria.getFolderHash() != null) {
            criterias.add(new Document("folder_hash", wbsWebsiteCriteria.getFolderHash()));
        }
        if (wbsWebsiteCriteria.getHash() != null) {
            criterias.add(new Document("hash", wbsWebsiteCriteria.getHash()));
        }
        if (wbsWebsiteCriteria.getModuleId() != null) {
            criterias.add(new Document("module_id", wbsWebsiteCriteria.getModuleId()));
        }
        if (wbsWebsiteCriteria.getStoreId() != null) {
            criterias.add(new Document("store_id", wbsWebsiteCriteria.getStoreId()));
        }
        if (wbsWebsiteCriteria.getStructureId() != null) {
            criterias.add(new Document("structure_id", wbsWebsiteCriteria.getStructureId()));
        }
        if (criterias.isEmpty()) {
            criterias.add(new Document());
        }
        List<WbsWebsiteDto> wbsWebsiteDtoList = wbsWebsiteMapper.toDtos(wbsWebsiteRepository.findByQuery(new Document("$and", criterias)));
         for (WbsWebsiteDto wbsWebsiteDto : wbsWebsiteDtoList) {
            try {
                if (wbsWebsiteDto.getStatusId() != null) {
                    wbsWebsiteDto.setStatus(wbsStatusService.findWbsStatusById(wbsWebsiteDto.getStatusId()));
                }
                if (wbsWebsiteDto.getLanguageCode() != null) {
                    wbsWebsiteDto.setLanguage(wbsLanguageService.findWbsLanguageByCode(wbsWebsiteDto.getLanguageCode()));
                }
                if (wbsWebsiteDto.getFontArabicId() != null) {
                    wbsWebsiteDto.setArabicFont(wbsFontService.findWbsFontById(wbsWebsiteDto.getFontArabicId()));
                }
                if (wbsWebsiteDto.getFontLatinId() != null) {
                    wbsWebsiteDto.setLatinFont(wbsFontService.findWbsFontById(wbsWebsiteDto.getFontLatinId()));
                }
                wbsWebsiteDto=websiteWarning(wbsWebsiteDto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return wbsWebsiteDtoList;
    }
    public WbsWebsiteDto findWbsWebsiteById(Long id) throws WimBaseException {
        try {
            List<WbsWebsiteDto> wbsWebsiteDtoList = findWbsWebsiteByCriteria(WbsWebsiteCriteria.builder()
                    .id(id)
                    .build());
            if (wbsWebsiteDtoList != null && !wbsWebsiteDtoList.isEmpty()) {
                WbsWebsiteDto wbsWebsiteDto = wbsWebsiteDtoList.get(0);
                return wbsWebsiteDto;
            } else {
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsWebsiteById)  id (" + id + ")", new RuntimeException(e));
        }
    }
    public WbsWebsiteDto findWbsSiteByHash(String hash) throws WimBaseException {
        try {
            List<WbsWebsiteDto> WbsWebsiteDto = findWbsWebsiteByCriteria(WbsWebsiteCriteria.builder()
                    .folderHash(hash)
                    .build());
            if (WbsWebsiteDto != null && !WbsWebsiteDto.isEmpty()) {
                WbsWebsiteDto wbsWebsiteDto = WbsWebsiteDto.get(0);
                return wbsWebsiteDto;
            } else {
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsSiteByHash)  id (" + hash + ")", new RuntimeException(e));
        }
    }
    public WbsWebsiteDto findWbsSiteByDomain(String domainName) throws WimBaseException {
        try {
            List<WbsWebsiteDto> WbsWebsiteDto = findWbsWebsiteByCriteria(WbsWebsiteCriteria.builder()
                    .domainName(domainName)
                    .build());
            if (WbsWebsiteDto != null && !WbsWebsiteDto.isEmpty()) {
                WbsWebsiteDto wbsWebsiteDto = WbsWebsiteDto.get(0);
                return wbsWebsiteDto;
            } else {
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsSiteByHash)  id (" + domainName + ")", new RuntimeException(e));
        }
    }
    public WbsWebsiteDto persisteWebSite(WbsWebsiteDto wbsWebsiteDto) throws WimBaseException {
        String folderHash=siteHashComponent.generateFolderHash();
        boolean result = validateWbsWebSite(wbsWebsiteDto);
        if (!isDomainNameUnique(wbsWebsiteDto.getDomainName())) {
            throw new FunctionalException("Le nom de domaine déja existe.");
        }
        if (result) {
            try {
                if (wbsWebsiteDto.getDomainType().equals("DOMAIN")) {
                    wbsWebsiteDto.setSubdomainName(null);
                }
                wbsWebsiteDto.setId(Long.valueOf(sequenceGeneratorService.getSequenceNumber(WbsWebsite.SEQUENCE_NAME)));
                wbsWebsiteDto.setStatusId(WimSiteConstants.WBS_STATUS_ACTIVE_ID);
                //wbsWebsiteDto.setModuleId(jwtTokenComponent.getModulelId());
                wbsWebsiteDto.setUserCreation(String.valueOf(jwtTokenComponent.getId()));
                wbsWebsiteDto.setDateCreation(LocalDateTime.now());
                wbsWebsiteDto.setHash(generateHashForWbsWebsite(wbsWebsiteDto));
                wbsWebsiteDto.setFolderHash(folderHash);
                filesService.createFolder(folderHash);
                WbsWebsiteDto websiteDto=wbsWebsiteMapper.toDto(wbsWebsiteRepository.save(wbsWebsiteMapper.toEntity(wbsWebsiteDto)));
                if(wbsWebsiteDto.getByJsonFile()!=null && wbsWebsiteDto.getByJsonFile()==true){
                    settingConfigService.importWebsiteFromJsonFile();
                }
                if(wbsWebsiteDto.getByJsonFile()!= null && wbsWebsiteDto.getByJsonFile()==false){
                    settingConfigService.importWebsiteFromOtherWebsite(wbsWebsiteDto.getInitByWebsiteId());
                }
                return websiteDto;
            } catch (Exception e) {
                throw new InternalErrorException("Erreur lors de l'exécution de cette méthode (persisteWbsWebsite)", new RuntimeException(e));
            }
        } else {
            throw new FunctionalException("La validation de site Web a échoué.");
        }
    }

    public String generateHashForWbsWebsite(WbsWebsiteDto wbsWebsiteDto) throws WimBaseException {
        String input = wbsWebsiteDto.getDomainName() + wbsWebsiteDto.getDomainType() + wbsWebsiteDto.getName() + wbsWebsiteDto.getId();
        return siteHashComponent.generateHash(input);
    }

    public WbsWebsiteDto updateWebSite(Long id, WbsWebsiteDto wbsWebsiteDto) throws WimBaseException {
        WbsWebsiteDto oldWbsWebsiteDto = findWbsWebsiteById(id);
        if (wbsWebsiteDto.getDomainName().equals(oldWbsWebsiteDto.getDomainName())) {
            wbsWebsiteDto.setId(oldWbsWebsiteDto.getId());
            boolean result = validateWbsWebSite(wbsWebsiteDto);
            if (result) {
                try {
                    if (wbsWebsiteDto.getDomainType().equals("DOMAIN")) {
                        wbsWebsiteDto.setSubdomainName("null");
                    }
                    wbsWebsiteDto.setStatusId(oldWbsWebsiteDto.getStatusId());
                    wbsWebsiteDto.setLogo(oldWbsWebsiteDto.getLogo());
                    wbsWebsiteDto.setFavicon(oldWbsWebsiteDto.getFavicon());
                    //wbsWebsiteDto.setModuleId(oldWbsWebsiteDto.getModuleId());
                    wbsWebsiteDto.setUserCreation(oldWbsWebsiteDto.getUserCreation());
                    wbsWebsiteDto.setDateCreation(oldWbsWebsiteDto.getDateCreation());
                    wbsWebsiteDto.setUserUpdate(String.valueOf(jwtTokenComponent.getId()));
                    wbsWebsiteDto.setDateUpdate(LocalDateTime.now());
                    return wbsWebsiteMapper.toDto(wbsWebsiteRepository.save(wbsWebsiteMapper.toEntity(wbsWebsiteDto)));
                } catch (Exception e) {
                    throw new InternalErrorException("Error while executing updateWebsite for id (" + id + ")", new RuntimeException(e));
                }
            } else {
                throw new FunctionalException("La validation de la modification du website a échoué.");
            }
        } else {
            boolean isUnique = isDomainNameUnique(wbsWebsiteDto.getDomainName());
            if (isUnique) {
                wbsWebsiteDto.setId(oldWbsWebsiteDto.getId());
                wbsWebsiteDto.setStatusId(oldWbsWebsiteDto.getStatusId());
                wbsWebsiteDto.setLogo(oldWbsWebsiteDto.getLogo());
                wbsWebsiteDto.setFavicon(oldWbsWebsiteDto.getFavicon());
                //wbsWebsiteDto.setModuleId(oldWbsWebsiteDto.getModuleId());
                wbsWebsiteDto.setUserCreation(oldWbsWebsiteDto.getUserCreation());
                wbsWebsiteDto.setDateCreation(oldWbsWebsiteDto.getDateCreation());
                wbsWebsiteDto.setUserUpdate(String.valueOf(jwtTokenComponent.getId()));
                wbsWebsiteDto.setDateUpdate(LocalDateTime.now());
                boolean result = validateWbsWebSite(wbsWebsiteDto);
                if (result) {
                    try {
                        return wbsWebsiteMapper.toDto(wbsWebsiteRepository.save(wbsWebsiteMapper.toEntity(wbsWebsiteDto)));
                    } catch (Exception e) {
                        throw new InternalErrorException("Error while executing updateWebsite for id (" + id + ")", new RuntimeException(e));
                    }
                } else {
                    throw new FunctionalException("La validation de la modification du website a échoué.");
                }
            } else {
                throw new FunctionalException("Le nom de domaine déja existe.");
            }
        }
    }

    public ResponseDto deleteWebSite(Long id) throws WimBaseException {
        try {
            WbsWebsiteDto wbsWebsiteDto = findWbsWebsiteById(Long.valueOf(id));
            wbsWebsiteDto.setStatusId(WimSiteConstants.WBS_STATUS_SUPPRIMEE_ID);
            wbsWebsiteMapper.toDto(wbsWebsiteRepository.save(wbsWebsiteMapper.toEntity(wbsWebsiteDto)));
            ResponseDto responseDto = new ResponseDto();
            responseDto.setMessage(messageComponent.get("globale.element.deleted.message"));
            return responseDto;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing deleteWebSite for id (" + id + ")", new RuntimeException(e));
        }
    }
    public WbsWebsiteDto updateDefaultLanguage(Long id, WbsLanguageDto wbsLanguageDto) throws WimBaseException {
        Optional<WbsWebsite> optionalWebsite = wbsWebsiteRepository.findById(id);
        if (optionalWebsite.isPresent()) {
            WbsWebsite website = optionalWebsite.get();
            website.setLanguageCode(wbsLanguageDto.getCode());
            wbsWebsiteRepository.save(website);
            return wbsWebsiteMapper.toDto(website);
        }
        throw new RuntimeException("Website not found with id: " + id);
    }
    public WbsWebsiteDto updateDefaultLatinFont(Long websiteId, Long fontLatinId) throws WimBaseException {
        Optional<WbsWebsite> optionalWebsite = wbsWebsiteRepository.findById(websiteId);
        if (optionalWebsite.isPresent()) {
            WbsWebsite website = optionalWebsite.get();
            website.setFontLatinId(fontLatinId);
            wbsWebsiteRepository.save(website);
            return wbsWebsiteMapper.toDto(website);
        }
        throw new InternalErrorException("Error while executing updateLatinFont for id (" + websiteId + ")");
    }
    public WbsWebsiteDto updateDefaultArabicFont(Long websiteId, Long fontArabicId) throws WimBaseException {
        Optional<WbsWebsite> optionalWebsite = wbsWebsiteRepository.findById(websiteId);
        if (optionalWebsite.isPresent()) {
            WbsWebsite website = optionalWebsite.get();
            website.setFontArabicId(fontArabicId);
            wbsWebsiteRepository.save(website);
            return wbsWebsiteMapper.toDto(website);
        }
        throw new InternalErrorException("Error while executing updateArabicFont for id (" + websiteId + ")");
    }
    public WbsWebsiteDto updateStatus(Long websiteId, Long statusId) throws WimBaseException {
        Optional<WbsWebsite> optionalWebsite = wbsWebsiteRepository.findById(websiteId);
        if (optionalWebsite.isPresent()) {
            WbsWebsite website = optionalWebsite.get();
            website.setStatusId(statusId);
            wbsWebsiteRepository.save(website);
            return wbsWebsiteMapper.toDto(website);
        }
        throw new InternalErrorException("Error while executing updateStatus for id (" + websiteId + ")");
    }
    public boolean validateWbsWebSite(WbsWebsiteDto wbsWebsiteDto) throws WimBaseException {
        String domainName = wbsWebsiteDto.getDomainName();
        String subDomainName = wbsWebsiteDto.getSubdomainName();
        String domainType = wbsWebsiteDto.getDomainType();
        String languageCode = wbsWebsiteDto.getLanguageCode();
        Long latinFontId = wbsWebsiteDto.getFontLatinId();
        Long arabicFontId = wbsWebsiteDto.getFontArabicId();
        Long statusId = wbsWebsiteDto.getStatusId();
        boolean validSubDomainName = isValidSubDomainName(subDomainName);
        boolean validDomainName = isValidDomainName(domainName);
        boolean validDomainType = false;
        boolean languageCodeExists = false;
        boolean latinFontExists = false;
        boolean arabicFontExists = false;
        boolean statusExists = false;

        if (!validDomainName) {
            throw new FunctionalException("Le nom de domaine est invalide");
        }
        if (!validSubDomainName) {
            throw new FunctionalException("Le nom de subdomaine est invalide :il doit contenir que des lettres et chiffres ");
        }

        if (domainType != null && (domainType.equals("DOMAIN") || domainType.equals("SUBDOMAIN"))) {
            validDomainType = true;
        } else {
            throw new FunctionalException(" Le type de domaine est invalide");
        }
        /*if(wbsWebsiteDto.getInitByJsonFile()==null || wbsWebsiteDto.getInitByWebsiteId()==null){
            throw new FunctionalException("le initByJsonFile ou initByWebsiteId manquant");
        }*/
        try {
            WbsLanguageDto wbsLanguageDto = wbsLanguageService.findWbsLanguageByCode(languageCode);
            languageCodeExists = wbsLanguageDto != null;
        } catch (FunctionalException e) {
            if (!languageCodeExists)
                throw new FunctionalException(" language n'existe pas.");
        }
        try {
            WbsFontDto wbsFontDto = wbsFontService.findWbsFontById(latinFontId);
            latinFontExists = wbsFontDto != null;
        } catch (FunctionalException e) {
            if (!latinFontExists)
                throw new FunctionalException(" Le font latin n'existe pas.");
        }
        try {
            WbsFontDto wbsFontDto = wbsFontService.findWbsFontById(arabicFontId);
            arabicFontExists = wbsFontDto != null;
        } catch (FunctionalException e) {
            if (!arabicFontExists)
                throw new FunctionalException(" Le font arabic n'existe pas.");
        }
        try {
            WbsStatusDto wbsStatusDto = wbsStatusService.findWbsStatusById(statusId);
            statusExists = wbsStatusDto != null;
        } catch (FunctionalException e) {
            if (!statusExists)
                throw new FunctionalException(" Le status n'existe pas.");
        }
        if (statusExists && arabicFontExists && latinFontExists && languageCodeExists && validDomainType && validDomainName)
            return true;
        return false;
    }
    public boolean isDomainNameUnique(String domainName) throws WimBaseException {
        List<WbsWebsiteDto> websiteDtoList = findWbsWebsiteByCriteria(WbsWebsiteCriteria.builder().build());
        for (WbsWebsiteDto websiteDto : websiteDtoList) {
            if (websiteDto != null) {
                Long statusId = websiteDto.getStatusId();
                if (statusId != null && !statusId.equals(WimSiteConstants.WBS_STATUS_SUPPRIMEE_ID)) {
                    String websiteDomainName = websiteDto.getDomainName();
                    if (websiteDomainName.equals(domainName)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    public WbsWebsiteDto websiteWarning(WbsWebsiteDto websiteDto) throws WimBaseException {
        List<WbsWebsiteMenu> websiteMenuList = wbsWebsiteMenuRepository.findByWebsiteId(websiteDto.getId());
        List<WbsWebsiteContact> websiteContactList=wbsWebsiteContactRepository.findByWebsiteId(websiteDto.getId());

        if (websiteMenuList == null || websiteMenuList.isEmpty()) {
            websiteDto.setWarning("Veuillez ajouter un menu à votre site web.");
        }else if (websiteContactList == null || websiteContactList.isEmpty()) {
            websiteDto.setWarning("Veuillez ajouter un contact à votre site web.");
        }else {
            websiteDto.setWarning(null);
        }
        return websiteDto;
    }
    public ResponseDto uploadImage(MultipartFile image, Long websiteId,Long websiteSectionId) throws WimBaseException {
        try {
                WbsWebsiteDto websiteDto=findWbsWebsiteById(websiteId);
                String defaultBaseFolder=websiteDto.getFolderHash();
                String completBaseFolder= WimSiteConstants.SPACE_WIMSTE_PREFIX +"/"+defaultBaseFolder ;
                String baseFolder;
                if (completBaseFolder.equals("0")) {
                    baseFolder = "";
                } else {
                    baseFolder = completBaseFolder + "/";
                }
                String imageHash= siteHashComponent.generateImageHash(image.getOriginalFilename());
                String imagePath=baseFolder + imageHash;
                InputStream input = image.getInputStream();
                ObjectMetadata metadata = new ObjectMetadata();
                metadata.setContentType(image.getContentType());
                PutObjectRequest request = new PutObjectRequest(spaceProperties.getBucketName(), imagePath, input, metadata).withCannedAcl(CannedAccessControlList.PublicRead);
                filesService.spaceS3().putObject(request);
                fleFileService.persistFleFile(image,websiteId,websiteSectionId);
                ResponseDto responseDto = new ResponseDto();
                responseDto.setMessage(imagePath);
                return responseDto;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (uploadImage)", new RuntimeException(e));
        }
    }
    public ResponseDto uploadFile(MultipartFile file, Long websiteId,Long websiteSectionId) throws WimBaseException {
        try {
            WbsWebsiteDto websiteDto=findWbsWebsiteById(websiteId);
            String defaultBaseFolder=websiteDto.getFolderHash();
            String completBaseFolder= WimSiteConstants.SPACE_WIMSTE_PREFIX +"/"+defaultBaseFolder ;
            String baseFolder;
            if (completBaseFolder.equals("0")) {
                baseFolder = "";
            } else {
                baseFolder = completBaseFolder + "/";
            }
            String hash=siteHashComponent.generateFileHash(file.getOriginalFilename());
            String filePath = baseFolder + hash;
            InputStream input = file.getInputStream();
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(file.getContentType());
            PutObjectRequest request = new PutObjectRequest(spaceProperties.getBucketName(),filePath , input, metadata).withCannedAcl(CannedAccessControlList.PublicRead);
            filesService.spaceS3().putObject(request);
            fleFileService.persistFleFile(file,websiteId,websiteSectionId);
            ResponseDto responseDto = new ResponseDto();
            responseDto.setMessage(filePath);
            return responseDto;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (uploadFile)", new RuntimeException(e));
        }
    }
    public List<String> listFilesForWebsite(Long websiteId) throws WimBaseException {
        try {
            WbsWebsiteDto websiteDto=findWbsWebsiteById(websiteId);
            String defaultBaseFolder=websiteDto.getFolderHash();
            String completBaseFolder = WimSiteConstants.SPACE_WIMSTE_PREFIX + "/" + defaultBaseFolder;
            String baseFolder;
            if (completBaseFolder.equals("0")) {
                baseFolder = "";
            } else {
                baseFolder = completBaseFolder + "/";
            }
            ListObjectsV2Result result = filesService.spaceS3().listObjectsV2(spaceProperties.getBucketName(), baseFolder);
            List<S3ObjectSummary> objects = result.getObjectSummaries();
            return objects.stream()
                    .map(S3ObjectSummary::getKey)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (listFilesForWebsite) ", new RuntimeException(e));
        }
    }
    public WbsWebsiteDto uploadFavicon(MultipartFile favicon, Long websiteId,Long websiteSectionId) throws WimBaseException {
        try {
            WbsWebsiteDto websiteDto=findWbsWebsiteById(websiteId);
            String defaultBaseFolder=websiteDto.getFolderHash();
            String completBaseFolder= WimSiteConstants.SPACE_WIMSTE_PREFIX +"/"+defaultBaseFolder ;
            System.out.println("defaultBaseFolder"+defaultBaseFolder);
            String baseFolder;
            if (completBaseFolder.equals("0")) {
                baseFolder = "";
            } else {
                baseFolder = completBaseFolder + "/";
            }
            String faviconHash= siteHashComponent.generateImageHash(favicon.getOriginalFilename());
            String faviconPath=baseFolder + faviconHash;
            InputStream input = favicon.getInputStream();
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(favicon.getContentType());
            PutObjectRequest request = new PutObjectRequest(spaceProperties.getBucketName(), faviconPath, input, metadata).withCannedAcl(CannedAccessControlList.PublicRead);
            System.out.println("debut");
            filesService.spaceS3().putObject(request);
            System.out.println("fin");
            fleFileService.persistFleFile(favicon,websiteId,websiteSectionId);
            System.out.println("persist");
            websiteDto.setFavicon(faviconPath);
            wbsWebsiteMapper.toDto(wbsWebsiteRepository.save(wbsWebsiteMapper.toEntity(websiteDto)));
            return wbsWebsiteMapper.toDto(wbsWebsiteRepository.save(wbsWebsiteMapper.toEntity(websiteDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (uploadFavicon)", new RuntimeException(e));
        }
    }
   public WbsWebsiteDto uploadLogo(MultipartFile logo, Long websiteId,Long websiteSectionId) throws WimBaseException {
        try {
            WbsWebsiteDto websiteDto=findWbsWebsiteById(websiteId);
            String defaultBaseFolder=websiteDto.getFolderHash();
            String completBaseFolder= WimSiteConstants.SPACE_WIMSTE_PREFIX +"/"+defaultBaseFolder ;
            String baseFolder;
            if (completBaseFolder.equals("0")) {
                baseFolder = "";
            } else {
                baseFolder = completBaseFolder + "/";
            }
            String logoHash= siteHashComponent.generateImageHash(logo.getOriginalFilename());
            String logoPath=baseFolder + logoHash;
            InputStream input = logo.getInputStream();
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(logo.getContentType());
            PutObjectRequest request = new PutObjectRequest(spaceProperties.getBucketName(), logoPath, input, metadata).withCannedAcl(CannedAccessControlList.PublicRead);
            filesService.spaceS3().putObject(request);
            fleFileService.persistFleFile(logo,websiteId,websiteSectionId);
            websiteDto.setLogo(logoPath);
            wbsWebsiteMapper.toDto(wbsWebsiteRepository.save(wbsWebsiteMapper.toEntity(websiteDto)));
            return wbsWebsiteMapper.toDto(wbsWebsiteRepository.save(wbsWebsiteMapper.toEntity(websiteDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (uploadLogo)", new RuntimeException(e));
        }
   }





}






