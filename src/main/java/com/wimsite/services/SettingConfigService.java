package com.wimsaas.wimapp.wimsite.services;

import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.wimapp.wimnotify.services.SequenceGeneratorService;
import com.wimsaas.wimapp.wimsite.dao.entities.*;
import com.wimsaas.wimapp.wimsite.services.criteria.*;
import com.wimsaas.wimapp.wimsite.services.dto.*;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.util.*;

@Service
public class SettingConfigService {
    @Autowired
    private WbsLayoutService wbsLayoutService;
    @Autowired
    private WbsFontService wbsFontService;
    @Autowired
    private WbsLanguageService wbsLanguageService;
    @Autowired
    private WbsSectionService wbsSectionService;
    @Autowired
    private WbsWebsiteService wbsWebsiteService;
    @Autowired
    private WbsWebsitePageService wbsWebsitePageService;
    @Autowired
    private WbsWebsiteMenuService wbsWebsiteMenuService;
    @Autowired
    private WbsWebsiteSectionService wbsWebsiteSectionService;
    @Autowired
    private WbsWebsiteContactService wbsWebSiteContactService;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    @Value("${init-module.path-data-exemple-website}")
    String dataExempleWebsite;
    @Value("${init-module.path-data-setting}")
    String datasetting;

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public static String getJSONFromFile(String filename) {
        String jsonText = "";
        try {
            BufferedReader bufferedReader =
                    new BufferedReader(new FileReader(filename));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                jsonText += line + "\n";
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonText;
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    /*#######################   initialize setting collection   #######################*/
    public ToResponseDto initializeSetting() throws WimBaseException {
        JSONObject mainJsonObject = null;
        try {
            String fileString = getJSONFromFile(ResourceUtils.getFile(datasetting).getPath());
            JSONParser parser = new JSONParser();
            Object object = parser.parse(fileString);
            mainJsonObject = (JSONObject) object;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (initializeSetting openfile)  ", new RuntimeException(e));
        }

        Set<?> keySet = mainJsonObject.keySet();
        Iterator<?> iterator = keySet.iterator();
        List<String> keyslist = new ArrayList<>();
        do {
            String k = iterator.next().toString();
            keyslist.add(k);
        } while (iterator.hasNext());
        SettingConfigDto settingConfigDto = new SettingConfigDto();
        for (int j = 0; j < mainJsonObject.size(); j++) {
            JSONArray jsonArray = (JSONArray) mainJsonObject.get(keyslist.get(j));
            List<WbsLayoutDto> layout = new ArrayList<>();
            List<WbsFontDto> font = new ArrayList<>();
            List<WbsLanguageDto> language = new ArrayList<>();
            List<WbsSectionDto> section = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                switch (keyslist.get(j)) {
                    case "wbsLayout":
                        Long idLayout = (Long) jsonObject.get("id");
                        String code = (String) jsonObject.get("code");
                        Long sectionId = (Long) jsonObject.get("sectionId");
                        String image = (String) jsonObject.get("image");
                        String data = (String) jsonObject.get("data");
                        WbsLayoutDto wbsLayoutDto = new WbsLayoutDto(idLayout, code, sectionId, image, data, false, new WbsSectionDto(), "{}", "{}");
                        layout.add(wbsLayoutDto);
                        if (i == jsonArray.size() - 1) {
                            settingConfigDto.setWbsLayout(layout);
                        }
                        break;
                    case "wbsFont":
                        Long idFont = (Long) jsonObject.get("id");
                        String url = (String) jsonObject.get("url");
                        Boolean arabic = (Boolean) jsonObject.get("arabic");
                        String name = (String) jsonObject.get("name");
                        String thumbnail = (String) jsonObject.get("thumbnail");
                        WbsFontDto wbsFontDto = new WbsFontDto(idFont, url, name, thumbnail, arabic);
                        font.add(wbsFontDto);
                        if (i == jsonArray.size() - 1) {
                            settingConfigDto.setWbsFont(font);
                        }
                        break;
                    case "wbsLanguage":
                        String codelanguage = (String) jsonObject.get("code");
                        String namelanguage = (String) jsonObject.get("name");
                        String  nameArlanguage = (String) jsonObject.get("nameAr");
                        WbsLanguageDto wbsLanguageDto = new WbsLanguageDto(codelanguage, namelanguage,nameArlanguage);
                        language.add(wbsLanguageDto);
                        if (i == jsonArray.size() - 1) {
                            settingConfigDto.setWbsLanguage(language);
                        }
                        break;
                    case "wbsSection":
                        Long idSection = (Long) jsonObject.get("id");
                        String codeSection = (String) jsonObject.get("code");
                        String nameSection = (String) jsonObject.get("name");
                        String nameAr = (String) jsonObject.get("nameAr");
                        WbsSectionDto wbsSectionDtos = new WbsSectionDto(idSection, codeSection, nameSection, nameAr, 0);
                        section.add(wbsSectionDtos);
                        if (i == jsonArray.size() - 1) {
                            settingConfigDto.setWbsSection(section);
                        }
                        break;
                }
            }
        }
        return persistConfigSettingData(settingConfigDto);
    }

    public ToResponseDto persistConfigSettingData(SettingConfigDto settingConfigDto) throws WimBaseException {
        ToResponseDto toResponseDto = new ToResponseDto(true);
        if (wbsLayoutService.findWbsLayoutByCriteria(WbsLayoutCriteria.builder().build()).size() == 0
                && wbsFontService.findWbsFontByCriteria(WbsFontCriteria.builder().build()).size() == 0
                && wbsLanguageService.findWbsLanguageByCriteria(WbsLanguageCriteria.builder().build()).size() == 0
                && wbsSectionService.findWbsSectionByCriteria(WbsSectionCriteria.builder().build()).size() == 0) {

            List<WbsLayoutDto> wbsLayoutDtoList = settingConfigDto.getWbsLayout();
            if (wbsLayoutDtoList.size() != 0) {
                for (int i = 0; i < wbsLayoutDtoList.size(); i++) {
                    wbsLayoutService.addWbsLayout(wbsLayoutDtoList.get(i));
                }
            }
            List<WbsLanguageDto> wbsLanguageDtoList = settingConfigDto.getWbsLanguage();
            if (wbsLanguageDtoList.size() != 0) {
                for (int i = 0; i < wbsLanguageDtoList.size(); i++) {
                    wbsLanguageService.persisteWbsLanguage(wbsLanguageDtoList.get(i));
                }
            }
            List<WbsFontDto> wbsFontDtoList = settingConfigDto.getWbsFont();
            if (wbsFontDtoList.size() != 0) {
                for (int i = 0; i < wbsFontDtoList.size(); i++) {
                    wbsFontService.persisteWbsFont(wbsFontDtoList.get(i));
                }
            }
            List<WbsSectionDto> wbsSectionDtoList = settingConfigDto.getWbsSection();
            if (wbsSectionDtoList.size() != 0) {
                for (int i = 0; i < wbsSectionDtoList.size(); i++) {
                    wbsSectionService.persisteWbsSection(wbsSectionDtoList.get(i));
                }
            }
            return toResponseDto;
        } else {
            throw new FunctionalException("Settings déjà configuré");
        }
    }

    /*#######################   import website from json file   #######################*/
    public ToResponseDto importWebsiteFromJsonFile() throws WimBaseException {
        JSONObject mainJsonObject = null;
        try {

            String fileString = getJSONFromFile(ResourceUtils.getFile(dataExempleWebsite).getPath());
            JSONParser parser = new JSONParser();
            Object object = parser.parse(fileString);
            mainJsonObject = (JSONObject) object;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (importWebsiteFromJsonFile openfile)  ", new RuntimeException(e));
        }
        Set<?> keySet = mainJsonObject.keySet();
        Iterator<?> iterator = keySet.iterator();
        List<String> keyslist = new ArrayList<>();
        do {
            String k = iterator.next().toString();
            keyslist.add(k);
        } while (iterator.hasNext());
        WebsiteConfigDto websiteConfigDto = new WebsiteConfigDto();
        Long WebsiteId = Long.valueOf(sequenceGeneratorService.getSequenceNumberValue(WbsWebsite.SEQUENCE_NAME)) ;
        Long websitePageId = Long.valueOf(sequenceGeneratorService.getSequenceNumberValue(WbsWebsitePage.SEQUENCE_NAME)) + 1L;
        Long websiteSectionId = Long.valueOf(sequenceGeneratorService.getSequenceNumberValue(WbsWebsiteSection.SEQUENCE_NAME)) + 1L;
        for (int j = 0; j < mainJsonObject.size(); j++) {
            JSONArray jsonArray = (JSONArray) mainJsonObject.get(keyslist.get(j));
            List<WbsWebsiteSectionDto> websiteSection = new ArrayList<>();
            List<WbsWebsiteContactDto> websiteContact = new ArrayList<>();
            List<WbsWebsiteMenuDto> websiteMenu = new ArrayList<>();
            List<WbsWebsitePageDto> websitePage = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                switch (keyslist.get(j)) {
                    case "wbsWebsiteSection":
                        String data = (String) jsonObject.get("data");
                        Long layoutId = (Long) jsonObject.get("layoutId");
                        Long sectionId = (Long) jsonObject.get("sectionId");
                        Double sortKey = (Double) jsonObject.get("sortKey");
                        WbsWebsiteSectionDto wbsWebsiteSectionDto = new WbsWebsiteSectionDto(websiteSectionId, data, layoutId, sortKey, null, sectionId, WebsiteId, websitePageId, new WbsSectionDto(), new WbsLayoutDto(), null, "{}", "{}");
                        websiteSection.add(wbsWebsiteSectionDto);
                        if (i == jsonArray.size() - 1) {
                            websiteConfigDto.setWbsWebsiteSection(websiteSection);
                        }
                        break;
                    case "wbsWebsiteContact":
                        Long idwebsiteContact = (Long) jsonObject.get("id");
                        String instagram = (String) jsonObject.get("instagram");
                        String facebook = (String) jsonObject.get("facebook");
                        String linkedIn = (String) jsonObject.get("linkedIn");
                        String twitter = (String) jsonObject.get("twitter");
                        String youtube = (String) jsonObject.get("youtube");
                        String phoneNumbre = (String) jsonObject.get("phoneNumbre");
                        String adresse = (String) jsonObject.get("adresse");
                        String email = (String) jsonObject.get("email");
                        WbsWebsiteContactDto wbsWebsiteContactDto = new WbsWebsiteContactDto(idwebsiteContact, instagram, facebook, linkedIn, twitter, youtube, phoneNumbre, adresse, email, WebsiteId, new WbsWebsiteDto());
                        websiteContact.add(wbsWebsiteContactDto);
                        if (i == jsonArray.size() - 1) {
                            websiteConfigDto.setWbsWebsiteContact(websiteContact);
                        }
                        break;
                    case "wbsWebsiteMenu":
                        Long idwebsiteMenu = (Long) jsonObject.get("id");
                        String nameMenu = (String) jsonObject.get("name");
                        String nameAr1 = (String) jsonObject.get("nameAr");
                        Double sortKey1 = (Double) jsonObject.get("sortKey");
                        WbsWebsiteMenuDto wbsWebsiteMenuDto = new WbsWebsiteMenuDto(idwebsiteMenu, nameMenu, nameAr1, sortKey1, WebsiteId, new WbsWebsitePageDto(), websitePageId);
                        websiteMenu.add(wbsWebsiteMenuDto);
                        if (i == jsonArray.size() - 1) {
                            websiteConfigDto.setWbsWebsiteMenu(websiteMenu);
                        }
                        break;
                    case "wbsWebsitePage":
                        String namewebsitePage = (String) jsonObject.get("name");
                        String nameArwebsitePage = (String) jsonObject.get("nameAr");
                        String path = (String) jsonObject.get("path");
                        Double sortKey2 = (Double) jsonObject.get("sortKey");
                        Boolean homePage = (Boolean) jsonObject.get("homePage");
                        String seoTitle = (String) jsonObject.get("seoTitle");
                        String seoDescription = (String) jsonObject.get("seoDescription");
                        String seoKeywords = (String) jsonObject.get("seoKeywords");
                        WbsWebsitePageDto wbsWebsitePageDto = new WbsWebsitePageDto(websitePageId, namewebsitePage, nameArwebsitePage, path, WebsiteId, sortKey2, homePage, seoTitle, seoDescription, seoKeywords, false, null, websiteSectionId, new WbsWebsiteSectionDto());
                        websitePage.add(wbsWebsitePageDto);
                        if (i == jsonArray.size() - 1) {
                            websiteConfigDto.setWbsWebsitePage(websitePage);
                        }
                        break;
                }
            }
        }
        return persistConfigWebSiteData(websiteConfigDto);
    }
    public ToResponseDto persistConfigWebSiteData(WebsiteConfigDto websiteConfigDto) throws WimBaseException {
        ToResponseDto toResponseDto = new ToResponseDto(true);
        List<WbsWebsiteSectionDto> wbsSectionDtoList = websiteConfigDto.getWbsWebsiteSection();
        if (wbsSectionDtoList.size() != 0) {
            for (int i = 0; i < wbsSectionDtoList.size(); i++) {
                wbsWebsiteSectionService.persisteWbsWebsiteSection(wbsSectionDtoList.get(i));
            }
        }
        List<WbsWebsitePageDto> wbsWebsitePageList = websiteConfigDto.getWbsWebsitePage();
        if (wbsWebsitePageList.size() != 0) {
            for (int i = 0; i < wbsWebsitePageList.size(); i++) {
                wbsWebsitePageService.persisteWbsWebsitePage(wbsWebsitePageList.get(i));
            }
        }
        List<WbsWebsiteContactDto> wbsWebsiteContactDtoList = websiteConfigDto.getWbsWebsiteContact();
        if (wbsWebsiteContactDtoList.size() != 0) {
            for (int i = 0; i < wbsWebsiteContactDtoList.size(); i++) {
                wbsWebSiteContactService.persisteWbsWebsiteContact(wbsWebsiteContactDtoList.get(i));
            }
        }
        List<WbsWebsiteMenuDto> wbsWebsiteMenuList = websiteConfigDto.getWbsWebsiteMenu();
        if (wbsWebsiteMenuList.size() != 0) {
            for (int i = 0; i < wbsWebsiteMenuList.size(); i++) {
                wbsWebsiteMenuService.persisteWbsWebsiteMenu(wbsWebsiteMenuList.get(i));
            }
        }
        return toResponseDto;
    }
    /*#######################   export website from with all composants   #######################*/
    public WbsWebsiteComposantsDto exportWbsWebsiteAllComposants(Long id) throws WimBaseException {
        WbsWebsiteDto wbsWebsite = wbsWebsiteService.findWbsWebsiteById(id);
        List<WbsWebsiteContactDto> wbsWebsiteContact = wbsWebSiteContactService.findWbsWebsiteContactByCriteria(WbsWebsiteContactCriteria.builder().websiteId(id).build());
        List<WbsWebsitePageDto> wbsWebsitePage = wbsWebsitePageService.findWbsWebsitePageByCriteria(WbsWebsitePageCriteria.builder().websiteId(id).build());
        List<WbsWebsiteMenuDto> wbsWebsiteMenu = wbsWebsiteMenuService.findWbsWebsiteMenuByCriteria(WbsWebsiteMenuCriteria.builder().websiteId(id).build());
        List<WbsWebsiteSectionDto> wbsWebsiteSection = wbsWebsiteSectionService.findWbsWebsiteSectionByCriteria(WbsWebsiteSectionCriteria.builder().websiteId(id).build());
        WbsWebsiteComposantsDto WbsWebsiteComposantsDto = new WbsWebsiteComposantsDto(wbsWebsite, wbsWebsiteContact, wbsWebsitePage, wbsWebsiteMenu, wbsWebsiteSection);
        return WbsWebsiteComposantsDto;
    }
    /*#######################   import website from other website   #######################*/
    public ToResponseDto importWebsiteFromOtherWebsite(Long id) throws WimBaseException {
        WbsWebsiteComposantsDto wbsWebsiteComposantsDto = exportWbsWebsiteAllComposants(id);
        ToResponseDto toResponseDto = new ToResponseDto(true);
        Long websiteId = Long.valueOf(sequenceGeneratorService.getSequenceNumberValue(WbsWebsite.SEQUENCE_NAME)) ;
        Long websitePageId = Long.valueOf(sequenceGeneratorService.getSequenceNumberValue(WbsWebsitePage.SEQUENCE_NAME)) + 1L;
        Long websiteSectionId = Long.valueOf(sequenceGeneratorService.getSequenceNumberValue(WbsWebsiteSection.SEQUENCE_NAME)) + 1L;

        copyWebsiteContacts(wbsWebsiteComposantsDto.getWbsWebsiteContact(), websiteId);
        copyWebsitePages(wbsWebsiteComposantsDto.getWbsWebsitePage(), websiteId, websitePageId, websiteSectionId);
        copyWebsiteMenus(wbsWebsiteComposantsDto.getWbsWebsiteMenu(), websiteId, websitePageId);
        copyWebsiteSections(wbsWebsiteComposantsDto.getWbsWebsiteSection(), websiteId, websitePageId);

        return toResponseDto;
    }
    private void copyWebsiteContacts(List<WbsWebsiteContactDto> websiteContactDtoList, Long newWebsiteId) throws WimBaseException {
        for (WbsWebsiteContactDto websiteContactDto : websiteContactDtoList) {
            websiteContactDto.setWebsiteId(newWebsiteId);
            wbsWebSiteContactService.persisteWbsWebsiteContact(websiteContactDto);
        }
    }
    private void copyWebsitePages(List<WbsWebsitePageDto> wbsWebsitePageDtoList, Long newWebsiteId, Long newWebsitePageId, Long newWebsiteSectionId) throws WimBaseException {
        Map<Long, Long>  newWebsiteSectionIds = new HashMap<>();
        for (WbsWebsitePageDto wbsWebsitePageDto : wbsWebsitePageDtoList) {
            wbsWebsitePageDto.setWebsiteId(newWebsiteId);
            wbsWebsitePageDto.setWebsiteSectionId(mapSectionId(newWebsiteSectionIds, wbsWebsitePageDto.getWebsiteSectionId(), newWebsiteSectionId));
            wbsWebsitePageDto.setAddMenu(false);
            wbsWebsitePageService.persisteWbsWebsitePage(wbsWebsitePageDto);
            newWebsitePageId++;
        }
    }
    private Long mapSectionId(Map<Long, Long> sectionIdMap, Long oldSectionId, Long newSectionId) {
        if (!sectionIdMap.containsKey(oldSectionId)) {
            sectionIdMap.put(oldSectionId, newSectionId);
            return newSectionId;
        } else {
            return sectionIdMap.get(oldSectionId);
        }
    }
    private void copyWebsiteMenus(List<WbsWebsiteMenuDto> wbsWebsiteMenuDtoList, Long newWebsiteId, Long newWebsitePageId) throws WimBaseException {
        for (WbsWebsiteMenuDto wbsWebsiteMenuDto: wbsWebsiteMenuDtoList) {
            wbsWebsiteMenuDto.setWebsiteId(newWebsiteId);
            wbsWebsiteMenuDto.setWebsitePageId(newWebsitePageId);
            wbsWebsiteMenuService.persisteWbsWebsiteMenu(wbsWebsiteMenuDto);
            newWebsitePageId++;
        }
    }
    private void copyWebsiteSections(List<WbsWebsiteSectionDto> wbsWebsiteSectionDtoList, Long newWebsiteId, Long newWebsitePageId) throws WimBaseException {
        Map<Long, Long> newWebsitePageIds = new HashMap<>();
        for (WbsWebsiteSectionDto wbsWebsiteSectionDto : wbsWebsiteSectionDtoList) {
            wbsWebsiteSectionDto.setWebsiteId(newWebsiteId);
            wbsWebsiteSectionDto.setWebsitePageId(mapPageId(newWebsitePageIds, wbsWebsiteSectionDto.getWebsitePageId(), newWebsitePageId));
            wbsWebsiteSectionService.persisteWbsWebsiteSection(wbsWebsiteSectionDto);
        }
    }
    private Long mapPageId(Map<Long, Long> pageIdMap, Long oldPageId, Long newPageId) {
        if (!pageIdMap.containsKey(oldPageId)) {
            pageIdMap.put(oldPageId, newPageId);
            newPageId++;
            return newPageId;
        } else {
            newPageId = pageIdMap.values().stream().findFirst().orElse(newPageId);
            pageIdMap.put(oldPageId, newPageId);
            return newPageId;
        }
    }
}




