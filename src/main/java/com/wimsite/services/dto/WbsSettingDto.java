package com.wimsaas.wimapp.wimsite.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WbsSettingDto implements Serializable {
    private Long id;
    private String code;
    private String name;
    private String nameAr;
    private String defaultValue;
    private boolean active;
    private String description;
    private LocalDateTime dateCreation;
    private LocalDateTime dateUpdate;
    private Long sortKey;
    private Long settingCategoryId;
    private String inputType;
}
