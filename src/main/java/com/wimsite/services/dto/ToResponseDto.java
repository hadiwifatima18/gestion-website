package com.wimsaas.wimapp.wimsite.services.dto;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ToResponseDto {
    private Object value;
}