package com.wimsaas.wimapp.wimsite.services.dto;
import lombok.Data;

import java.util.List;
@Data
public class SettingConfigDto {
    List<WbsSectionDto> wbsSection;
    List<WbsLayoutDto> wbsLayout;
    List<WbsFontDto> wbsFont;
    List<WbsLanguageDto> wbsLanguage;

}
