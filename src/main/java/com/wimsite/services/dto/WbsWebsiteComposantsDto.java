package com.wimsaas.wimapp.wimsite.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WbsWebsiteComposantsDto {
    WbsWebsiteDto wbsWebsite;
    List<WbsWebsiteContactDto> wbsWebsiteContact;
    List<WbsWebsitePageDto> wbsWebsitePage;
    List<WbsWebsiteMenuDto> wbsWebsiteMenu;
    List<WbsWebsiteSectionDto> wbsWebsiteSection;
}
