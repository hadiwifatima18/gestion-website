package com.wimsaas.wimapp.wimsite.services.dto;
import lombok.Data;

import java.util.List;
@Data
public class WebsiteConfigDto {
    List<WbsWebsiteDto> wbsWebsite;
    List<WbsWebsiteContactDto> wbsWebsiteContact;
    List<WbsWebsitePageDto> wbsWebsitePage;
    List<WbsWebsiteMenuDto> wbsWebsiteMenu;
    List<WbsWebsiteSectionDto> wbsWebsiteSection;
}
