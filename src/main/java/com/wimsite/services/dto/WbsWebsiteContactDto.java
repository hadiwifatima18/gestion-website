package com.wimsaas.wimapp.wimsite.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WbsWebsiteContactDto implements Serializable {
    private Long id;
    private String instagram;
    private String facebook;
    private String linkedIn;
    private String twitter;
    private String youtube;
    private String phoneNumbre;
    private String  adresse;
    private String email;
    private Long websiteId;
    private WbsWebsiteDto website;
}