package com.wimsaas.wimapp.wimsite.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WbsSettingCategoryDto implements Serializable {
    private Long id;
    private String code;
    private String name;
    private String nameAr;
    private String description;
    private LocalDateTime dateCreation;
    private LocalDateTime dateUpdate;
    private Long sortKey;
}
