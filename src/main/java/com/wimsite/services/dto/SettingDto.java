package com.wimsaas.wimapp.wimsite.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SettingDto implements Serializable {

    private List<WbsLayoutDto> layout;
    private List<WbsSectionDto> section;
    private List<WbsLanguageDto> language;
    private List<WbsFontDto> font;
}

