package com.wimsaas.wimapp.wimsite.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteContact;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WbsWebsiteDto implements Serializable{

    private Long id;
    private String name;
    private String domainType;
    private String domainName;
    private String subdomainName;
    private String logo;
    private String favicon;
    private String warning;
    private String hash;
    private String languageCode;
    private Long fontLatinId;
    private Long fontArabicId;
    private Long statusId;
    private String initByJsonFile;
    private Long initByWebsiteId;
    private Boolean byJsonFile;
    private String folderHash;
    private Long websiteCount;
    private String nameStatus;

    // supplémentaire
    private WbsStatusDto status;
    private WbsLanguageDto language;
    private WbsFontDto arabicFont;
    private WbsFontDto latinFont;
    private List<WbsWebsiteMenuDto> websiteMenuDtos;
    private List<WbsWebsitePageDto> websitePageDtos;
    private WbsWebsiteContact websiteContactDto;
    private List<WbsWebsiteSectionDto> websiteSectionDtos;
    private LocalDateTime dateCreation;
    private String userCreation;
    private String userUpdate;
    private LocalDateTime dateUpdate;
    private Long storeId;
    private Long structureId;
    private Long moduleId;
    private Long month;
    private Long day;


}
