package com.wimsaas.wimapp.wimsite.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class  WbsSectionDto implements Serializable {

    private Long id;
    private String code;
    private String name;
    private String nameAr;
    private int countLayout;
}