package com.wimsaas.wimapp.wimsite.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;



@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WbsWebsitePageDto implements Serializable{
    private Long id;
    private String name;
    private String nameAr;
    private String path;
    private Long websiteId;
    private Double sortKey;
    private boolean homePage;
    private String seoTitle;
    private String seoDescription;
    private String seoKeywords;
    private boolean addMenu;
    private String hash;
    private Long websiteSectionId;
    private WbsWebsiteSectionDto websiteSection;
}




