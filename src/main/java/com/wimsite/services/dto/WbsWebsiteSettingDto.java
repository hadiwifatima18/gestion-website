package com.wimsaas.wimapp.wimsite.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WbsWebsiteSettingDto implements Serializable {
    private Long id;
    private Long websiteId;
    private Long settingId;
    private String value;
    private String userCreation;
    private String userUpdate;
    private LocalDateTime dateCreation;
    private LocalDateTime dateUpdate;
}
