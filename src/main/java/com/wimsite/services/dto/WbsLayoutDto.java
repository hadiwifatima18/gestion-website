package com.wimsaas.wimapp.wimsite.services.dto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONObject;


import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WbsLayoutDto implements Serializable{

    private Long id;
    private String code;
    private Long sectionId;
    private String image;
    private String data;
    private boolean selected;
    private WbsSectionDto sectionDto;
    private String dataAr;
    private String dataEn;

    @JsonIgnore
    public JSONObject getDataAsJson() {
        return new JSONObject(data);
    }
    @JsonProperty
    public void setDataFromJson(JSONObject jsonObject) {
        this.data = jsonObject.toString();
    }
    @JsonIgnore
    public JSONObject getDataArAsJson() {
        return new JSONObject(dataAr);
    }
    @JsonProperty
    public void setDataArFromJson(JSONObject jsonObject) {
        this.dataAr = jsonObject.toString();
    }
    @JsonIgnore
    public JSONObject getDataEnAsJson() {
        return new JSONObject(dataEn);
    }
    @JsonProperty
    public void setDataEnFromJson(JSONObject jsonObject) {
        this.dataEn = jsonObject.toString();
    }
}
