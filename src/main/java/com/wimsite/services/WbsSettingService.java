package com.wimsaas.wimapp.wimsite.services;

import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.starter.messages.MessageComponent;
import com.wimsaas.wimapp.wimnotify.services.SequenceGeneratorService;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsSetting;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsSettingRepository;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsSettingCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.WbsSettingDto;
import com.wimsaas.wimapp.wimsite.services.mappers.WbsSettingMapper;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WbsSettingService {
    @Autowired
    private WbsSettingRepository wbsSettingRepository;
    @Autowired
    private WbsSettingMapper wbsSettingMapper;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    private final MessageComponent messageComponent;

    public WbsSettingDto findWbsSettingById(Long id) throws WimBaseException {
        try {
            List<WbsSettingDto> WbsSettingDto = findWbsSettingByCriteria(WbsSettingCriteria.builder()
                    .id(id)
                    .build());
            if (WbsSettingDto != null && !WbsSettingDto.isEmpty()) {
                WbsSettingDto wbsSettingDto = WbsSettingDto.get(0);
                return wbsSettingDto;
            } else {
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsSettingById)  id (" + id + ")", new RuntimeException(e));
        }
    }
    public List<WbsSettingDto> findWbsSettingByCriteria(WbsSettingCriteria wbsSettingCriteria) throws WimBaseException {
        List<Document> criterias = new ArrayList<Document>();
        if (wbsSettingCriteria.getId() != null) {
            criterias.add(new Document("id", wbsSettingCriteria.getId()));
        }
        if (wbsSettingCriteria.getCode() != null) {
            criterias.add(new Document("code", wbsSettingCriteria.getCode()));
        }
        if (wbsSettingCriteria.getName() != null) {
            criterias.add(new Document("name", wbsSettingCriteria.getName()));
        }
        if (wbsSettingCriteria.getNameAr() != null) {
            criterias.add(new Document("name_ar", wbsSettingCriteria.getNameAr()));
        }
        if (wbsSettingCriteria.getDefaultValue() != null) {
            criterias.add(new Document("default_value", wbsSettingCriteria.getDefaultValue()));
        }
        if (wbsSettingCriteria.getDateCreation() != null) {
            criterias.add(new Document("date_creation", wbsSettingCriteria.getDateCreation()));
        }
        if (wbsSettingCriteria.getDateUpdate() != null) {
            criterias.add(new Document("date_update", wbsSettingCriteria.getDateUpdate()));
        }
        if (wbsSettingCriteria.getSortKey() != null) {
            criterias.add(new Document("sort_key", wbsSettingCriteria.getSortKey()));
        }
        if (wbsSettingCriteria.getDescription() != null) {
            criterias.add(new Document("description", wbsSettingCriteria.getDescription()));
        }
        if (wbsSettingCriteria.getInputType() != null) {
            criterias.add(new Document("input_type", wbsSettingCriteria.getInputType()));
        }
        if (wbsSettingCriteria.getSettingCategoryId() != null) {
            criterias.add(new Document("setting_category_id", wbsSettingCriteria.getSettingCategoryId()));
        }
        if (criterias.isEmpty()) {
            criterias.add(new Document());
        }
        return wbsSettingMapper.toDtos(wbsSettingRepository.findByQuery(new Document("$and", criterias)));
    }
    public WbsSettingDto persisteWbsSetting(WbsSettingDto wbsSettingDto) {
        wbsSettingDto.setId(Long.valueOf(sequenceGeneratorService.getSequenceNumber(WbsSetting.SEQUENCE_NAME)));
        wbsSettingDto.setDateCreation(LocalDateTime.now());
        return wbsSettingMapper.toDto(wbsSettingRepository.save(wbsSettingMapper.toEntity(wbsSettingDto)));
    }
    public WbsSettingDto updateWbsSettingDto(Long id, WbsSettingDto wbsSettingDto) throws WimBaseException {
        try {
            WbsSettingDto oldWbsSettingDto = findWbsSettingById(id);
            wbsSettingDto.setId(oldWbsSettingDto.getId());
            wbsSettingDto.setDateUpdate(LocalDateTime.now());
            return wbsSettingMapper.toDto(wbsSettingRepository.save(wbsSettingMapper.toEntity(wbsSettingDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing updateWbsSettingDto for id (" + id + ")", new RuntimeException(e));
        }
    }
    public ResponseDto deleteWbsSettingDto(Long id)throws WimBaseException {
        try{
            wbsSettingRepository.deleteById(id);
            ResponseDto responseDto=new ResponseDto();
            responseDto.setMessage(messageComponent.get("globale.element.deleted.message"));
            return responseDto;
        }
        catch (Exception e){
            throw new InternalErrorException("Error while executing deleteWbsSettingDto for id (" + id + ")", new RuntimeException(e))  ;
        }
    }
}
