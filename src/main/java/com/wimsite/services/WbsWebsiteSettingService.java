package com.wimsaas.wimapp.wimsite.services;

import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.starter.messages.MessageComponent;
import com.wimsaas.wimapp.wimnotify.services.SequenceGeneratorService;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteSetting;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsWebsiteSettingRepository;
import com.wimsaas.security.components.JwtTokenComponent;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsWebsiteSettingCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteSettingDto;
import com.wimsaas.wimapp.wimsite.services.mappers.WbsWebsiteSettingMapper;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WbsWebsiteSettingService {
    @Autowired
    private WbsWebsiteSettingRepository wbsWebsiteSettingRepository;
    @Autowired
    private WbsWebsiteSettingMapper wbsWebsiteSettingMapper;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    private JwtTokenComponent jwtTokenComponent;
    private final MessageComponent messageComponent;

    public WbsWebsiteSettingDto persisteWbsWebsiteSetting(WbsWebsiteSettingDto wbsWebsiteSettingDto) {
        wbsWebsiteSettingDto.setId(Long.valueOf(sequenceGeneratorService.getSequenceNumber(WbsWebsiteSetting.SEQUENCE_NAME)));
        wbsWebsiteSettingDto.setUserCreation(String.valueOf(jwtTokenComponent.getId()));
        wbsWebsiteSettingDto.setDateCreation(LocalDateTime.now());
        return wbsWebsiteSettingMapper.toDto(wbsWebsiteSettingRepository.save(wbsWebsiteSettingMapper.toEntity(wbsWebsiteSettingDto)));
    }
    public WbsWebsiteSettingDto updateWbsWebsiteSetting(Long id, WbsWebsiteSettingDto wbsWebsiteSettingDto) throws WimBaseException {
        try {
            WbsWebsiteSettingDto oldWbsWebsiteSettingDto = findWbsWebsiteSettingById(id);
            wbsWebsiteSettingDto.setId(oldWbsWebsiteSettingDto.getId());
            wbsWebsiteSettingDto.setUserUpdate(String.valueOf(jwtTokenComponent.getId()));
            wbsWebsiteSettingDto.setDateUpdate(LocalDateTime.now());
            return wbsWebsiteSettingMapper.toDto(wbsWebsiteSettingRepository.save(wbsWebsiteSettingMapper.toEntity(wbsWebsiteSettingDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing updateWbsWebsiteSetting for id (" + id + ")", new RuntimeException(e));
        }
    }
    public WbsWebsiteSettingDto findWbsWebsiteSettingById(Long id) throws WimBaseException {
        try {
            List<WbsWebsiteSettingDto> WbsWebsiteSettingDto = findWbsWebsiteSettingByCriteria(WbsWebsiteSettingCriteria.builder()
                    .id(id)
                    .build());
            if (WbsWebsiteSettingDto != null && !WbsWebsiteSettingDto.isEmpty()) {
                WbsWebsiteSettingDto wbsWebsiteSettingDto = WbsWebsiteSettingDto.get(0);
                return wbsWebsiteSettingDto;
            } else {
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsWebsiteSettingById)  id (" + id + ")", new RuntimeException(e));
        }
    }
    public List<WbsWebsiteSettingDto> findWbsWebsiteSettingByCriteria(WbsWebsiteSettingCriteria wbsWebsiteSettingCriteria) throws WimBaseException {
        List<Document> criterias = new ArrayList<Document>();
        if (wbsWebsiteSettingCriteria.getId() != null) {
            criterias.add(new Document("id", wbsWebsiteSettingCriteria.getId()));
        }
        if (wbsWebsiteSettingCriteria.getWebsiteId() != null) {
            criterias.add(new Document("website_id", wbsWebsiteSettingCriteria.getWebsiteId()));
        }
        if (wbsWebsiteSettingCriteria.getSettingId() != null) {
            criterias.add(new Document("setting_id", wbsWebsiteSettingCriteria.getSettingId()));
        }
        if (wbsWebsiteSettingCriteria.getValue() != null) {
            criterias.add(new Document("value", wbsWebsiteSettingCriteria.getValue()));
        }
        if (wbsWebsiteSettingCriteria.getUserUpdate() != null) {
            criterias.add(new Document("user_update", wbsWebsiteSettingCriteria.getDateUpdate()));
        }
        if (wbsWebsiteSettingCriteria.getDateCreation() != null) {
            criterias.add(new Document("user_creation", wbsWebsiteSettingCriteria.getUserCreation()));
        }
        if (wbsWebsiteSettingCriteria.getDateCreation() != null) {
            criterias.add(new Document("date_creation", wbsWebsiteSettingCriteria.getDateCreation()));
        }
        if (wbsWebsiteSettingCriteria.getDateUpdate() != null) {
            criterias.add(new Document("date_update", wbsWebsiteSettingCriteria.getSettingId()));
        }
        if (criterias.isEmpty()) {
            criterias.add(new Document());
        }
        return wbsWebsiteSettingMapper.toDtos(wbsWebsiteSettingRepository.findByQuery(new Document("$and", criterias)));
    }
    public ResponseDto deleteWbsWebsiteSetting(Long id)throws WimBaseException {
        try{
            wbsWebsiteSettingRepository.deleteById(id);
            ResponseDto responseDto=new ResponseDto();
            responseDto.setMessage(messageComponent.get("globale.element.deleted.message"));
            return responseDto;
        }
        catch (Exception e){
            throw new InternalErrorException("Error while executing deleteWbsWebsiteSetting for id (" + id + ")", new RuntimeException(e))  ;
        }
    }
}
