package com.wimsaas.wimapp.wimsite.services;

import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.starter.messages.MessageComponent;
import com.wimsaas.wimapp.wimnotify.services.SequenceGeneratorService;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteMenu;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsitePage;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsWebsiteMenuRepository;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsWebsitePageRepository;
import com.wimsaas.wimapp.wimsite.services.component.SiteHashComponent;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsWebsitePageCriteria;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsWebsiteSectionCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteDto;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteMenuDto;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsitePageDto;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteSectionDto;
import com.wimsaas.wimapp.wimsite.services.mappers.WbsWebsitePageMapper;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class WbsWebsitePageService {
    @Autowired
    private WbsWebsitePageMapper wbsWebsitePageMapper;
    @Autowired
    private WbsWebsitePageRepository  wbsWebsitePageRepository ;
    @Autowired
    private WbsWebsiteSectionService  wbsWebsiteSectionService ;
    @Autowired
    private WbsWebsiteService wbsWebsiteService;
    @Autowired
    private WbsWebsiteMenuService wbsWebsiteMenuService;
    @Autowired
    private WbsWebsiteMenuRepository wbsWebsiteMenuRepository;
    @Autowired
    private SiteHashComponent siteHashComponent;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    private MessageComponent messageComponent;
    public WbsWebsitePageDto findWbsWebsitePageById(Long id) throws WimBaseException {
        try{
            List<WbsWebsitePageDto>WbsWebsitePageDtos=findWbsWebsitePageByCriteria(WbsWebsitePageCriteria.builder()
                    .id(id)
                    .build());
            if(WbsWebsitePageDtos != null && ! WbsWebsitePageDtos.isEmpty()){
                WbsWebsitePageDto wbsWebsitePageDto=WbsWebsitePageDtos.get(0);
                return wbsWebsitePageDto;}

            else{
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        }
        catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsSectionById)  id (" + id + ")", new RuntimeException(e));
        }
    }
    public List<WbsWebsitePageDto> findWbsWebsitePageByCriteria(WbsWebsitePageCriteria wbsWebsitePageCriteria)throws WimBaseException {
        List<Document> criterias= new ArrayList<Document>();
        if (wbsWebsitePageCriteria.getId() != null) {
            criterias.add (new Document("id",wbsWebsitePageCriteria.getId()));
        }
        if (wbsWebsitePageCriteria.getName()!= null) {
            criterias.add(new Document("name",wbsWebsitePageCriteria.getName()));
        }
        if (wbsWebsitePageCriteria.getNameAr() != null) {
            criterias.add(new Document("name_ar",wbsWebsitePageCriteria.getNameAr()));
        }
        if (wbsWebsitePageCriteria.getPath()!= null) {
            criterias.add(new Document("path",wbsWebsitePageCriteria.getPath()));
        }
        if (wbsWebsitePageCriteria.getSortKey() != null) {
            criterias.add(new Document("sort_key",wbsWebsitePageCriteria.getSortKey()));
        }
        if (wbsWebsitePageCriteria.getSeoTitle()!= null) {
            criterias.add(new Document("seo_title",wbsWebsitePageCriteria.getSeoTitle()));
        }
        if (wbsWebsitePageCriteria.getWebsiteId()!= null) {
            criterias.add(new Document("website_id",wbsWebsitePageCriteria.getWebsiteId()));
        }
        if (wbsWebsitePageCriteria.getSeoDescription() != null) {
            criterias.add(new Document("seo_description",wbsWebsitePageCriteria.getSeoDescription()));
        }
        if (wbsWebsitePageCriteria.getSeoKeywords()!= null) {
            criterias.add(new Document("seo_keywords",wbsWebsitePageCriteria.getSeoKeywords()));
        }
        if (wbsWebsitePageCriteria.getWebsiteSectionId()!= null) {
            criterias.add(new Document("website_section_id",wbsWebsitePageCriteria.getWebsiteSectionId()));
        }
        if(criterias.isEmpty()){
            criterias.add(new Document());
        }
        List<WbsWebsitePageDto>  wbsWebsitePageDtoList= wbsWebsitePageMapper.toDtos(wbsWebsitePageRepository.findByQuery(new Document("$and", criterias)));
        for(WbsWebsitePageDto wbsWebsitePageDto : wbsWebsitePageDtoList){
            if(wbsWebsitePageDto.getWebsiteSectionId() != null) {
                try {
                    wbsWebsitePageDto.setWebsiteSection(wbsWebsiteSectionService.findWbsWebsiteSectionById(wbsWebsitePageDto.getWebsiteSectionId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return wbsWebsitePageDtoList;
    }
     public WbsWebsitePageDto persisteWbsWebsitePage(WbsWebsitePageDto wbsWebsitePageDto) throws WimBaseException {
        WbsWebsiteMenuDto wbsWebsiteMenuDto =new WbsWebsiteMenuDto();
        boolean result = validateWbsWebSitePage(wbsWebsitePageDto);
        if (result) {
            try {
                Long pageId = Long.valueOf(sequenceGeneratorService.getSequenceNumber(WbsWebsitePage.SEQUENCE_NAME));
                wbsWebsitePageDto.setId(pageId);
                wbsWebsitePageDto.setHash(generateHashForWbsWebsitePage(wbsWebsitePageDto));
                WbsWebsitePage entity = wbsWebsitePageMapper.toEntity(wbsWebsitePageDto);
                WbsWebsitePage savedPage = wbsWebsitePageRepository.save(entity);
                if(wbsWebsitePageDto.isAddMenu()==true){
                    wbsWebsiteMenuDto.setName(wbsWebsitePageDto.getName());
                    wbsWebsiteMenuDto.setNameAr(wbsWebsitePageDto.getNameAr());
                    wbsWebsiteMenuDto.setSortKey(wbsWebsitePageDto.getSortKey());
                    wbsWebsiteMenuDto.setWebsiteId(wbsWebsitePageDto.getWebsiteId());
                    wbsWebsiteMenuDto.setWebsitePageId(pageId);
                    wbsWebsiteMenuService.persisteWbsWebsiteMenu(wbsWebsiteMenuDto);
                }
                return wbsWebsitePageMapper.toDto(savedPage);
            } catch (Exception e) {
                throw new InternalErrorException("Erreur lors de l'exécution de cette méthode (persisteWbsWebsitePage)", new RuntimeException(e));
            }
        } else {
            throw new FunctionalException("La validation de la page du site Web a échoué.");
        }

    }
    public String generateHashForWbsWebsitePage(WbsWebsitePageDto wbsWebsitePageDto) throws WimBaseException {
        String input = wbsWebsitePageDto.getId()+ wbsWebsitePageDto.getPath() + wbsWebsitePageDto.getSeoTitle() + wbsWebsitePageDto.getSeoKeywords();
        return siteHashComponent.generateHash(input);
    }
    public WbsWebsitePageDto updateWbsWebsitePage(Long id, WbsWebsitePageDto wbsWebsitePageDto) throws WimBaseException {
        WbsWebsitePageDto oldWbsWebsitePageDto = findWbsWebsitePageById(id);
        wbsWebsitePageDto.setId(oldWbsWebsitePageDto.getId());
        boolean result = validateWbsWebSitePage(wbsWebsitePageDto);
        if(result){
            try{
                return wbsWebsitePageMapper.toDto(wbsWebsitePageRepository.save(wbsWebsitePageMapper.toEntity( wbsWebsitePageDto)));
            }
            catch(Exception e){
                throw new InternalErrorException("Error while executing updateWbsWebsitePage for id (" + id + ")", new RuntimeException(e));
            }
        }
        else {
            throw new FunctionalException("La validation de la modification du site Web a échoué.");
        }
    }
   public boolean validateWbsWebSitePage(WbsWebsitePageDto wbsWebsitePageDto) throws WimBaseException {
       Long websiteId=wbsWebsitePageDto.getWebsiteId();
       Long websiteSectionId=wbsWebsitePageDto.getWebsiteSectionId();
       boolean pathUnique=isPathUnique(wbsWebsitePageDto.getPath(),wbsWebsitePageDto.getWebsiteId());
        boolean websiteExists = false;
       // boolean websiteSectionExists = false;
        try {
            WbsWebsiteDto wbsWebsiteDto = wbsWebsiteService.findWbsWebsiteById(websiteId);
            websiteExists = wbsWebsiteDto != null;
        } catch (FunctionalException e) {
            if (!websiteExists)
                throw new FunctionalException("Le website n'existe pas.");
        }
       /* try {
            WbsWebsiteSectionDto wbsWebsiteSectionDto = wbsWebsiteSectionService.findWbsWebsiteSectionById(websiteSectionId);
            websiteSectionExists = wbsWebsiteSectionDto != null;
        } catch (FunctionalException e) {
            if (!websiteSectionExists)
                throw new FunctionalException("La websiteSection n'existe pas.");
        }*/
        if(pathUnique==false){
            throw new FunctionalException("La path est déja existe .");
        }
        if (websiteExists  && pathUnique)
            return true;
        return false;
    }
    /*private boolean isPathUnique(String path) {
        WbsWebsitePage existingPage = wbsWebsitePageRepository.findByPath(path);
        return existingPage == null;
    }*/
    private boolean isPathUnique(String path, Long websiteId) {
        WbsWebsitePage existingPage = wbsWebsitePageRepository.findByPathAndWebsiteId(path, websiteId);
        return existingPage == null;
    }

    public ResponseDto deleteWebsitePage(Long id) throws WimBaseException {
      try {
          List<WbsWebsiteSectionDto> wbsWebsiteSectionDtoList = wbsWebsiteSectionService.findWbsWebsiteSectionByCriteria(
                  WbsWebsiteSectionCriteria.builder()
                          .websitePageId(id)
                          .build()
          );
          for (WbsWebsiteSectionDto websiteSectionDto : wbsWebsiteSectionDtoList) {
              wbsWebsiteSectionService.deleteWbsWebsiteSectionByHash(websiteSectionDto.getHash());
          }
          List<WbsWebsiteMenu> wbsWebsiteMenuList =wbsWebsiteMenuRepository.findByWebsitePageId(id);
          for (WbsWebsiteMenu websiteMenu : wbsWebsiteMenuList) {
              wbsWebsiteMenuService.deleteWbsWebsiteMenu(websiteMenu.getId());
          }
          wbsWebsitePageRepository.deleteById(id);
          ResponseDto responseDto = new ResponseDto();
          responseDto.setMessage(messageComponent.get("globale.element.deleted.message"));
          return responseDto;
      } catch (Exception e) {
          throw new InternalErrorException("Error while executing deleteWebsitePage for id (" + id + ")", new RuntimeException(e));
      }
  }
}
