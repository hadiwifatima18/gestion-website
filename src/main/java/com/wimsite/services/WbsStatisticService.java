package com.wimsaas.wimapp.wimsite.services;

import com.wimsaas.security.components.JwtTokenComponent;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.wimapp.wimsite.dao.repositories.StatisticsRepository;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsWebsiteRepository;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsWebsiteSectionRepository;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteDto;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.*;


@Service
public class WbsStatisticService {

    @Autowired
    private WbsWebsiteRepository wbsWebsiteRepository;
    @Autowired
    private StatisticsRepository statisticsRepository;
    @Autowired
    private WbsWebsiteSectionRepository wbsWebsiteSectionRepository;

    /*##############    Number of website and websiteSection   ##############*/
    public JSONObject findNbrOfWebsiteAndWebsiteSection() throws WimBaseException {
        try {
                JSONObject mainObject = new JSONObject();
                mainObject.put("totalWebsite", wbsWebsiteRepository.count());
                mainObject.put("totalWebsiteSection",wbsWebsiteSectionRepository.count());
                return mainObject;
            } catch (Exception e) {
                throw new InternalErrorException("Error while executing this method (findTotalOfwebsiteAndWebsiteSection)", new RuntimeException(e));
        }
    }
    /*##############   Number of website for each status   ##############*/
    public List<WbsWebsiteDto> nbrOfWebsiteForEachStatus(LocalDate startDate, LocalDate endDate, Long structureId, boolean currentDay, boolean currentWeek, boolean currentMonth, boolean currentYear) throws WimBaseException {
        try {
                List<WbsWebsiteDto> wbsWebsiteDtoList=new ArrayList<>();

                int year = Calendar.getInstance().get(Calendar.YEAR);
                int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
                int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

                LocalDateTime minDateTime = null;
                LocalDateTime maxDateTime = null;
                boolean exitingDate = false;
                LocalDate minDate = null;
                LocalDate maxDate = null;
                if (startDate != null && endDate != null) {
                    minDate = startDate;
                    maxDate = endDate;
                } else {
                    if (currentYear) {
                        exitingDate = true;
                        minDate = LocalDate.of(year, 1, 1);
                        maxDate = minDate.plusYears(1);
                    }
                    if (currentMonth && !exitingDate) {
                        exitingDate = true;
                        minDate = LocalDate.of(year, month, 1);
                        maxDate = minDate.plusMonths(1);
                    }
                    if (currentWeek && !exitingDate) {
                        exitingDate = true;
                        TemporalField fieldISO = WeekFields.of(Locale.FRANCE).dayOfWeek();
                        LocalDate now = LocalDate.now();
                        minDate = now.with(fieldISO, 1);
                        maxDate = minDate.plusDays(7);
                    }
                    if (currentDay && !exitingDate) {
                        exitingDate = true;
                        minDate = LocalDate.of(year, month, day);
                        maxDate = minDate.plusDays(1);
                    }
                }
                if (minDate == null &&  maxDate == null) {
                    minDate = LocalDate.of(2021, 1, 1);
                    maxDate =  LocalDate.now().plusDays(1);

                } else if (minDate == null &&  maxDate != null) {
                    minDate = LocalDate.of(2021, 1, 1);

                } else if (minDate != null &&  maxDate == null) {
                    maxDate = LocalDate.now().plusDays(1);

                }
                if(structureId!=null){
                    return statisticsRepository.nbrOfWebsiteForEachStatus(minDate.atStartOfDay(), maxDate.atTime(LocalTime.MAX),structureId);
                } else{
                     return statisticsRepository.nbrOfWebsiteWithoutStructureIdForEachStatus(minDate.atStartOfDay(), maxDate.atTime(LocalTime.MAX));
                }
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (NbrOfWebsiteForEachStatus)", new RuntimeException(e));
        }
    }

    /*##############   Number of website for each domain type   ##############*/
    public List<WbsWebsiteDto> nbrOfWebsiteForEachDomainType(LocalDate startDate, LocalDate endDate, Long structureId, boolean currentDay, boolean currentWeek, boolean currentMonth, boolean currentYear) throws WimBaseException {
        try {
                JSONObject mainObject = new JSONObject();
                int year = Calendar.getInstance().get(Calendar.YEAR);
                int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
                int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

                LocalDateTime minDateTime = null;
                LocalDateTime maxDateTime = null;
                boolean exitingDate = false;
                LocalDate minDate = null;
                LocalDate maxDate = null;
                if (startDate != null && endDate != null) {
                    minDate = startDate;
                    maxDate = endDate;
                } else {
                    if (currentYear) {
                        exitingDate = true;
                        minDate = LocalDate.of(year, 1, 1);
                        maxDate = minDate.plusYears(1);
                    }
                    if (currentMonth && !exitingDate) {
                        exitingDate = true;
                        minDate = LocalDate.of(year, month, 1);
                        maxDate = minDate.plusMonths(1);
                    }
                    if (currentWeek && !exitingDate) {
                        exitingDate = true;
                        TemporalField fieldISO = WeekFields.of(Locale.FRANCE).dayOfWeek();
                        LocalDate now = LocalDate.now();
                        minDate = now.with(fieldISO, 1);
                        maxDate = minDate.plusDays(7);
                    }
                    if (currentDay && !exitingDate) {
                        exitingDate = true;
                        minDate = LocalDate.of(year, month, day);
                        maxDate = minDate.plusDays(1);
                    }
                }
                if(minDate != null && maxDate != null){
                    minDateTime = minDate.atStartOfDay();
                    maxDateTime = maxDate.atTime(23, 59, 59);
                }
                if (minDate == null &&  maxDate == null) {
                    minDate = LocalDate.of(2021, 1, 1);
                    maxDate =  LocalDate.now().plusDays(1);

                } else if (minDate == null &&  maxDate != null) {
                    minDate = LocalDate.of(2021, 1, 1);

                } else if (minDate != null &&  maxDate == null) {
                    maxDate = LocalDate.now().plusDays(1);

                }
                if(structureId!=null){
                    return statisticsRepository.nbrOfWebsiteForEachDomainType(minDateTime, maxDateTime, structureId);
                }
                else{
                    return statisticsRepository.nbrOfWebsiteWithoutStructureIdForEachDomainType(minDateTime, maxDateTime);
                }
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (NbrOfWebsiteForEachDomainType)", new RuntimeException(e));
        }
    }

    /*#####################   Total number of website by year and months   #####################*/
    public JSONObject getWebsiteForYearMonth(Long year, Long month, Long structureId)throws WimBaseException  {
        try {
            List<WbsWebsiteDto> websiteDtoList;
            LocalDate minDate = null;
            LocalDate maxDate = null;
            if (year != null && month != null) {
                minDate = LocalDate.of(year.intValue(), month.intValue(), 1);
                maxDate = minDate.plusMonths(1);
            } else if (year != null && month == null) {
                minDate = LocalDate.of(year.intValue(), 1, 1);
                maxDate = minDate.plusYears(1);
            }
            if (structureId != null && year != null && month != null) {
                websiteDtoList = statisticsRepository.nbrOfWebsiteByDays(minDate.atStartOfDay(), maxDate.atTime(LocalTime.MAX), structureId);
            } else if (structureId != null && year != null && month == null) {
                websiteDtoList = statisticsRepository.nbrOfWebsiteByMonths(minDate.atStartOfDay(), maxDate.atTime(LocalTime.MAX), structureId);
            } else if (structureId == null && year != null && month != null) {
                websiteDtoList = statisticsRepository.nbrOfWebsiteWithoutStructureIdByDays(minDate.atStartOfDay(), maxDate.atTime(LocalTime.MAX));
            } else if (structureId == null && year != null && month == null) {
                websiteDtoList = statisticsRepository.nbrOfWebsiteWithoutStructureIdByMonths(minDate.atStartOfDay(), maxDate.atTime(LocalTime.MAX));
            } else {
                JSONObject errorResponse = new JSONObject();
                return errorResponse;
            }
            TreeMap<String, Integer> sortedResult = new TreeMap<>();
            if (websiteDtoList != null && !websiteDtoList.isEmpty()) {
                if (year != null && month != null) {
                    int lastDayOfMonth = minDate.lengthOfMonth();
                    for (int day = 1; day <= lastDayOfMonth; day++) {
                        String key = String.valueOf(day);
                        int value = 0;
                        for (WbsWebsiteDto websiteDto : websiteDtoList) {
                            if (websiteDto.getDay() != null && websiteDto.getDay() == day) {
                                value = Math.toIntExact(websiteDto.getWebsiteCount());
                                break;
                            }
                        }
                        sortedResult.put(key, value);
                    }
                } else if (year != null && month == null) {
                    for (int months = 1; months <= 12; months++) {
                        String key = String.valueOf(months);
                        int value = 0;
                        for (WbsWebsiteDto websiteDto : websiteDtoList) {
                            if (websiteDto.getMonth() == months) {
                                value = Math.toIntExact(websiteDto.getWebsiteCount());
                                break;
                            }
                        }
                        sortedResult.put(key, value);
                    }
                }
            }
            JSONObject result = new JSONObject(sortedResult);
            return result;
        }catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (getWebsiteForYearMonth)", new RuntimeException(e));
        }
    }
}






