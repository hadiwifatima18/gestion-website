package com.wimsaas.wimapp.wimsite.services;

import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.starter.messages.MessageComponent;
import com.wimsaas.wimapp.wimnotify.services.SequenceGeneratorService;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsStatus;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsStatusRepository;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsStatusCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.WbsStatusDto;
import com.wimsaas.wimapp.wimsite.services.mappers.WbsStatusMapper;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WbsStatusService {
    @Autowired
    private WbsStatusRepository wbsStatusRepository;
    @Autowired
    private WbsStatusMapper wbsStatusMapper;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    private MessageComponent messageComponent;


    public WbsStatusDto findWbsStatusById(Long id) throws WimBaseException {
        try {
            List<WbsStatusDto> wbsStatusDtos = findWbsStatusByCriteria(WbsStatusCriteria.builder()
                    .id(id)
                    .build());
            if (wbsStatusDtos != null && !wbsStatusDtos.isEmpty()) {
                WbsStatusDto wbsStatusDto = wbsStatusDtos.get(0);
                return wbsStatusDto;
            } else {
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsStatusById)  id (" + id + ")", new RuntimeException(e));
        }
    }

    public List<WbsStatusDto> findWbsStatusByCriteria(WbsStatusCriteria wbsStatusCriteria) throws WimBaseException {
        List<Document> criterias = new ArrayList<Document>();
        if (wbsStatusCriteria.getId() != null) {
            criterias.add(new Document("id", wbsStatusCriteria.getId()));
        }
        if (wbsStatusCriteria.getDescription() != null) {
            criterias.add(new Document("description", wbsStatusCriteria.getDescription()));
        }
        if (wbsStatusCriteria.getName() != null) {
            criterias.add(new Document("name", wbsStatusCriteria.getName()));
        }
        if (criterias.isEmpty()) {
            criterias.add(new Document());
        }
        return wbsStatusMapper.toDtos(wbsStatusRepository.findByQuery(new Document("$and", criterias)));
    }

    public WbsStatusDto persisteWbsStatus(WbsStatusDto wbsStatusDto) {
        wbsStatusDto.setId(Long.valueOf(sequenceGeneratorService.getSequenceNumber(WbsStatus.SEQUENCE_NAME)));
        return wbsStatusMapper.toDto(wbsStatusRepository.save(wbsStatusMapper.toEntity(wbsStatusDto)));
    }

    public WbsStatusDto updateWbsStatus(Long id, WbsStatusDto wbsStatusDto) throws WimBaseException {
        try {
            WbsStatusDto oldwbsStatusDto = findWbsStatusById(id);
            wbsStatusDto.setId(oldwbsStatusDto.getId());
            return wbsStatusMapper.toDto(wbsStatusRepository.save(wbsStatusMapper.toEntity(wbsStatusDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing updateWbsStatus for id (" + id + ")", new RuntimeException(e));
        }
    }

    public ResponseDto deleteWbsStatus(Long id) throws WimBaseException {
        try {
            wbsStatusRepository.deleteById(id);
            ResponseDto responseDto = new ResponseDto();
            responseDto.setMessage(messageComponent.get("globale.element.deleted.message"));
            return responseDto;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing deleteWbsStatus for id (" + id + ")", new RuntimeException(e));
        }
    }
}

