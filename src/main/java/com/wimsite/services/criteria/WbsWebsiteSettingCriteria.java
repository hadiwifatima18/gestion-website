package com.wimsaas.wimapp.wimsite.services.criteria;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
@Data
@Builder
public class WbsWebsiteSettingCriteria implements Serializable {
    private Long id;
    private Long websiteId;
    private Long settingId;
    private String value;
    private String userCreation;
    private String userUpdate;
    private LocalDateTime dateCreation;
    private LocalDateTime dateUpdate;
}
