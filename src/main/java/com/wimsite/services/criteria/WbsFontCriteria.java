package com.wimsaas.wimapp.wimsite.services.criteria;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

@Data
@Builder
public class WbsFontCriteria implements Serializable {
    private Long id;
    private String urlFont;
    private String nameFont;
    private String thumbnail;
    private Boolean arabic;
    public Boolean getArabic() {
        return arabic;
    }
}
