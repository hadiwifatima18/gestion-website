package com.wimsaas.wimapp.wimsite.services.criteria;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;


@Data
@Builder
public class WbsSectionCriteria implements Serializable {
    private Long id;
    private String code;
    private String nameAr;
    private String name;
}
