package com.wimsaas.wimapp.wimsite.services.criteria;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
public class WbsWebsiteCriteria implements Serializable {
    private Long id;
    private String name;
    private String domainType;
    private String domainName;
    private String subdomainName;
    private String folderHash;
    private String hash;
    private String codeLanguage;
    private Long fontArabicId;
    private Long fontLatinId;
    private Long statusId;
    private String initByJsonFile;
    private Long initByWebsiteId;
    private LocalDateTime dateCreation;
    private String userCreation;
    private String userUpdate;
    private LocalDateTime dateUpdate;
    private Long storeId;
    private Long structureId;
    private Long moduleId;

}
