package com.wimsaas.wimapp.wimsite.services.criteria;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;


@Data
@Builder
public class WbsWebsiteMenuCriteria implements Serializable {
    private Long id;
    private String name;
    private String nameAr;
    private Double sortKey;
    private Long websiteId;
    private Long websitePageId;
}
