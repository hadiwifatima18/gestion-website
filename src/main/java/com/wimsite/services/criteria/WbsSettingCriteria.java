package com.wimsaas.wimapp.wimsite.services.criteria;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
@Data
@Builder
public class WbsSettingCriteria implements Serializable {
    private Long id;
    private String code;
    private String name;
    private String nameAr;
    private String defaultValue;
    private boolean active;
    private String description;
    private LocalDateTime dateCreation;
    private LocalDateTime dateUpdate;
    private Long sortKey;
    private Long settingCategoryId;
    private String inputType;
}
