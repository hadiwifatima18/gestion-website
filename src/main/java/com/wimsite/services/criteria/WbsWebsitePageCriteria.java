package com.wimsaas.wimapp.wimsite.services.criteria;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;


@Data
@Builder
public class WbsWebsitePageCriteria implements Serializable {
    private Long id;
    private String name;
    private String nameAr;
    private String path;
    private Long websiteId;
    private Double sortKey;
    private boolean homePage;
    private String seoTitle;
    private String seoDescription;
    private String seoKeywords;
    private Long websiteSectionId;
    private String hash;
}



