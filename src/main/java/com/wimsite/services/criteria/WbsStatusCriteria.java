package com.wimsaas.wimapp.wimsite.services.criteria;

import lombok.Builder;
import lombok.Data;
import java.io.Serializable;

@Data
@Builder
public class WbsStatusCriteria implements Serializable {
    private Long id;
    private String description;
    private String name;
}
