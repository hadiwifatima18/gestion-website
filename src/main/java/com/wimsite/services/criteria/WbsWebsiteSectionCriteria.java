package com.wimsaas.wimapp.wimsite.services.criteria;

import lombok.Builder;
import lombok.Data;
import java.io.Serializable;

@Data
@Builder
public class WbsWebsiteSectionCriteria implements Serializable {
    private Long id;
    private Long layoutId;
    private Double sortKey;
    private String hash;
    private Long websiteId;
    private Long websitePageId;
    private Long sectionId;

}
