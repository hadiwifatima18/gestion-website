package com.wimsaas.wimapp.wimsite.services.criteria;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
public class WbsLayoutCriteria implements Serializable{

    private Long id;
    private String code;
    private Long sectionId;
    private String image;
}
