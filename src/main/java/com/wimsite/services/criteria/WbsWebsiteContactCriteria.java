package com.wimsaas.wimapp.wimsite.services.criteria;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class WbsWebsiteContactCriteria implements Serializable {
    private Long id;
    private String instagram;
    private String facebook;
    private String linkedIn;
    private String twitter;
    private String youtube;
    private String phoneNumbre;
    private String adresse;
    private String email;
    private Long websiteId;
}