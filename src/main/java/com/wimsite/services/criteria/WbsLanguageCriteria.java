package com.wimsaas.wimapp.wimsite.services.criteria;

import lombok.Builder;
import lombok.Data;
import java.io.Serializable;
@Data
@Builder
public class WbsLanguageCriteria implements Serializable {
    private String name;
    private String code;
    private String nameAr;

}
