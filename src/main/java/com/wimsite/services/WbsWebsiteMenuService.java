package com.wimsaas.wimapp.wimsite.services;

import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.starter.messages.MessageComponent;
import com.wimsaas.wimapp.wimnotify.services.SequenceGeneratorService;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteMenu;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsWebsiteMenuRepository;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsWebsiteMenuCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteDto;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteMenuDto;
import com.wimsaas.wimapp.wimsite.services.mappers.WbsWebsiteMenuMapper;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WbsWebsiteMenuService {

    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    private WbsWebsiteMenuMapper wbsWebsiteMenuMapper;
    @Autowired
    private WbsWebsiteMenuRepository wbsWebsiteMenuRepository;
    @Autowired
    private WbsWebsitePageService wbsWebsitePageService;
    @Autowired
    private WbsWebsiteService wbsWebsiteService;
    @Autowired
    private  MessageComponent messageComponent;

    public List<WbsWebsiteMenuDto> findWbsWebsiteMenuByCriteria(WbsWebsiteMenuCriteria wbsWebsiteMenuCriteria)throws WimBaseException {
        List<Document> criterias= new ArrayList<Document>();
        if (wbsWebsiteMenuCriteria.getId() != null) {
            criterias.add (new Document("id",wbsWebsiteMenuCriteria.getId()));
        }
        if (wbsWebsiteMenuCriteria.getName()!= null) {
            criterias.add(new Document("name",wbsWebsiteMenuCriteria.getName()));
        }
        if (wbsWebsiteMenuCriteria.getNameAr() != null) {
            criterias.add(new Document("name_ar",wbsWebsiteMenuCriteria.getNameAr()));
        }
        if (wbsWebsiteMenuCriteria.getWebsiteId()!= null) {
            criterias.add(new Document("website_id",wbsWebsiteMenuCriteria.getWebsiteId()));
        }
        if (wbsWebsiteMenuCriteria.getSortKey() != null) {
            criterias.add(new Document("sort_key", wbsWebsiteMenuCriteria.getSortKey()));
        }
        if (wbsWebsiteMenuCriteria.getWebsitePageId() != null) {
            criterias.add(new Document("websitePage_id", wbsWebsiteMenuCriteria.getWebsitePageId()));
        }
        if (criterias.isEmpty()) {
            criterias.add(new Document());
        }
        List<WbsWebsiteMenuDto> wbsWebsiteMenuDtoList = wbsWebsiteMenuMapper.toDtos(wbsWebsiteMenuRepository.findByQuery(new Document("$and", criterias)));

        for (WbsWebsiteMenuDto wbsWebsiteMenuDto : wbsWebsiteMenuDtoList) {
            if (wbsWebsiteMenuDto.getWebsitePageId() != null) {
                try {
                    wbsWebsiteMenuDto.setWbsWebsitePage(wbsWebsitePageService.findWbsWebsitePageById(wbsWebsiteMenuDto.getWebsitePageId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return wbsWebsiteMenuDtoList;
    }
    public WbsWebsiteMenuDto findWbsWebsiteMenuById(Long  id) throws WimBaseException {
        try{
            List<WbsWebsiteMenuDto> WbsWebsiteMenuItemsDtos=findWbsWebsiteMenuByCriteria(WbsWebsiteMenuCriteria.builder()
                    .id(id)
                    .build());
            if(WbsWebsiteMenuItemsDtos != null && ! WbsWebsiteMenuItemsDtos.isEmpty()){
                WbsWebsiteMenuDto wbsWebsiteMenuItemsDto=WbsWebsiteMenuItemsDtos.get(0);
                return wbsWebsiteMenuItemsDto;}
            else{
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        }
        catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsWebsiteMenuById)  id (" + id + ")", new RuntimeException(e));
        }
    }

    public WbsWebsiteMenuDto  persisteWbsWebsiteMenu(WbsWebsiteMenuDto wbsWebsiteMenuDto ) throws WimBaseException  {
        boolean result = validateWbsWebSiteMenu(wbsWebsiteMenuDto);
        if (result) {
            try {
                wbsWebsiteMenuDto.setId(Long.valueOf(sequenceGeneratorService.getSequenceNumber(WbsWebsiteMenu.SEQUENCE_NAME)));
                return wbsWebsiteMenuMapper.toDto(wbsWebsiteMenuRepository.save(wbsWebsiteMenuMapper.toEntity(wbsWebsiteMenuDto)) );
            } catch (Exception e) {
                throw new InternalErrorException("Erreur lors de l'exécution de cette méthode (persisteWbsWebsiteMenu)", new RuntimeException(e));
            }
        } else {
            throw new FunctionalException("La validation du Menu du site Web a échoué.");
        }
    }

    public WbsWebsiteMenuDto updateWbsWebsiteMenu(Long id, WbsWebsiteMenuDto wbsWebsiteMenuDto) throws WimBaseException {
        WbsWebsiteMenuDto oldWbsWebsiteMenuDto = findWbsWebsiteMenuById(id);
        wbsWebsiteMenuDto.setId(oldWbsWebsiteMenuDto.getId());
        boolean result = validateWbsWebSiteMenu(wbsWebsiteMenuDto);
        if(result){
            try{
                return wbsWebsiteMenuMapper.toDto(wbsWebsiteMenuRepository.save(wbsWebsiteMenuMapper.toEntity(wbsWebsiteMenuDto)));
            }
            catch(Exception e){
                throw new InternalErrorException("Error while executing updateWbsWebsiteMenu for id (" + id + ")", new RuntimeException(e));
            }
        }
        else {
            throw new FunctionalException("La validation de la modification du websiteMenu a échoué.");
        }
    }

    public ResponseDto deleteWbsWebsiteMenu(Long id)throws WimBaseException{
        try{
            wbsWebsiteMenuRepository.deleteById(id);
            ResponseDto responseDto=new ResponseDto();
            responseDto.setMessage(messageComponent.get("globale.element.deleted.message"));
            return responseDto;
        }
        catch (Exception e){
            throw new InternalErrorException("Error while executing deleteWbsWebsiteMenu for id (" + id + ")", new RuntimeException(e))  ;
        }
    }

    public boolean validateWbsWebSiteMenu(WbsWebsiteMenuDto wbsWebsiteMenuDto) throws WimBaseException {
        Long websiteId = wbsWebsiteMenuDto.getWebsiteId();
        Long websitePageId = wbsWebsiteMenuDto.getWebsitePageId();

        WbsWebsiteDto wbsWebsiteDto = wbsWebsiteService.findWbsWebsiteById(websiteId);
        if (wbsWebsiteDto == null) {
            throw new FunctionalException("Le website n'existe pas.");
        }
       /* WbsWebsitePageDto wbsWebsitePageDto = wbsWebsitePageService.findWbsWebsitePageById(websitePageId);
        if (wbsWebsitePageDto == null) {
            throw new FunctionalException("La websitePage n'existe pas.");
        }
        if (websiteId == null) {
            throw new FunctionalException("La website est obligatoire.");
        }
       if (websitePageId == null) {
            throw new FunctionalException("La websitePage est obligatoire.");
        }
        if (!wbsWebsitePageDto.getWebsiteId().equals(websiteId)) {
            throw new FunctionalException("Le websitePageId ne correspond pas au websiteId spécifié.");
        }*/
        return true;
    }

}