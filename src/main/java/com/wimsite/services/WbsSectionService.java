package com.wimsaas.wimapp.wimsite.services;

import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.starter.messages.MessageComponent;
import com.wimsaas.wimapp.wimnotify.services.SequenceGeneratorService;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsSection;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsLayoutRepository;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsSectionRepository;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsFontCriteria;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsLanguageCriteria;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsLayoutCriteria;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsSectionCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.*;
import com.wimsaas.wimapp.wimsite.services.mappers.WbsSectionMapper;
import org.bson.Document;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.*;


@Service
public class WbsSectionService {
        @Autowired
        private WbsSectionRepository wbsSectionRepository;
        @Autowired
        private WbsLanguageService wbsLanguageService;
        @Autowired
        private WbsSectionMapper wbsSectionMapper;
        @Autowired
        private SequenceGeneratorService sequenceGeneratorService;
        @Autowired
        private WbsLayoutService wbsLayoutService;
        @Autowired
        private WbsFontService wbsFontService;
        @Autowired
        private WbsLayoutRepository wbsLayoutRepository;
        @Autowired
        private  MessageComponent messageComponent;

       public WbsSectionDto findWbsSectionById(Long id) throws WimBaseException{
         try{
            List<WbsSectionDto> WbsSectionDtos=findWbsSectionByCriteria(WbsSectionCriteria.builder()
                    .id(id)
                    .build());
            if(WbsSectionDtos != null && ! WbsSectionDtos.isEmpty()){
                WbsSectionDto  wbsSectionDto=WbsSectionDtos.get(0);
                return wbsSectionDto;}
            else{
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
         }
         catch (FunctionalException e) {
             throw e;
         } catch (Exception e) {
             throw new InternalErrorException("Error while executing this method (findWbsSectionById)  id (" + id + ")", new RuntimeException(e));
         }
       }
    public List<WbsSectionDto> findWbsSectionByCriteria(WbsSectionCriteria wbsSectionCriteria) throws WimBaseException {
        List<Document> criterias = new ArrayList<Document>();

        if (wbsSectionCriteria.getId() != null) {
            criterias.add(new Document("id", wbsSectionCriteria.getId()));
        }
        if (wbsSectionCriteria.getCode() != null) {
            criterias.add(new Document("code", wbsSectionCriteria.getCode()));
        }
        if (wbsSectionCriteria.getNameAr() != null) {
            criterias.add(new Document("name_ar", wbsSectionCriteria.getNameAr()));
        }
        if (wbsSectionCriteria.getName() != null) {
            criterias.add(new Document("name", wbsSectionCriteria.getName()));
        }
        if (criterias.isEmpty()) {
            criterias.add(new Document());
        }
        List<WbsSectionDto> wbsSectionDtoList=wbsSectionMapper.toDtos(wbsSectionRepository.findByQuery(new Document("$and", criterias)));
        for (WbsSectionDto sectionDto : wbsSectionDtoList) {
            long sectionId = sectionDto.getId();
            int layoutCount =wbsLayoutRepository.countBySectionId(sectionId);
            sectionDto.setCountLayout(layoutCount);
        }
        return wbsSectionDtoList;
    }
    public WbsSectionDto persisteWbsSection(WbsSectionDto wbsSectionDto) {
        wbsSectionDto.setId(Long.valueOf(sequenceGeneratorService.getSequenceNumber(WbsSection.SEQUENCE_NAME)));
        return wbsSectionMapper.toDto(wbsSectionRepository.save(wbsSectionMapper.toEntity(wbsSectionDto)));
    }
    public WbsSectionDto updateWbsSection(Long id, WbsSectionDto wbsSectionDto) throws WimBaseException {
            try {
                WbsSectionDto oldWbsSectionDto = findWbsSectionById(id);
                wbsSectionDto.setId(oldWbsSectionDto.getId());

                return wbsSectionMapper.toDto(wbsSectionRepository.save(wbsSectionMapper.toEntity(wbsSectionDto)));
            } catch (Exception e) {
                throw new InternalErrorException("Error while executing updateWbsSection for id (" + id + ")", new RuntimeException(e));
            }
        }
    public ResponseDto deletewbsSection(Long id)throws WimBaseException{
            try{
                wbsSectionRepository.deleteById(id);
                ResponseDto responseDto=new ResponseDto();
                responseDto.setMessage(messageComponent.get("globale.element.deleted.message"));
                return responseDto;
            }
            catch (Exception e){
              throw new InternalErrorException("Error while executing deleteWbsSection for id (" + id + ")", new RuntimeException(e))  ;
            }
        }
    public SettingDto findAllSetting() throws WimBaseException{
        List<WbsSectionDto> sections=findWbsSectionByCriteria(WbsSectionCriteria.builder().build()) ;
        List<WbsFontDto> fonts=wbsFontService.findWbsFontByCriteria(WbsFontCriteria.builder().build());
        List<WbsLanguageDto> languages =wbsLanguageService.findWbsLanguageByCriteria(WbsLanguageCriteria.builder().build());
        List<WbsLayoutDto> layouts =wbsLayoutService.findWbsLayoutByCriteria(WbsLayoutCriteria.builder().build());
        SettingDto settingDto=new SettingDto();
        settingDto.setLayout(layouts);
        settingDto.setLanguage(languages);
        settingDto.setFont(fonts);
        settingDto.setSection(sections);
        return settingDto;
    }
}

