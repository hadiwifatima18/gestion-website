package com.wimsaas.wimapp.wimsite.services.component;


import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class SiteHashComponent {

    public String generateHash(String key) throws WimBaseException {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hashBytes = digest.digest(key.getBytes(StandardCharsets.UTF_8));
            StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < hashBytes.length / 3; i++) {
                byte b = hashBytes[i];
                String hex = String.format("%02x", b);
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing generateHash", new RuntimeException(e));
        }
    }
    public static String generateImageHash(String imageName) throws InternalErrorException {
        try {
            String input = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
            String hash  = DigestUtils.sha256Hex(input);
            int dotIndex = imageName.lastIndexOf(".");
            String extension = "";
            if (dotIndex >= 0) {
                extension = imageName.substring(dotIndex);
            }
            String shortHash = hash.substring(0, hash.length() / 3);
            String hashedImageName = shortHash + extension;
            return hashedImageName;

        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (generateImageHash)", e);
        }
    }
    public static String generateFileHash(String fileName) throws InternalErrorException {
        try {

            // Generate the MD5 hash
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] messageDigest = md.digest(fileName.getBytes());
            BigInteger bigInt = new BigInteger(1, messageDigest);
            String hash = bigInt.toString(16);
            int lengthHash = hash.length() / 3;
            String fileHash = hash.substring(0, lengthHash);
            int dotIndex = fileName.lastIndexOf(".");
            String extension = "";
            if (dotIndex >= 0) {
                extension = fileName.substring(dotIndex);
            }

            String hashedFileName = fileHash + extension;

            return hashedFileName;
        } catch (NoSuchAlgorithmException e) {
            throw new InternalErrorException("Error while executing this method (generateFileHash) ", new RuntimeException(e));

        }
    }
    public static String generateFolderHash() throws InternalErrorException {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            String input = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
            byte[] hashBytes = digest.digest(input.getBytes(StandardCharsets.UTF_8));

            StringBuilder hashBuilder = new StringBuilder();
            for (int i = 0; i < hashBytes.length /3; i++) {
                String hex = Integer.toHexString(0xff & hashBytes[i]);
                if (hex.length() == 1) {
                    hashBuilder.append('0');
                }
                hashBuilder.append(hex);
            }
            return hashBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new InternalErrorException("Error while executing this method (generateFolderHash) ", new RuntimeException(e));
        }
    }

}
