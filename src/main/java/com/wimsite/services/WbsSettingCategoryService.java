package com.wimsaas.wimapp.wimsite.services;

import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.starter.messages.MessageComponent;
import com.wimsaas.wimapp.wimnotify.services.SequenceGeneratorService;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsSettingCategory;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsSettingCategoryRepository;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsSettingCategoryCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.WbsSettingCategoryDto;
import com.wimsaas.wimapp.wimsite.services.mappers.WbsSettingCategoryMapper;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WbsSettingCategoryService {
    @Autowired
    private WbsSettingCategoryRepository wbsSettingCategoryRepository;
    @Autowired
    private WbsSettingCategoryMapper wbsSettingCategoryMapper;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    private final MessageComponent messageComponent;

    public WbsSettingCategoryDto findWbsSettingCategoryById(Long id) throws WimBaseException {
        try {
            List<WbsSettingCategoryDto> WbsSettingCategoryDto = findWbsSettingCategoryByCriteria(WbsSettingCategoryCriteria.builder()
                    .id(id)
                    .build());
            if (WbsSettingCategoryDto != null && !WbsSettingCategoryDto.isEmpty()) {
                WbsSettingCategoryDto wbsSettingCategoryDto = WbsSettingCategoryDto.get(0);
                return wbsSettingCategoryDto;
            } else {
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsWebSiteContactId)  id (" + id + ")", new RuntimeException(e));
        }
    }
    public List<WbsSettingCategoryDto> findWbsSettingCategoryByCriteria(WbsSettingCategoryCriteria wbsSettingCategoryCriteria) throws WimBaseException {
        List<Document> criterias = new ArrayList<Document>();
        if (wbsSettingCategoryCriteria.getId() != null) {
            criterias.add(new Document("id", wbsSettingCategoryCriteria.getId()));
        }
        if (wbsSettingCategoryCriteria.getCode() != null) {
            criterias.add(new Document("code", wbsSettingCategoryCriteria.getCode()));
        }
        if (wbsSettingCategoryCriteria.getName() != null) {
            criterias.add(new Document("name", wbsSettingCategoryCriteria.getName()));
        }
        if (wbsSettingCategoryCriteria.getNameAr() != null) {
            criterias.add(new Document("name_ar", wbsSettingCategoryCriteria.getNameAr()));
        }
        if (wbsSettingCategoryCriteria.getDescription() != null) {
            criterias.add(new Document("description", wbsSettingCategoryCriteria.getDescription()));
        }
        if (wbsSettingCategoryCriteria.getDateCreation() != null) {
            criterias.add(new Document("date_creation", wbsSettingCategoryCriteria.getDateCreation()));
        }
        if (wbsSettingCategoryCriteria.getDateUpdate() != null) {
            criterias.add(new Document("date_update", wbsSettingCategoryCriteria.getDateUpdate()));
        }
        if (wbsSettingCategoryCriteria.getSortKey() != null) {
            criterias.add(new Document("sort_key", wbsSettingCategoryCriteria.getSortKey()));
        }
        if (criterias.isEmpty()) {
            criterias.add(new Document());
        }
        return wbsSettingCategoryMapper.toDtos(wbsSettingCategoryRepository.findByQuery(new Document("$and", criterias)));
    }

    public WbsSettingCategoryDto persisteWbsSettingCategory(WbsSettingCategoryDto wbsSettingCategoryDto) {
        wbsSettingCategoryDto.setId(Long.valueOf(sequenceGeneratorService.getSequenceNumber(WbsSettingCategory.SEQUENCE_NAME)));
        wbsSettingCategoryDto.setDateCreation(LocalDateTime.now());
        return wbsSettingCategoryMapper.toDto(wbsSettingCategoryRepository.save(wbsSettingCategoryMapper.toEntity(wbsSettingCategoryDto)));
    }
    public WbsSettingCategoryDto updateWbsSettingCategory(Long id, WbsSettingCategoryDto wbsSettingCategoryDto) throws WimBaseException {
        try {
            WbsSettingCategoryDto oldWbsSettingCategoryDto = findWbsSettingCategoryById(id);
            wbsSettingCategoryDto.setId(oldWbsSettingCategoryDto.getId());
            wbsSettingCategoryDto.setDateUpdate(LocalDateTime.now());
            return wbsSettingCategoryMapper.toDto(wbsSettingCategoryRepository.save(wbsSettingCategoryMapper.toEntity(wbsSettingCategoryDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing updateWbsSettingCategory for id (" + id + ")", new RuntimeException(e));
        }
    }
    public ResponseDto deleteWbsSettingCategory(Long id)throws WimBaseException {
        try{
            wbsSettingCategoryRepository.deleteById(id);
            ResponseDto responseDto=new ResponseDto();
            responseDto.setMessage(messageComponent.get("globale.element.deleted.message"));
            return responseDto;
        }
        catch (Exception e){
            throw new InternalErrorException("Error while executing deleteWbsSettingCategory for id (" + id + ")", new RuntimeException(e))  ;
        }
    }
}