package com.wimsaas.wimapp.wimsite.services;

import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.starter.messages.MessageComponent;
import com.wimsaas.wimapp.wimnotify.services.SequenceGeneratorService;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsFont;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsFontRepository;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsFontCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.WbsFontDto;
import com.wimsaas.wimapp.wimsite.services.mappers.WbsFontMapper;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class WbsFontService {
    @Autowired
    private WbsFontMapper wbsFontMapper;
    @Autowired
    private WbsFontRepository wbsFontRepository;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    private MessageComponent messageComponent;

    public WbsFontDto findWbsFontById(Long id) throws WimBaseException {
        try {
            List<WbsFontDto> wbsFontDtos = findWbsFontByCriteria(WbsFontCriteria.builder()
                    .id(id)
                    .build());
            if (wbsFontDtos != null && !wbsFontDtos.isEmpty()) {
                WbsFontDto wbsFontDto = wbsFontDtos.get(0);
                return wbsFontDto;
            } else {
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsFontById)  id (" + id + ")", new RuntimeException(e));
        }
    }

    public List<WbsFontDto> findWbsFontByCriteria(WbsFontCriteria wbsFontCriteria) throws WimBaseException {
        List<Document> criterias = new ArrayList<Document>();
        if (wbsFontCriteria.getId() != null) {
            criterias.add(new Document("id", wbsFontCriteria.getId()));
        }
        if (wbsFontCriteria.getUrlFont() != null) {
            criterias.add(new Document("url", wbsFontCriteria.getUrlFont()));
        }

        if (wbsFontCriteria.getNameFont() != null) {
            criterias.add(new Document("name", wbsFontCriteria.getNameFont()));
        }

        if (wbsFontCriteria.getThumbnail() != null) {
            criterias.add(new Document("thumbnail", wbsFontCriteria.getThumbnail()));
        }
        if (wbsFontCriteria.getArabic() != null) {
            criterias.add(new Document("arabic", wbsFontCriteria.getArabic()));
        }
        if (criterias.isEmpty()) {
            criterias.add(new Document());
        }
        return wbsFontMapper.toDtos(wbsFontRepository.findByQuery(new Document("$and", criterias)));
    }

    public WbsFontDto persisteWbsFont(WbsFontDto wbsFontDto) {
        wbsFontDto.setId(Long.valueOf(sequenceGeneratorService.getSequenceNumber(WbsFont.SEQUENCE_NAME)));
        return wbsFontMapper.toDto(wbsFontRepository.save(wbsFontMapper.toEntity(wbsFontDto)));
    }

    public WbsFontDto updateWbsFont(Long id, WbsFontDto wbsFontDto) throws WimBaseException {
        try {
            WbsFontDto oldwbsFontDto = findWbsFontById(id);
            wbsFontDto.setId(oldwbsFontDto.getId());
            return wbsFontMapper.toDto(wbsFontRepository.save(wbsFontMapper.toEntity(wbsFontDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing updateWbsFont for id (" + id + ")", new RuntimeException(e));
        }
    }

    public ResponseDto deleteWbsFont(Long id) throws WimBaseException {
        try {
            wbsFontRepository.deleteById(id);
            ResponseDto responseDto = new ResponseDto();
            responseDto.setMessage(messageComponent.get("globale.element.deleted.message"));
            return responseDto;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing deleteWbsFont for id (" + id + ")", new RuntimeException(e));
        }
    }
}
