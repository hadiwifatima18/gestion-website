package com.wimsaas.wimapp.wimsite.services;

import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.starter.messages.MessageComponent;
import com.wimsaas.wimapp.wimnotify.services.SequenceGeneratorService;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteContact;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsWebsiteContactRepository;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsWebsiteContactCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteContactDto;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteDto;
import com.wimsaas.wimapp.wimsite.services.mappers.WbsWebsiteContactMapper;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import static com.wimsaas.wimapp.wimsite.utils.WimSiteValidator.*;

@Service
@RequiredArgsConstructor
public class WbsWebsiteContactService {
    
    @Autowired
    private WbsWebsiteContactRepository wbsWebsiteContactRepository;
    @Autowired
    private WbsWebsiteService wbsWebsiteService;
    @Autowired
    private WbsWebsiteContactMapper wbsWebsiteContactMapper;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    private final MessageComponent messageComponent;


    public WbsWebsiteContactDto findWbsWebsiteContactById(Long id) throws WimBaseException {
        try {
            List<WbsWebsiteContactDto> WbsWebSiteContactDto = findWbsWebsiteContactByCriteria(WbsWebsiteContactCriteria.builder()
                    .id(id)
                    .build());
            if (WbsWebSiteContactDto != null && !WbsWebSiteContactDto.isEmpty()) {
                WbsWebsiteContactDto wbsWebsiteContactDto = WbsWebSiteContactDto.get(0);
                return wbsWebsiteContactDto;
            } else {
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsWebSiteContactId)  id (" + id + ")", new RuntimeException(e));
        }
    }

    public List<WbsWebsiteContactDto> findWbsWebsiteContactByCriteria(WbsWebsiteContactCriteria wbsWebsiteContactCriteria) throws WimBaseException {
        List<Document> criterias = new ArrayList<Document>();
        if (wbsWebsiteContactCriteria.getId() != null) {
            criterias.add(new Document("id", wbsWebsiteContactCriteria.getId()));
        }
        if (wbsWebsiteContactCriteria.getInstagram() != null) {
            criterias.add(new Document("instagram", wbsWebsiteContactCriteria.getInstagram()));
        }
        if (wbsWebsiteContactCriteria.getFacebook() != null) {
            criterias.add(new Document("facebook", wbsWebsiteContactCriteria.getFacebook()));
        }
        if (wbsWebsiteContactCriteria.getLinkedIn() != null) {
            criterias.add(new Document("linked_in", wbsWebsiteContactCriteria.getLinkedIn()));
        }
        if (wbsWebsiteContactCriteria.getTwitter() != null) {
            criterias.add(new Document("twitter", wbsWebsiteContactCriteria.getTwitter()));
        }
        if (wbsWebsiteContactCriteria.getYoutube() != null) {
            criterias.add(new Document("youtube", wbsWebsiteContactCriteria.getYoutube()));
        }
        if (wbsWebsiteContactCriteria.getPhoneNumbre()!= null) {
            criterias.add(new Document("phone_number", wbsWebsiteContactCriteria.getPhoneNumbre()));
        }
        if (wbsWebsiteContactCriteria.getAdresse() != null) {
            criterias.add(new Document("address", wbsWebsiteContactCriteria.getAdresse()));
        }
        if (wbsWebsiteContactCriteria.getEmail() != null) {
            criterias.add(new Document("email", wbsWebsiteContactCriteria.getEmail()));
        }
        if (wbsWebsiteContactCriteria.getWebsiteId() != null) {
            criterias.add(new Document("website_id", wbsWebsiteContactCriteria.getWebsiteId()));
        }
        if (criterias.isEmpty()) {
            criterias.add(new Document());
        }
        List<WbsWebsiteContactDto> listWbsWebSiteContactDto = wbsWebsiteContactMapper.toDtos(wbsWebsiteContactRepository.findByQuery(new Document("$and", criterias)));
        for(WbsWebsiteContactDto wbsWebsiteContactDto:listWbsWebSiteContactDto){
            if(wbsWebsiteContactDto.getWebsiteId()!=null){
                try{
                    wbsWebsiteContactDto.setWebsite(wbsWebsiteService.findWbsWebsiteById(wbsWebsiteContactDto.getWebsiteId()));
                } catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
        return listWbsWebSiteContactDto;
    }
    public WbsWebsiteContactDto persisteWbsWebsiteContact(WbsWebsiteContactDto wbsWebsiteContactDto) throws WimBaseException {
        boolean result = validateWbsWebsiteContact(wbsWebsiteContactDto);
        boolean contactExists = isContactExistsForWebsite(wbsWebsiteContactDto.getWebsiteId());

        if (!contactExists) {
            if (result) {
                try {
                    wbsWebsiteContactDto.setId(Long.valueOf(sequenceGeneratorService.getSequenceNumber(WbsWebsiteContact.SEQUENCE_NAME)));
                    return wbsWebsiteContactMapper.toDto(wbsWebsiteContactRepository.save(wbsWebsiteContactMapper.toEntity(wbsWebsiteContactDto)));
                } catch (Exception e) {
                    throw new InternalErrorException("Erreur lors de l'exécution de cette méthode (persisteWbsWebsiteContact)", new RuntimeException(e));
                }
            } else {
                throw new FunctionalException("La validation de websiteContact a échoué.");
            }
        } else {
            List<WbsWebsiteContactDto> existingContacts = findWbsWebsiteContactByCriteria(WbsWebsiteContactCriteria.builder()
                    .websiteId(wbsWebsiteContactDto.getWebsiteId())
                    .build());
                WbsWebsiteContactDto existingContact = existingContacts.get(0);
                return updateWbsWebsiteContact(existingContact.getId(), wbsWebsiteContactDto);
            }
    }
     public WbsWebsiteContactDto updateWbsWebsiteContact(Long id, WbsWebsiteContactDto wbsWebsiteContactDto) throws WimBaseException {
        boolean result= validateWbsWebsiteContact(wbsWebsiteContactDto);
        if(result) {
            try {
                WbsWebsiteContactDto oldWbsWebSiteContactDto = findWbsWebsiteContactById(id);
                wbsWebsiteContactDto.setId(oldWbsWebSiteContactDto.getId());
                return wbsWebsiteContactMapper.toDto(wbsWebsiteContactRepository.save(wbsWebsiteContactMapper.toEntity(wbsWebsiteContactDto)));
            } catch (Exception e) {
                throw new InternalErrorException("Error while executing updateWbsWebsiteContact for id (" + id + ")", new RuntimeException(e));
            }
        }else {
            throw new FunctionalException("La validation de la modification du websiteContact a échoué.");
        }
    }
    public ResponseDto deleteWbsWebsiteContact(Long id) throws WimBaseException {
        try {
            wbsWebsiteContactRepository.deleteById(id);
            ResponseDto responseDto = new ResponseDto();
            responseDto.setMessage(messageComponent.get("globale.element.deleted.message"));
            return responseDto;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing deleteWbsWebsiteContact for id (" + id + ")", new RuntimeException(e));
        }
    }
    public boolean validateWbsWebsiteContact(WbsWebsiteContactDto wbsWebsiteContactDto ) throws WimBaseException {
        String phoneNumber=wbsWebsiteContactDto.getPhoneNumbre();
        String email=wbsWebsiteContactDto.getEmail();
        Long websiteId = wbsWebsiteContactDto.getWebsiteId();
        boolean validPhoneNumber = isValidPhoneNumber(phoneNumber);
        boolean validEmail=isValidEmailAddress(email);
        boolean websiteExists=false;

        try {
            WbsWebsiteDto wbsWebsiteDto = wbsWebsiteService.findWbsWebsiteById(websiteId);
            websiteExists = wbsWebsiteDto != null;
        } catch (FunctionalException e) {
            if (!websiteExists)
                throw new FunctionalException("Le website n'existe pas.");
        }
        if (!validPhoneNumber) {
            throw new FunctionalException("Le numero de téléphone est invalide");
        }
        if (!validEmail) {
            throw new FunctionalException("L'email est invalide ");
        }
        if(validPhoneNumber && validEmail && websiteExists)
            return true;
        return false;
    }
    public boolean isContactExistsForWebsite(Long websiteId) throws WimBaseException {
        List<WbsWebsiteContactDto> wbsWebSiteContactDto = findWbsWebsiteContactByCriteria(WbsWebsiteContactCriteria.builder()
                .websiteId(websiteId)
                .build());
        if (wbsWebSiteContactDto != null && !wbsWebSiteContactDto.isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }

}






