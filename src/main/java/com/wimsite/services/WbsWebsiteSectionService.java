package com.wimsaas.wimapp.wimsite.services;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.starter.messages.MessageComponent;
import com.wimsaas.wimapp.wimfile.config.properties.space.SpaceProperties;
import com.wimsaas.wimapp.wimfile.services.scheduler.catalog.FleFileService;
import com.wimsaas.wimapp.wimfile.services.scheduler.catalog.FilesService;
import com.wimsaas.wimapp.wimnotify.services.SequenceGeneratorService;

import java.io.InputStream;
import java.util.function.Function;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteSection;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsWebsiteRepository;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsWebsiteSectionRepository;
import com.wimsaas.wimapp.wimsite.services.component.SiteHashComponent;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsLayoutCriteria;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsWebsiteSectionCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.*;
import com.wimsaas.wimapp.wimsite.services.mappers.WbsWebsiteSectionMapper;
import com.wimsaas.wimapp.wimsite.utils.WimSiteConstants;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WbsWebsiteSectionService {
    @Autowired
    private WbsSectionService wbsSectionService;
    @Autowired
    private WbsWebsiteSectionRepository wbsWebsiteSectionRepository;
    @Autowired
    private WbsWebsiteSectionMapper wbsWebsiteSectionMapper;
    @Autowired
    private WbsWebsiteService wbsWebsiteService;
    @Autowired
    private WbsWebsitePageService wbsWebsitePageService;
    @Autowired
    private WbsLayoutService wbsSectionLayoutService;
    @Autowired
    private FilesService filesService;
    @Autowired
    private FleFileService fleFileService;
    @Autowired
    private SpaceProperties spaceProperties;
    @Autowired
    private SiteHashComponent siteHashComponent;
    @Autowired
    private WbsWebsiteRepository wbsWebsiteRepository;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    private  MessageComponent messageComponent;

    public List<WbsWebsiteSectionDto> findWbsWebsiteSectionByCriteria(WbsWebsiteSectionCriteria wbsWebsiteSectionCriteria)throws WimBaseException {
        List<Document> criterias= new ArrayList<Document>();
        if (wbsWebsiteSectionCriteria.getId() != null) {
            criterias.add (new Document("id",wbsWebsiteSectionCriteria.getId()));
        }
        if (wbsWebsiteSectionCriteria.getSortKey()!= null) {
            criterias.add(new Document("sort_key",wbsWebsiteSectionCriteria.getSortKey()));
        }
        if (wbsWebsiteSectionCriteria.getLayoutId()!= null) {
            criterias.add(new Document("layout_id",wbsWebsiteSectionCriteria.getLayoutId()));
        }
        if (wbsWebsiteSectionCriteria.getWebsiteId()!= null) {
            criterias.add(new Document("website_id",wbsWebsiteSectionCriteria.getWebsiteId()));
        }
        if (wbsWebsiteSectionCriteria.getWebsitePageId()!= null) {
            criterias.add(new Document("website_page_id",wbsWebsiteSectionCriteria.getWebsitePageId()));
        }
        if (wbsWebsiteSectionCriteria.getSectionId()!= null) {
            criterias.add(new Document("section_id",wbsWebsiteSectionCriteria.getSectionId()));
        }
        if (wbsWebsiteSectionCriteria.getHash()!= null) {
            criterias.add(new Document("hash",wbsWebsiteSectionCriteria.getHash()));
        }
        if(criterias.isEmpty()){
            criterias.add(new Document());
        }
        List<WbsWebsiteSectionDto> wbsWebsiteSectionDtoList = wbsWebsiteSectionMapper.toDtos(wbsWebsiteSectionRepository.findByQuery(new Document("$and", criterias)));
        for(WbsWebsiteSectionDto wbsWebsiteSectionDto: wbsWebsiteSectionDtoList){
            if(wbsWebsiteSectionDto.getSectionId()!=null){
                WbsLayoutDto wbsLayoutDto1=wbsSectionLayoutService.findWbsLayoutById(wbsWebsiteSectionDto.getLayoutId());
                wbsLayoutDto1.setData("{}");
                wbsLayoutDto1.setDataAr("{}");
                wbsLayoutDto1.setDataEn("{}");
                wbsWebsiteSectionDto.setLayout(wbsLayoutDto1);
            }
            
            if(wbsWebsiteSectionDto.getLayout().getSectionId()!=null) {
                List<WbsLayoutDto> wbsLayoutDtoList = wbsSectionLayoutService.findWbsLayoutBySectionId(wbsWebsiteSectionDto.getLayout().getSectionId());
                for (WbsLayoutDto wbsLayoutDto : wbsLayoutDtoList) {
                    wbsLayoutDto.setData("{}");
                    wbsLayoutDto.setDataAr("{}");
                    wbsLayoutDto.setDataEn("{}");
                    if (wbsLayoutDto.getId().equals(wbsWebsiteSectionDto.getLayoutId())) {
                        wbsLayoutDto.setSelected(true);
                    } else {
                        wbsLayoutDto.setSelected(false);
                    }
                }
                wbsWebsiteSectionDto.setAvailableLayout(wbsLayoutDtoList);
            }
        }
        return wbsWebsiteSectionDtoList;
    }
    public WbsWebsiteSectionDto findWbsWebsiteSectionById(Long  id) throws WimBaseException {
        try{
            List<WbsWebsiteSectionDto> wbsWebsiteSectionDto=findWbsWebsiteSectionByCriteria(WbsWebsiteSectionCriteria.builder()
                    .id(id)
                    .build());
            if(wbsWebsiteSectionDto != null && ! wbsWebsiteSectionDto.isEmpty()){
                WbsWebsiteSectionDto wbsWebsiteSectionDtos=wbsWebsiteSectionDto.get(0);
                return wbsWebsiteSectionDtos;}
            else{
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));

            }
        }
        catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsWebsiteSectionById)  id (" + id + ")", new RuntimeException(e));
        }
    }
    public WbsWebsiteSectionDto findWbsWebsiteSectionByHash(String hash) throws WimBaseException {
        try{
            List<WbsWebsiteSectionDto> wbsWebsiteSectionDto=findWbsWebsiteSectionByCriteria(WbsWebsiteSectionCriteria.builder()
                    .hash(hash)
                    .build());
            if(wbsWebsiteSectionDto != null && ! wbsWebsiteSectionDto.isEmpty()){
                WbsWebsiteSectionDto wbsWebsiteSectionDtos=wbsWebsiteSectionDto.get(0);
                return wbsWebsiteSectionDtos;}
            else{
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));

            }
        }
        catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsWebsiteSectionByHash)  id (" + hash + ")", new RuntimeException(e));
        }
    }
    public WbsWebsiteSectionDto  persisteWbsWebsiteSection(WbsWebsiteSectionDto wbsWebsiteSectionDto) throws WimBaseException{
        WbsSectionDto wbsSectionDto=wbsSectionService.findWbsSectionById(wbsWebsiteSectionDto.getSectionId());
        if(wbsSectionDto.getCode().equals("header")){
            List<WbsWebsiteSectionDto> wbsWebsiteSectionDtoList=findWbsWebsiteSectionByCriteria(WbsWebsiteSectionCriteria.builder()
                    .websitePageId(wbsWebsiteSectionDto.getWebsitePageId())
                    .build());
            for (WbsWebsiteSectionDto websiteSectionDto : wbsWebsiteSectionDtoList) {
                WbsSectionDto sectionDto = websiteSectionDto.getSection();
                if (sectionDto != null && "header".equals(sectionDto.getCode())) {
                    throw new FunctionalException("This page already contains a header section.");
                }
            }
        }
        wbsWebsiteSectionDto.setId(Long.valueOf(sequenceGeneratorService.getSequenceNumber(WbsWebsiteSection.SEQUENCE_NAME)));
        wbsWebsiteSectionDto.setHash(generateHashForWbsWebsiteSection(wbsWebsiteSectionDto));
        Long sectionId = wbsWebsiteSectionDto.getSectionId();
        JSONObject data = mergeDataLayoutsInWbsWebsiteSection(sectionId, WbsLayoutDto::getDataAsJson);
        JSONObject dataAr = mergeDataLayoutsInWbsWebsiteSection(sectionId, WbsLayoutDto::getDataArAsJson);
        JSONObject dataEn = mergeDataLayoutsInWbsWebsiteSection(sectionId, WbsLayoutDto::getDataEnAsJson);
        wbsWebsiteSectionDto.setData(data.toString());
        wbsWebsiteSectionDto.setDataAr(dataAr.toString());
        wbsWebsiteSectionDto.setDataEn(dataEn.toString());
        return wbsWebsiteSectionMapper.toDto(wbsWebsiteSectionRepository.save(wbsWebsiteSectionMapper.toEntity(wbsWebsiteSectionDto)));
    }
    public String generateHashForWbsWebsiteSection(WbsWebsiteSectionDto wbsWebsiteSectionDto) throws WimBaseException {
        String input = String.valueOf(wbsWebsiteSectionDto.getWebsitePageId()) + String.valueOf(wbsWebsiteSectionDto.getId());
        return siteHashComponent.generateHash(input);
    }
    public WbsWebsiteSectionDto updateWbsWebsiteSection(Long id, WbsWebsiteSectionDto wbsWebsiteSectionDto) throws WimBaseException {
        try {
            WbsWebsiteSectionDto oldWbsWebsiteSectionDto = findWbsWebsiteSectionById(id);
            wbsWebsiteSectionDto.setId(oldWbsWebsiteSectionDto.getId());
            return wbsWebsiteSectionMapper.toDto(wbsWebsiteSectionRepository.save(wbsWebsiteSectionMapper.toEntity( wbsWebsiteSectionDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing updateWbsWebsiteSection for id (" + id + ")", new RuntimeException(e));
        }
    }
    public ResponseDto deleteWbsWebsiteSection(Long id)throws WimBaseException{
        try{
            wbsWebsiteSectionRepository.deleteById(id);
            ResponseDto responseDto=new ResponseDto();
            responseDto.setMessage(messageComponent.get("globale.element.deleted.message"));
            return responseDto;
        }
        catch (Exception e){
            throw new InternalErrorException("Error while executing deleteWbsWebsiteSectionDto for id (" + id + ")", new RuntimeException(e))  ;
        }
    }
    public ResponseDto deleteWbsWebsiteSectionByHash(String hash) throws WimBaseException {
        try {
            List<WbsWebsiteSectionDto> wbsWebsiteSectionDto = findWbsWebsiteSectionByCriteria(WbsWebsiteSectionCriteria.builder()
                    .hash(hash)
                    .build());
            if (wbsWebsiteSectionDto != null && !wbsWebsiteSectionDto.isEmpty()) {
                WbsWebsiteSectionDto wbsWebsiteSectionDtos = wbsWebsiteSectionDto.get(0);
                wbsWebsiteSectionRepository.deleteById(Long.valueOf(wbsWebsiteSectionDtos.getId()));
                ResponseDto responseDto = new ResponseDto();
                responseDto.setMessage(messageComponent.get("globale.element.deleted.message"));
                return responseDto;
            }else {
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing deleteWbsWebsiteSectionByHash for id (" + hash + ")", new RuntimeException(e));
        }
    }
    public boolean validateWbsWebSiteSection(WbsWebsiteSectionDto wbsWebsiteSectionDto) throws WimBaseException {
        Long websiteId=wbsWebsiteSectionDto.getWebsiteId();
        Long websitePageId=wbsWebsiteSectionDto.getWebsitePageId();
        Long loyoutId=wbsWebsiteSectionDto.getLayoutId();
        Long sectionId=wbsWebsiteSectionDto.getSectionId();
        boolean websiteExists = false;
        boolean websitePageExists = false;
        boolean layoutExists = false;
        boolean sectionExists = false;
        try {
            WbsWebsiteDto wbsWebsiteDto = wbsWebsiteService.findWbsWebsiteById(websiteId);
            websiteExists = wbsWebsiteDto != null;
        } catch (FunctionalException e) {
            if (!websiteExists)
                throw new FunctionalException("Le website n'existe pas.");
        }
        try {
            WbsWebsitePageDto wbsWebsitePageDto = wbsWebsitePageService.findWbsWebsitePageById(websitePageId);
            websitePageExists = wbsWebsitePageDto != null;
        } catch (FunctionalException e) {
            if (!websitePageExists)
                throw new FunctionalException("La websitePage n'existe pas.");
        }
        try {
            WbsLayoutDto wbsSectionLayoutDto = wbsSectionLayoutService.findWbsLayoutById(loyoutId);
            layoutExists = wbsSectionLayoutDto != null;
        } catch (FunctionalException e) {
            if (!layoutExists)
                throw new FunctionalException("Le layout n'existe pas.");
        }
        try {
            WbsSectionDto wbsSectionDto = wbsSectionService.findWbsSectionById(sectionId);
            sectionExists = wbsSectionDto != null;
        } catch (FunctionalException e) {
            if (!sectionExists)
                throw new FunctionalException("Le section n'existe pas.");
        }
        if (websiteExists && websitePageExists && layoutExists && sectionExists)
            return true;
        return false;
    }
    public JSONObject mergeDataLayoutsInWbsWebsiteSection(Long idSection, Function<WbsLayoutDto, JSONObject> dataExtractor) throws WimBaseException {
        List<WbsLayoutDto> listLayouts = wbsSectionLayoutService.findWbsLayoutByCriteria(WbsLayoutCriteria.builder()
                .sectionId(idSection)
                .build());
        JSONObject mergedData = new JSONObject();
        for (WbsLayoutDto layout : listLayouts) {
            JSONObject jsonData = dataExtractor.apply(layout);
            mergeJsonObjects(mergedData, jsonData);
        }
        return mergedData;
    }
    private void mergeJsonObjects(JSONObject baseObject, JSONObject newObject) {
        for (String key : newObject.keySet()) {
            baseObject.put(key, newObject.get(key));
        }
    }
    public WbsWebsiteSectionDto updateWbsWebsiteSectionDataByKeyValue(Long id, String key, String value,String code) throws WimBaseException {
        try {
            WbsWebsiteSectionDto oldWbsWebsiteSectionDto = findWbsWebsiteSectionById(id);
            if ("fr".equals(code)) {
                JSONObject jsonData = oldWbsWebsiteSectionDto.getDataAsJson();
                jsonData.put(key, value);
                oldWbsWebsiteSectionDto.setDataFromJson(jsonData);
            } else if ("ar".equals(code)) {
                JSONObject jsonData = oldWbsWebsiteSectionDto.getDataArAsJson();
                jsonData.put(key, value);
                oldWbsWebsiteSectionDto.setDataArFromJson(jsonData);
            }else if ("en".equals(code)) {
                JSONObject jsonData = oldWbsWebsiteSectionDto.getDataEnAsJson();
                jsonData.put(key, value);
                oldWbsWebsiteSectionDto.setDataEnFromJson(jsonData);
            }else {
                throw new IllegalArgumentException("Invalid language code value");
            }
            return updateWbsWebsiteSection(id, oldWbsWebsiteSectionDto);
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing updateWbsWebsiteSectionDataByKeyValue for id (" + id + ")", new RuntimeException(e));
        }
    }
    public WbsWebsiteSectionDto updateWbsWebsiteSectionData(Long id,String code,JSONObject data) throws WimBaseException {
        try {
            WbsWebsiteSectionDto oldWbsWebsiteSectionDto = findWbsWebsiteSectionById(id);
            if ("fr".equals(code)) {
                oldWbsWebsiteSectionDto.setDataFromJson(data);
            } else if ("ar".equals(code)) {
                oldWbsWebsiteSectionDto.setDataArFromJson(data);
            }else if ("en".equals(code)) {
                oldWbsWebsiteSectionDto.setDataEnFromJson(data);
            }else {
                throw new IllegalArgumentException("Invalid language code value");
            }
            return updateWbsWebsiteSection(id, oldWbsWebsiteSectionDto);
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing updateWbsWebsiteSectionJsonData for id (" + id + ")", new RuntimeException(e));
        }
    }
    public WbsWebsiteSectionDto setLayoutForSetion(Long websiteSectionId, WbsLayoutDto wbsLayoutDto)throws WimBaseException{
        try {
            WbsWebsiteSectionDto wbsWebsiteSectionDto = findWbsWebsiteSectionById(websiteSectionId);
            wbsWebsiteSectionDto.setLayoutId(wbsLayoutDto.getId());
            return wbsWebsiteSectionMapper.toDto(wbsWebsiteSectionRepository.save(wbsWebsiteSectionMapper.toEntity(wbsWebsiteSectionDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing setLayoutForSetion for id (" + websiteSectionId + ")", new RuntimeException(e));
        }
    }
    public WbsWebsiteSectionDto uploadWbsWebsiteSectionImage(Long websiteId,MultipartFile image, String websiteSectionHash,String languageCode,String jsonPath) throws WimBaseException {
        try {
            WbsWebsiteSectionDto oldWebsiteSectionDto=findWbsWebsiteSectionByHash(websiteSectionHash);
            WbsWebsiteDto websiteDto=wbsWebsiteService.findWbsWebsiteById(websiteId);
            String defaultBaseFolder=websiteDto.getFolderHash();
            String completBaseFolder= WimSiteConstants.SPACE_WIMSTE_PREFIX +"/"+defaultBaseFolder ;
            String baseFolder;
            if (completBaseFolder.equals("0")) {
                baseFolder = "";
            } else {
                baseFolder = completBaseFolder + "/";
            }
            String hashImage= siteHashComponent.generateImageHash(image.getOriginalFilename());
            InputStream input = image.getInputStream();
            String pathImage=baseFolder + hashImage;
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(image.getContentType());
            PutObjectRequest request = new PutObjectRequest(spaceProperties.getBucketName(), pathImage, input, metadata).withCannedAcl(CannedAccessControlList.PublicRead);
            filesService.spaceS3().putObject(request);
            fleFileService.persistFleFile(image,websiteId,oldWebsiteSectionDto.getId());
            if ("fr".equals(languageCode)){
                JSONObject jsonData = oldWebsiteSectionDto.getDataAsJson();
                jsonData.put(jsonPath, pathImage);
                oldWebsiteSectionDto.setDataFromJson(jsonData);
            }
            if ("ar".equals(languageCode)){
                JSONObject jsonDataAr = oldWebsiteSectionDto.getDataArAsJson();
                jsonDataAr.put(jsonPath, pathImage);
                oldWebsiteSectionDto.setDataArFromJson(jsonDataAr);
            }
            if ("en".equals(languageCode)){
                JSONObject jsonDataEn = oldWebsiteSectionDto.getDataEnAsJson();
                jsonDataEn.put(jsonPath, pathImage);
                oldWebsiteSectionDto.setDataEnFromJson(jsonDataEn);
            }
            return wbsWebsiteSectionMapper.toDto(wbsWebsiteSectionRepository.save(wbsWebsiteSectionMapper.toEntity(oldWebsiteSectionDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (uploadWbsWebsiteSectionImage)", new RuntimeException(e));
        }
    }
}







