package com.wimsaas.wimapp.wimsite.services;

import com.wimsaas.starter.dtos.ResponseDto;
import com.wimsaas.starter.exceptions.business.FunctionalException;
import com.wimsaas.starter.exceptions.business.InternalErrorException;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.starter.messages.MessageComponent;
import com.wimsaas.wimapp.wimnotify.services.SequenceGeneratorService;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsLayout;
import com.wimsaas.wimapp.wimsite.dao.repositories.WbsLayoutRepository;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsLayoutCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.WbsLayoutDto;
import com.wimsaas.wimapp.wimsite.services.mappers.WbsLayoutMapper;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WbsLayoutService {
    @Autowired
    private WbsLayoutRepository wbsLayoutRepository;
    @Autowired
    private WbsLayoutMapper wbsLayoutMapper;
    @Autowired
    private WbsSectionService wbsSectionService;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    private MessageComponent messageComponent;

    public List<WbsLayoutDto> findWbsLayoutByCriteria(WbsLayoutCriteria wbsLayoutCriteria) throws WimBaseException {
        List<Document> criterias = new ArrayList<Document>();
        if (wbsLayoutCriteria.getId() != null) {
            criterias.add(new Document("id", wbsLayoutCriteria.getId()));
        }
        if (wbsLayoutCriteria.getCode() != null) {
            criterias.add(new Document("code", wbsLayoutCriteria.getCode()));
        }
        if (wbsLayoutCriteria.getSectionId() != null) {
            criterias.add(new Document("section_id", wbsLayoutCriteria.getSectionId()));
        }
        if (wbsLayoutCriteria.getImage() != null) {
            criterias.add(new Document("image", wbsLayoutCriteria.getImage()));
        }
        if (criterias.isEmpty()) {
            criterias.add(new Document());
        }
        List<WbsLayoutDto> wbsLayoutDtoList = wbsLayoutMapper.toDtos(wbsLayoutRepository.findByQuery(new Document("$and", criterias)));
        for (WbsLayoutDto wbsLayoutDto : wbsLayoutDtoList) {
            if(wbsLayoutDto.getSectionId()!=null){
                try{
                    wbsLayoutDto.setSectionDto(wbsSectionService.findWbsSectionById(wbsLayoutDto.getSectionId()));
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
        return wbsLayoutDtoList;

    }

    public List<WbsLayoutDto> findWbsLayoutBySectionId(Long sectionId) throws WimBaseException {
        try {
            List<WbsLayoutDto> wbsLayoutDtos = findWbsLayoutByCriteria(WbsLayoutCriteria.builder()
                    .sectionId(sectionId)
                    .build());
            if (wbsLayoutDtos != null && !wbsLayoutDtos.isEmpty()) {
                return wbsLayoutDtos;
            } else {
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsLayoutBySectionId)  id (" + sectionId + ")", new RuntimeException(e));
        }
    }

    public WbsLayoutDto findWbsLayoutById(Long id) throws WimBaseException {
        try {
            List<WbsLayoutDto> wbsLayoutDtos = findWbsLayoutByCriteria(WbsLayoutCriteria.builder()
                    .id(id)
                    .build());
            if (wbsLayoutDtos != null && !wbsLayoutDtos.isEmpty()) {
                WbsLayoutDto wbsSectionLayoutDto = wbsLayoutDtos.get(0);
                return wbsSectionLayoutDto;
            } else {
                throw new FunctionalException(messageComponent.get("globale.element.notFount.error"));
            }
        } catch (FunctionalException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing this method (findWbsLayoutById)  id (" + id + ")", new RuntimeException(e));
        }
    }
    public WbsLayoutDto addWbsLayout(WbsLayoutDto wbsLayoutDto) {
        wbsLayoutDto.setId(Long.valueOf(sequenceGeneratorService.getSequenceNumber(WbsLayout.SEQUENCE_NAME)));
        return wbsLayoutMapper.toDto(wbsLayoutRepository.save(wbsLayoutMapper.toEntity(wbsLayoutDto)));
    }

    public WbsLayoutDto updateWbsLayout(Long id, WbsLayoutDto wbsLayoutDto) throws WimBaseException {
        try {
            WbsLayoutDto oldwbsSectionLayoutDto = findWbsLayoutById(id);
            wbsLayoutDto.setId(oldwbsSectionLayoutDto.getId());

            return wbsLayoutMapper.toDto(wbsLayoutRepository.save(wbsLayoutMapper.toEntity(wbsLayoutDto)));
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing updateWebsSectionLayout for id (" + id + ")", new RuntimeException(e));
        }
    }

    public ResponseDto deleteWbsLayout(Long id) throws WimBaseException {
        try {
            wbsLayoutRepository.deleteById(id);
            ResponseDto responseDto = new ResponseDto();
            responseDto.setMessage(messageComponent.get("globale.element.deleted.message"));
            return responseDto;
        } catch (Exception e) {
            throw new InternalErrorException("Error while executing deleteWebsSectionLayout for id (" + id + ")", new RuntimeException(e));
        }
    }
}
