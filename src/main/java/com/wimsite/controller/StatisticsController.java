package com.wimsaas.wimapp.wimsite.controller;

import com.wimsaas.security.components.JwtTokenComponent;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.wimapp.wimsite.services.WbsStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDate;

@RestController
@RequestMapping("/api/site/statistic")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class StatisticsController {
    @Autowired
    private WbsStatisticService wbsStatisticService;
    @Autowired
    private JwtTokenComponent jwtTokenComponent;

    /*#####################   Total number of website and websiteSection   #####################*/
   @GetMapping("/wbsWebsiteCount")
   ResponseEntity<?> findNbrOfWebsiteAndWebsiteSection() throws WimBaseException {
       return ResponseEntity.ok(wbsStatisticService.findNbrOfWebsiteAndWebsiteSection());
   }

    /*#####################   Total number of website for each status   #####################*/
   @GetMapping(value = "/websiteForEachStatus")
    public ResponseEntity<?> nbrOfWebsiteForEachStatus(@RequestParam(name = "startDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate startDate,
                                                       @RequestParam(name = "endDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate endDate,
                                                       @RequestParam(name = "currentDay", required = false) boolean currentDay,
                                                       @RequestParam(name = "currentWeek", required = false) boolean currentWeek,
                                                       @RequestParam(name = "currentMonth", required = false) boolean currentMonth,
                                                       @RequestParam(name = "currentYear", required = false) boolean currentYear,
                                                       @RequestParam(name = "structureId", required = false) Long structureId) throws WimBaseException {
        return ResponseEntity.ok(wbsStatisticService.nbrOfWebsiteForEachStatus(startDate, endDate, structureId, currentDay, currentWeek, currentMonth, currentYear));
    }

    /*#####################   Total number of current website for each status   #####################*/
    @GetMapping(value = "/websiteForEachStatus/current")
    public ResponseEntity<?> nbrOfCurrentWebsiteForEachStatus(@RequestParam(name = "startDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate startDate,
                                                       @RequestParam(name = "endDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate endDate,
                                                       @RequestParam(name = "currentDay", required = false) boolean currentDay,
                                                       @RequestParam(name = "currentWeek", required = false) boolean currentWeek,
                                                       @RequestParam(name = "currentMonth", required = false) boolean currentMonth,
                                                       @RequestParam(name = "currentYear", required = false) boolean currentYear) throws WimBaseException {
        return ResponseEntity.ok(wbsStatisticService.nbrOfWebsiteForEachStatus(startDate, endDate,jwtTokenComponent.getStructureId(), currentDay, currentWeek, currentMonth, currentYear));

    }

   /*#####################   Total number of website for each domain type   #####################*/
   @GetMapping(value = "/websiteForEachDomainType")
   public ResponseEntity<?> nbrOfWebsiteForEachDomainType(@RequestParam(name = "startDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate startDate,
                                                          @RequestParam(name = "endDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate endDate,
                                                          @RequestParam(name = "currentDay", required = false) boolean currentDay,
                                                          @RequestParam(name = "currentWeek", required = false) boolean currentWeek,
                                                          @RequestParam(name = "currentMonth", required = false) boolean currentMonth,
                                                          @RequestParam(name = "currentYear", required = false) boolean currentYear,
                                                          @RequestParam(name = "structureId", required = false) Long structureId) throws WimBaseException {
       return ResponseEntity.ok(wbsStatisticService.nbrOfWebsiteForEachDomainType(startDate, endDate, structureId, currentDay, currentWeek, currentMonth, currentYear));
   }

    /*#####################   Total number of current website for each domain type   #####################*/
    @GetMapping(value = "/websiteForEachDomainType/current")
    public ResponseEntity<?> nbrOfCurrentWebsiteForEachDomainType(@RequestParam(name = "startDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate startDate,
                                                           @RequestParam(name = "endDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate endDate,
                                                           @RequestParam(name = "currentDay", required = false) boolean currentDay,
                                                           @RequestParam(name = "currentWeek", required = false) boolean currentWeek,
                                                           @RequestParam(name = "currentMonth", required = false) boolean currentMonth,
                                                           @RequestParam(name = "currentYear", required = false) boolean currentYear) throws WimBaseException {
        return ResponseEntity.ok(wbsStatisticService.nbrOfWebsiteForEachDomainType(startDate, endDate,jwtTokenComponent.getStructureId(), currentDay, currentWeek, currentMonth, currentYear));
    }

    /*#####################   Total number of website for year and months   #####################*/
    @GetMapping(value = "/websiteYearAndMonth")
    public ResponseEntity<?> getWebsiteForYearMonth (@RequestParam(name = "year", required = false) Long year,
                                                     @RequestParam(name = "month", required = false) Long month,
                                                     @RequestParam(name = "structureId", required = false) Long structureId)throws WimBaseException{
        return ResponseEntity.ok(wbsStatisticService.getWebsiteForYearMonth( year, month, structureId));
    }

    /*#####################  Total number of current website for year and months  #####################*/
    @GetMapping(value = "/websiteYearAndMonth/current")
    public ResponseEntity<?> getWebsiteForYearMonth (@RequestParam(name = "year", required = false) Long year,
                                                     @RequestParam(name = "month", required = false) Long month) throws WimBaseException{
        return ResponseEntity.ok(wbsStatisticService.getWebsiteForYearMonth( year, month,jwtTokenComponent.getStructureId()));
    }
}
