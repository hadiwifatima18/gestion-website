package com.wimsaas.wimapp.wimsite.controller;

import com.wimsaas.security.components.JwtTokenComponent;
import com.wimsaas.starter.exceptions.business.WimBaseException;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsitePage;
import com.wimsaas.wimapp.wimsite.services.*;
import com.wimsaas.wimapp.wimsite.services.criteria.*;
import com.wimsaas.wimapp.wimsite.services.criteria.WbsWebsiteContactCriteria;
import com.wimsaas.wimapp.wimsite.services.dto.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;


@RestController
@RequestMapping("/api/website")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class WebsiteController {
    @Autowired
    private WbsSectionService wbsSectionService;
    @Autowired
    private WbsLayoutService wbsSectionLayoutService;
    @Autowired
    private WbsWebsiteService wbsWebsiteService;
    @Autowired
    private WbsWebsitePageService wbsWebsitePageService;
    @Autowired
    private JwtTokenComponent jwtTokenComponent;
    @Autowired
    private WbsWebsiteMenuService wbsWebsiteMenuService;
    @Autowired
    private WbsWebsiteSectionService wbsWebsiteSectionService;
    @Autowired
    private WbsWebsiteContactService wbsWebSiteContactService;
    @Autowired
    private WbsLanguageService wbsLanguageService;
    @Autowired
    private WbsFontService wbsFontService;
    @Autowired
    private WbsStatusService wbsStatusService;
    @Autowired
    private SettingConfigService settingConfigService;
    @Autowired
    private WbsWebsiteSettingService wbsWebsiteSettingService;
    @Autowired
    private WbsSettingCategoryService wbsSettingCategoryService;
    @Autowired
    private WbsSettingService wbsSettingService;

    /*###################  wbsSection  ###################*/
    @GetMapping("/wbsSection")
    ResponseEntity<?> findAllWbsSection(@RequestParam(name = "id", required = false) Long id,
                                        @RequestParam(name = "code", required = false) String code,
                                        @RequestParam(name = "nameAr", required = false) String nameAr,
                                        @RequestParam(name = "name", required = false) String name) throws WimBaseException {
        WbsSectionCriteria wbsSectionCriteria = WbsSectionCriteria.builder().
                id(id).
                code(code).
                nameAr(nameAr).
                name(name).
                build();
        return ResponseEntity.ok(wbsSectionService.findWbsSectionByCriteria(wbsSectionCriteria));
    }

    @GetMapping("wbsSection/{id}")
    ResponseEntity<?> findSectionById(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsSectionService.findWbsSectionById(id));
    }

    @PostMapping("/wbsSection")
    ResponseEntity<?> persisteSection(@RequestBody WbsSectionDto wbsSectionDto) {
        return ResponseEntity.ok(wbsSectionService.persisteWbsSection(wbsSectionDto));
    }

    @PutMapping("wbsSection/{id}")
    ResponseEntity<?> updateSection(@PathVariable Long id, @RequestBody WbsSectionDto wbsSectionDto) throws WimBaseException {
        return ResponseEntity.ok(wbsSectionService.updateWbsSection(id, wbsSectionDto));
    }

    @DeleteMapping("wbsSection/{id}")
    ResponseEntity<?> deleteSection(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsSectionService.deletewbsSection(id));
    }

    /*############WbsLayout########################*/
    @GetMapping("/wbsLayout/{id}")
    ResponseEntity<?> findWbsLayoutById(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsSectionLayoutService.findWbsLayoutById(id));
    }

    @GetMapping("/wbsLayout")
    ResponseEntity<?> findAllWbsLayout(@RequestParam(name = "id", required = false) Long id,
                                       @RequestParam(name = "code", required = false) String code,
                                       @RequestParam(name = "sectionId", required = false) Long sectionId,
                                       @RequestParam(name = "image", required = false) String image) throws WimBaseException {
        WbsLayoutCriteria wbsSectionLayoutCriteria = WbsLayoutCriteria.builder().
                id(id).
                code(code).
                sectionId(sectionId).
                image(image).
                build();
        return ResponseEntity.ok(wbsSectionLayoutService.findWbsLayoutByCriteria(wbsSectionLayoutCriteria));
    }

    @PostMapping("/wbsLayout")
    ResponseEntity<?> addWbsLayout(@RequestBody WbsLayoutDto wbsSectionLayoutDto) {
        return ResponseEntity.ok(wbsSectionLayoutService.addWbsLayout(wbsSectionLayoutDto));
    }

    @PutMapping("/wbsLayout/{id}")
    ResponseEntity<?> updateWbsLayout(@PathVariable Long id, @RequestBody WbsLayoutDto wbsSectionLayoutDto) throws WimBaseException {
        return ResponseEntity.ok(wbsSectionLayoutService.updateWbsLayout(id, wbsSectionLayoutDto));
    }

    @DeleteMapping("/wbsLayout/{id}")
    ResponseEntity<?> deleteWbsLayout(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsSectionLayoutService.deleteWbsLayout(id));
    }

    @GetMapping("/wbsLayout/data/{id}")
    public ResponseEntity<?> getLayoutDataAsJson(@PathVariable Long id) throws WimBaseException {
        WbsLayoutDto layout = wbsSectionLayoutService.findWbsLayoutById(id);
        if (layout != null) {
            JSONObject jsonData = layout.getDataAsJson();
            return ResponseEntity.ok(jsonData.toString());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/wbsLayout/sectionId")
    ResponseEntity<?> findWbsLayoutBySectionId(@RequestParam Long sectionId) throws WimBaseException {
        return ResponseEntity.ok(wbsSectionLayoutService.findWbsLayoutBySectionId(sectionId));
    }

    /*###################  wbsWebsite  ###################*/
    @GetMapping("/wbsWebsite")
    ResponseEntity<?> findAllWbsSite(@RequestParam(name = "id", required = false) Long id,
                                     @RequestParam(name = "name", required = false) String name,
                                     @RequestParam(name = "domainName", required = false) String domainName,
                                     @RequestParam(name = "domainType", required = false) String domainType,
                                     @RequestParam(name = "fontLatinId", required = false) Long fontLatinId,
                                     @RequestParam(name = "fontArabicId", required = false) Long fontArabicId,
                                     @RequestParam(name = "codeLanguage", required = false) String codeLanguage,
                                     @RequestParam(name = "folderHash", required = false) String folderHash,
                                     @RequestParam(name = "statusId", required = false) Long statusId,
                                     @RequestParam(name = "moduleId", required = false) Long moduleId,
                                     @RequestParam(name = "structureId", required = false) Long structureId,
                                     @RequestParam(name = "storeId", required = false) Long storeId) throws WimBaseException {
        WbsWebsiteCriteria wbsWebsiteCriteria = WbsWebsiteCriteria.builder().
                id(id).
                name(name).
                domainName(domainName).
                domainType(domainType).
                fontLatinId(fontLatinId).
                fontArabicId(fontArabicId).
                codeLanguage(codeLanguage).
                folderHash(folderHash).
                statusId(statusId).
                moduleId(moduleId).
                structureId(structureId).
                storeId(storeId).
                build();
        return ResponseEntity.ok(wbsWebsiteService.findWbsWebsiteByCriteria(wbsWebsiteCriteria));
    }

    @GetMapping("/wbsWebsite/current")
    ResponseEntity<?> findCurrentWbsSite(@RequestParam(name = "id", required = false) Long id,
                                         @RequestParam(name = "name", required = false) String name,
                                         @RequestParam(name = "domainName", required = false) String domainName,
                                         @RequestParam(name = "domainType", required = false) String domainType,
                                         @RequestParam(name = "fontLatinId", required = false) Long fontLatinId,
                                         @RequestParam(name = "fontArabicId", required = false) Long fontArabicId,
                                         @RequestParam(name = "codeLanguage", required = false) String codeLanguage,
                                         @RequestParam(name = "folderHash", required = false) String folderHash,
                                         @RequestParam(name = "statusId", required = false) Long statusId) throws WimBaseException {
        WbsWebsiteCriteria wbsWebsiteCriteria = WbsWebsiteCriteria.builder().
                id(id).
                name(name).
                domainName(domainName).
                domainType(domainType).
                fontLatinId(fontLatinId).
                fontArabicId(fontArabicId).
                codeLanguage(codeLanguage).
                folderHash(folderHash).
                statusId(statusId).
                moduleId(jwtTokenComponent.getModulelId()).
                storeId(jwtTokenComponent.getStoreId()).
                structureId(jwtTokenComponent.getStructureId()).
                build();
        return ResponseEntity.ok(wbsWebsiteService.findWbsWebsiteByCriteria(wbsWebsiteCriteria));
    }

    @GetMapping("/wbsWebsite/{id}")
    ResponseEntity<?> findWbsSiteById(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.findWbsWebsiteById(id));
    }

    @GetMapping("/wbsWebsite/byHash")
    ResponseEntity<?> findWbsSiteByHash(@RequestParam(name = "hash", required = false) String hash) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.findWbsSiteByHash(hash));
    }

    @GetMapping("/wbsWebsite/byDomain")
    ResponseEntity<?> findWbsSiteByDomain(@RequestParam(name = "domain", required = false) String domainName) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.findWbsSiteByDomain(domainName));
    }

    @PostMapping("/wbsWebsite")
    ResponseEntity<?> persisteWebSite(@RequestBody WbsWebsiteDto wbsWebsiteDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.persisteWebSite(wbsWebsiteDto));
    }

    @PutMapping("/wbsWebsite/{id}")
    ResponseEntity<?> updateWebSite(@PathVariable Long id, @RequestBody WbsWebsiteDto wbsWebsiteDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.updateWebSite(id, wbsWebsiteDto));
    }

    @DeleteMapping("/wbsWebsite/{id}")
    ResponseEntity<?> deleteWebSite(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.deleteWebSite(id));
    }

    @PutMapping("/wbsWebsite/language")
    ResponseEntity<?> updateDefaultLanguage(@RequestParam(name = "websiteId") Long websiteId,
                                            @RequestBody WbsLanguageDto wbsLanguageDto
    ) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.updateDefaultLanguage(websiteId, wbsLanguageDto));
    }

    @PutMapping("/wbsWebsite/latinFont")
    ResponseEntity<?> updateLatinFont(@RequestParam(name = "websiteId") Long websiteId,
                                      @RequestBody WbsFontDto wbsFontDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.updateDefaultLatinFont(websiteId, wbsFontDto.getId()));
    }

    @PutMapping("/wbsWebsite/arabicFont")
    ResponseEntity<?> updateArabicFont(@RequestParam(name = "websiteId") Long websiteId,
                                       @RequestBody WbsFontDto wbsFontDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.updateDefaultArabicFont(websiteId, wbsFontDto.getId()));
    }

    @PutMapping("/wbsWebsite/status")
    ResponseEntity<?> updateStatus(@RequestParam(name = "websiteId") Long websiteId,
                                   @RequestBody WbsStatusDto wbsStatusDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.updateStatus(websiteId, wbsStatusDto.getId()));
    }

    @PostMapping("/wbsWebsite/uploadImage")
    public ResponseEntity<?> uploadImage(@RequestParam(name = "image") MultipartFile image, @RequestParam(name = "websiteId") Long websiteId,@RequestParam(name = "websiteSectionId") Long websiteSectionId) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.uploadImage(image, websiteId,websiteSectionId));
    }

    @PostMapping("/wbsWebsite/uploadFile")
    public ResponseEntity<?> uploadFile(@RequestParam(name = "file") MultipartFile file, @RequestParam(name = "websiteId") Long websiteId,@RequestParam(name = "websiteSectionId") Long websiteSectionId) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.uploadFile(file, websiteId,websiteSectionId));
    }

    @GetMapping("/wbsWebsite/listeFile")
    public ResponseEntity<?> listeFile(@RequestParam(name = "websiteId") Long websiteId) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.listFilesForWebsite(websiteId));
    }

    @PostMapping("/wbsWebsite/uploadFavicon")
    public ResponseEntity<?> uploadFavicon(@RequestParam(name = "favicon") MultipartFile favicon, @RequestParam(name = "websiteId") Long websiteId,@RequestParam(name = "websiteSectionId") Long websiteSectionId) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.uploadFavicon(favicon, websiteId,websiteSectionId));
    }

    @PostMapping("/wbsWebsite/uploadLogo")
    public ResponseEntity<?> uploadLogo(@RequestParam(name = "logo") MultipartFile logo, @RequestParam(name = "websiteId") Long websiteId,@RequestParam(name = "websiteSectionId") Long websiteSectionId) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteService.uploadLogo(logo, websiteId,websiteSectionId));
    }

    /*###################  wbsWebsitePage  ###################*/
    @GetMapping("/wbsWebsitePage")
    ResponseEntity<?> findAllWebsitePage(@RequestParam(name = "id", required = false) Long id,
                                         @RequestParam(name = "websiteId", required = true) Long websiteId,
                                         @RequestParam(name = "name", required = false) String name,
                                         @RequestParam(name = "nameAr", required = false) String nameAr,
                                         @RequestParam(name = "seoTitle", required = false) String seoTitle,
                                         @RequestParam(name = "path", required = false) String path,
                                         @RequestParam(name = "seoKeywords", required = false) String seoKeywords,
                                         @RequestParam(name = "seoDescription", required = false) String seoDescription,
                                         @RequestParam(name = "sort_key", required = false) Double sortKey,
                                         @RequestParam(name = "websiteSectionId", required = false) Long websiteSectionId) throws WimBaseException {
        WbsWebsitePageCriteria wbsWebsitePageCriteria = WbsWebsitePageCriteria.builder().
                id(id).
                websiteId(websiteId).
                name(name).
                nameAr(nameAr).
                path(path).
                seoKeywords(seoKeywords).
                seoTitle(seoTitle).
                seoDescription(seoDescription).
                websiteSectionId(websiteSectionId).
                sortKey(sortKey).
                build();
        return ResponseEntity.ok(wbsWebsitePageService.findWbsWebsitePageByCriteria(wbsWebsitePageCriteria));
    }

    @GetMapping("/wbsWebsitePage/{id}")
    ResponseEntity<?> findWbsWebsitePageById(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsitePageService.findWbsWebsitePageById(id));
    }

    @PostMapping("/wbsWebsitePage")
    ResponseEntity<?> persisteWebsitePage(@RequestBody WbsWebsitePageDto wbsWebsitePageDto) throws WimBaseException {
        System.out.println("persistWebsitePage");
        return ResponseEntity.ok(wbsWebsitePageService.persisteWbsWebsitePage(wbsWebsitePageDto));
    }

    @PutMapping("/wbsWebsitePage/{id}")
    ResponseEntity<?> updateWebsitePage(@PathVariable Long id, @RequestBody WbsWebsitePageDto wbsWebsitePageDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsitePageService.updateWbsWebsitePage(id, wbsWebsitePageDto));
    }

    @DeleteMapping("/wbsWebsitePage/{id}")
    ResponseEntity<?> deleteWebsitePage(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsitePageService.deleteWebsitePage(id));
    }

    /*###################  wbsWebsiteMenu  ###################*/
    @GetMapping("/wbsWebsiteMenu")
    ResponseEntity<?> findAllWbsWebsiteMenu(@RequestParam(name = "id", required = false) Long id,
                                            @RequestParam(name = "websiteId", required = true) Long websiteId,
                                            @RequestParam(name = "nameAr", required = false) String nameAr,
                                            @RequestParam(name = "name", required = false) String name,
                                            @RequestParam(name = "pages", required = false) WbsWebsitePage wbsWebsitePage,
                                            @RequestParam(name = "sortKey", required = false) Double sortKey,
                                            @RequestParam(name = "websitePageId", required = false) Long websitePageId) throws WimBaseException {
        WbsWebsiteMenuCriteria wbsWebsiteMenuItemsCriteria = WbsWebsiteMenuCriteria.builder().
                id(id).
                websiteId(websiteId).
                nameAr(nameAr).
                name(name).
                websitePageId(websitePageId).
                sortKey(sortKey).
                build();
        return ResponseEntity.ok(wbsWebsiteMenuService.findWbsWebsiteMenuByCriteria(wbsWebsiteMenuItemsCriteria));
    }

    @GetMapping("wbsWebsiteMenu/{id}")
    ResponseEntity<?> findWbsWebsiteMenuById(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteMenuService.findWbsWebsiteMenuById(id));
    }

    @PostMapping("/wbsWebsiteMenu")
    ResponseEntity<?> persisteWbsWebsiteMenu(@RequestBody WbsWebsiteMenuDto wbsWebsiteMenuItemsDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteMenuService.persisteWbsWebsiteMenu(wbsWebsiteMenuItemsDto));
    }

    @PutMapping("wbsWebsiteMenu/{id}")
    ResponseEntity<?> updateWbsWebsiteMenu(@PathVariable Long id, @RequestBody WbsWebsiteMenuDto wbsWebsiteMenuDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteMenuService.updateWbsWebsiteMenu(id, wbsWebsiteMenuDto));
    }

    @DeleteMapping("wbsWebsiteMenu/{id}")
    ResponseEntity<?> deleteWbsWebsiteMenu(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteMenuService.deleteWbsWebsiteMenu(id));
    }

    /*###################  wbsWebsiteSection  ###################*/
    @GetMapping("/wbsWebsiteSection")
    ResponseEntity<?> findAllwbsWebsiteSection(@RequestParam(name = "id", required = false) Long id,
                                               @RequestParam(name = "layoutId", required = false) Long layoutId,
                                               @RequestParam(name = "websiteId", required = true) Long websiteId,
                                               @RequestParam(name = "websitePageId", required = false) Long websitePageId,
                                               @RequestParam(name = "sectionId", required = false) Long sectionId,
                                               @RequestParam(name = "sortKey", required = false) Double sortKey) throws WimBaseException {
        WbsWebsiteSectionCriteria wbsWebsiteSectionCriteria = WbsWebsiteSectionCriteria.builder().
                id(id).
                layoutId(layoutId).
                sortKey(sortKey).
                websitePageId(websitePageId).
                websiteId(websiteId).
                sectionId(sectionId).
                build();
        return ResponseEntity.ok(wbsWebsiteSectionService.findWbsWebsiteSectionByCriteria(wbsWebsiteSectionCriteria));
    }

    @GetMapping("wbsWebsiteSection/{id}")
    ResponseEntity<?> findWbsWebsiteSectionById(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteSectionService.findWbsWebsiteSectionById(id));
    }

    @PostMapping("/wbsWebsiteSection")
    ResponseEntity<?> persisteWbsWebsiteSection(@RequestBody WbsWebsiteSectionDto wbsWebsiteSectionDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteSectionService.persisteWbsWebsiteSection(wbsWebsiteSectionDto));
    }

    @PutMapping("/wbsWebsiteSection/layout")
    ResponseEntity<?> setLayoutForSetion(@RequestParam(name = "websiteSectionId") Long websiteSectionId, @RequestBody WbsLayoutDto wbsLayoutDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteSectionService.setLayoutForSetion(websiteSectionId, wbsLayoutDto));
    }

    @PutMapping("wbsWebsiteSection/{id}")
    ResponseEntity<?> updatewbsWebsiteSection(@PathVariable Long id, @RequestBody WbsWebsiteSectionDto wbsWebsiteSectionDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteSectionService.updateWbsWebsiteSection(id, wbsWebsiteSectionDto));
    }

    @DeleteMapping("/wbsWebsiteSection/byHash")
    ResponseEntity<?> deleteWbsWebsiteSectionByHash(@RequestParam(name = "hash", required = false) String hash) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteSectionService.deleteWbsWebsiteSectionByHash(hash));
    }

    @GetMapping("/wbsWebsiteSection/data/{id}")
    public ResponseEntity<?> getWbsWebsiteSectionDataAsJson(@PathVariable Long id) throws WimBaseException {
        WbsWebsiteSectionDto sectionService = wbsWebsiteSectionService.findWbsWebsiteSectionById(id);
        if (sectionService != null) {
            JSONObject jsonData = sectionService.getDataAsJson();
            return ResponseEntity.ok(jsonData.toString());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/wbsWebsiteSection/data/{id}")
    public ResponseEntity<?> updateWbsWebsiteSectionDataByKeyValue(@PathVariable Long id, @RequestParam String code, @RequestParam String key, @RequestParam String value) throws WimBaseException {
        WbsWebsiteSectionDto updatedWbsWebsiteSectionDto = wbsWebsiteSectionService.updateWbsWebsiteSectionDataByKeyValue(id, key, value, code);
        return ResponseEntity.ok(updatedWbsWebsiteSectionDto);
    }

    @PutMapping("/wbsWebsiteSection/updateData/{id}")
    public ResponseEntity<?> updateWbsWebsiteSectionData(@PathVariable Long id, @RequestParam String code, @RequestBody Map<String, Object> data) throws WimBaseException {
        WbsWebsiteSectionDto updatedWbsWebsiteSectionDto = wbsWebsiteSectionService.updateWbsWebsiteSectionData(id, code, new JSONObject(data));
        return ResponseEntity.ok(updatedWbsWebsiteSectionDto);
    }
    @PostMapping("/wbsWebsiteSection/uploadWbsWebsiteSectionImage")
    public ResponseEntity<?> uploadWbsWebsiteSectionImage(@RequestParam(name = "websiteId") Long websiteId, @RequestParam(name = "image") MultipartFile image, @RequestParam String websiteSectionHash, @RequestParam String languageCode, @RequestParam String jsonPath) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteSectionService.uploadWbsWebsiteSectionImage(websiteId, image, websiteSectionHash, languageCode, jsonPath));
    }
    /*###################  setting  ###################*/
    @GetMapping("setting")
    ResponseEntity<?> findAllSetting() throws WimBaseException {
        return ResponseEntity.ok(wbsSectionService.findAllSetting());
    }
    /*###################  wbsLanguage  ###################*/
    @GetMapping("/wbsLanguage")
    ResponseEntity<?> findAllWbsLanguage(@RequestParam(name = "name", required = false) String name,
                                         @RequestParam(name = "code", required = false) String code) throws WimBaseException {
        WbsLanguageCriteria wbsLanguageCriteria = WbsLanguageCriteria.builder().
                name(name).
                code(code).
                build();
        return ResponseEntity.ok(wbsLanguageService.findWbsLanguageByCriteria(wbsLanguageCriteria));
    }

    @GetMapping("wbsLanguage/{code}")
    ResponseEntity<?> findWbsLanguageById(@PathVariable String code) throws WimBaseException {
        return ResponseEntity.ok(wbsLanguageService.findWbsLanguageByCode(code));
    }

    @PostMapping("/wbsLanguage")
    ResponseEntity<?> persisteWbsLanguage(@RequestBody WbsLanguageDto wbsLanguageDto) {
        return ResponseEntity.ok(wbsLanguageService.persisteWbsLanguage(wbsLanguageDto));
    }

    @PutMapping("wbsLanguage/{code}")
    ResponseEntity<?> updateWbsLanguage(@PathVariable String code, @RequestBody WbsLanguageDto wbsLanguageDto) throws WimBaseException {
        return ResponseEntity.ok(wbsLanguageService.updateWbsLanguage(code, wbsLanguageDto));
    }

    @DeleteMapping("wbsLanguage/{code}")
    ResponseEntity<?> deleteWbsLanguage(@PathVariable String code) throws WimBaseException {
        return ResponseEntity.ok(wbsLanguageService.deleteWbsLanguage(code));
    }

    /*###################  wbsFont  ###################*/
    @GetMapping("/wbsFont")
    ResponseEntity<?> findAllWbsFont(@RequestParam(name = "id", required = false) Long id,
                                     @RequestParam(name = "urlFont", required = false) String urlFont,
                                     @RequestParam(name = "nameFont", required = false) String nameFont,
                                     @RequestParam(name = "thumbnail", required = false) String thumbnail,
                                     @RequestParam(name = "arabic", required = false) boolean arabic) throws WimBaseException {
        WbsFontCriteria wbsFontCriteria = WbsFontCriteria.builder().
                id(id).
                urlFont(urlFont).
                nameFont(nameFont).
                thumbnail(thumbnail).
                arabic(arabic).
                build();
        return ResponseEntity.ok(wbsFontService.findWbsFontByCriteria(wbsFontCriteria));
    }

    @GetMapping("wbsFont/{id}")
    ResponseEntity<?> findWbsFontById(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsFontService.findWbsFontById(id));
    }

    @PostMapping("/wbsFont")
    ResponseEntity<?> persisteWbsFont(@RequestBody WbsFontDto wbsFontDto) {
        return ResponseEntity.ok(wbsFontService.persisteWbsFont(wbsFontDto));
    }

    @PutMapping("wbsFont/{id}")
    ResponseEntity<?> updateWbsFont(@PathVariable Long id, @RequestBody WbsFontDto wbsFontDto) throws WimBaseException {
        return ResponseEntity.ok(wbsFontService.updateWbsFont(id, wbsFontDto));
    }

    @DeleteMapping("wbsFont/{id}")
    ResponseEntity<?> deleteWbsFont(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsFontService.deleteWbsFont(id));
    }

    /*###################  wbsStatus ###################*/
    @GetMapping("/wbsStatus")
    ResponseEntity<?> findAllWbsStatus(@RequestParam(name = "id", required = false) Long id,
                                       @RequestParam(name = "description", required = false) String description,
                                       @RequestParam(name = "name", required = false) String name) throws WimBaseException {
        WbsStatusCriteria wbsStatusCriteria = WbsStatusCriteria.builder().
                id(id).
                description(description).
                name(name).
                build();
        return ResponseEntity.ok(wbsStatusService.findWbsStatusByCriteria(wbsStatusCriteria));
    }

    @GetMapping("wbsStatus/{id}")
    ResponseEntity<?> findWbsStatusById(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsStatusService.findWbsStatusById(id));
    }

    @PostMapping("/wbsStatus")
    ResponseEntity<?> persisteWbsStatus(@RequestBody WbsStatusDto wbsStatusDto) {
        return ResponseEntity.ok(wbsStatusService.persisteWbsStatus(wbsStatusDto));
    }

    @PutMapping("wbsStatus/{id}")
    ResponseEntity<?> updateWbsStatus(@PathVariable Long id, @RequestBody WbsStatusDto wbsStatusDto) throws WimBaseException {
        return ResponseEntity.ok(wbsStatusService.updateWbsStatus(id, wbsStatusDto));
    }

    @DeleteMapping("wbsStatus/{id}")
    ResponseEntity<?> deleteWbsStatus(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsStatusService.deleteWbsStatus(id));
    }

    /*###################  wbsWebsiteContact  ###################*/
    @GetMapping("/wbsWebsiteContact")
    ResponseEntity<?> findAllWbsWebsiteContact(@RequestParam(name = "id", required = false) Long id,
                                               @RequestParam(name = "websiteId", required = true) Long websiteId,
                                               @RequestParam(name = "instagram", required = false) String instagram,
                                               @RequestParam(name = "facebook", required = false) String facebook,
                                               @RequestParam(name = "linkedIn", required = false) String linkedIn,
                                               @RequestParam(name = "twitter", required = false) String twitter,
                                               @RequestParam(name = "youtube", required = false) String youtube,
                                               @RequestParam(name = "phoneNumbre;", required = false) String phoneNumbre,
                                               @RequestParam(name = "adresse", required = false) String adresse,
                                               @RequestParam(name = "email", required = false) String email) throws WimBaseException {
        WbsWebsiteContactCriteria wbsWebSiteContactCriteria = WbsWebsiteContactCriteria.builder().
                id(id).
                websiteId(websiteId).
                instagram(instagram).
                facebook(facebook).
                linkedIn(linkedIn).
                twitter(twitter).
                youtube(youtube).
                phoneNumbre(phoneNumbre).
                adresse(adresse).
                email(email).
                build();
        return ResponseEntity.ok(wbsWebSiteContactService.findWbsWebsiteContactByCriteria(wbsWebSiteContactCriteria));
    }

    @GetMapping("wbsWebsiteContact/{id}")
    ResponseEntity<?> findWbsWebsiteContactById(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsWebSiteContactService.findWbsWebsiteContactById(id));
    }

    @PostMapping("/wbsWebsiteContact")
    ResponseEntity<?> persisteWbsWebsiteContact(@RequestBody WbsWebsiteContactDto wbsWebSiteContactDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebSiteContactService.persisteWbsWebsiteContact(wbsWebSiteContactDto));
    }

    @PutMapping("wbsWebsiteContact/{id}")
    ResponseEntity<?> updateWbsWebsiteContact(@PathVariable Long id, @RequestBody WbsWebsiteContactDto wbsWebSiteContactDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebSiteContactService.updateWbsWebsiteContact(id, wbsWebSiteContactDto));
    }

    @DeleteMapping("wbsWebsiteContact/{id}")
    ResponseEntity<?> deleteWbsWebsiteContact(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsWebSiteContactService.deleteWbsWebsiteContact(id));
    }

    /*###################  WbsWebsiteSetting ###################*/
    @GetMapping("/wbsWebsiteSetting")
    ResponseEntity<?> findAllWbsWebsiteSetting(@RequestParam(name = "id", required = false) Long id,
                                               @RequestParam(name = "websiteId", required = false) Long websiteId,
                                               @RequestParam(name = "settingId", required = false) Long settingId,
                                               @RequestParam(name = "value", required = false) String value,
                                               @RequestParam(name = "userCreation", required = false) String userCreation,
                                               @RequestParam(name = "userUpdate", required = false) String userUpdate,
                                               @RequestParam(name = "dateCreation", required = false) LocalDateTime dateCreation,
                                               @RequestParam(name = "dateUpdate", required = false) LocalDateTime dateUpdate) throws WimBaseException {
        WbsWebsiteSettingCriteria wbsWebsiteSettingCriteria = WbsWebsiteSettingCriteria.builder().
                id(id).
                websiteId(websiteId).
                settingId(settingId).
                value(value).
                userCreation(userCreation).
                userUpdate(userUpdate).
                dateCreation(dateCreation).
                dateUpdate(dateUpdate).
                build();
        return ResponseEntity.ok(wbsWebsiteSettingService.findWbsWebsiteSettingByCriteria(wbsWebsiteSettingCriteria));
    }

    @GetMapping("wbsWebsiteSetting/{id}")
    ResponseEntity<?> findWbsWebsiteSettingById(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteSettingService.findWbsWebsiteSettingById(id));
    }

    @PostMapping("/wbsWebsiteSetting")
    ResponseEntity<?> persisteWbsWebsiteSetting(@RequestBody WbsWebsiteSettingDto wbsWebsiteSettingDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteSettingService.persisteWbsWebsiteSetting(wbsWebsiteSettingDto));
    }

    @PutMapping("wbsWebsiteSetting/{id}")
    ResponseEntity<?> updateWbsWebsiteSetting(@PathVariable Long id, @RequestBody WbsWebsiteSettingDto wbsWebsiteSettingDto) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteSettingService.updateWbsWebsiteSetting(id, wbsWebsiteSettingDto));
    }

    @DeleteMapping("wbsWebsiteSetting/{id}")
    ResponseEntity<?> deleteWbsWebsiteSetting(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsWebsiteSettingService.deleteWbsWebsiteSetting(id));
    }

    /*###################  WbsSettingCategory ###################*/
    @GetMapping("/wbsSettingCategory")
    ResponseEntity<?> findAllWbsSettingCategory(@RequestParam(name = "id", required = false) Long id,
                                                @RequestParam(name = "sortKey", required = false) Long sortKey,
                                                @RequestParam(name = "code", required = false) String code,
                                                @RequestParam(name = "name", required = false) String name,
                                                @RequestParam(name = "nameAr", required = false) String nameAr,
                                                @RequestParam(name = "description", required = false) String description,
                                                @RequestParam(name = "dateCreation", required = false) LocalDateTime dateCreation,
                                                @RequestParam(name = "dateUpdate", required = false) LocalDateTime dateUpdate) throws WimBaseException {
        WbsSettingCategoryCriteria wbsSettingCategoryCriteria = WbsSettingCategoryCriteria.builder().
                id(id).
                sortKey(sortKey).
                code(code).
                nameAr(nameAr).
                name(name).
                description(description).
                dateCreation(dateCreation).
                dateUpdate(dateUpdate).
                build();
        return ResponseEntity.ok(wbsSettingCategoryService.findWbsSettingCategoryByCriteria(wbsSettingCategoryCriteria));
    }

    @GetMapping("wbsSettingCategory/{id}")
    ResponseEntity<?> findWbsSettingCategoryById(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsSettingCategoryService.findWbsSettingCategoryById(id));
    }

    @PostMapping("wbsSettingCategory")
    ResponseEntity<?> persisteWbsSettingCategory(@RequestBody WbsSettingCategoryDto wbsSettingCategoryDto) throws WimBaseException {
        return ResponseEntity.ok(wbsSettingCategoryService.persisteWbsSettingCategory(wbsSettingCategoryDto));
    }

    @PutMapping("wbsSettingCategory/{id}")
    ResponseEntity<?> updateWbsSettingCategory(@PathVariable Long id, @RequestBody WbsSettingCategoryDto wbsSettingCategoryDto) throws WimBaseException {
        return ResponseEntity.ok(wbsSettingCategoryService.updateWbsSettingCategory(id, wbsSettingCategoryDto));
    }

    @DeleteMapping("wbsSettingCategory/{id}")
    ResponseEntity<?> deleteWbsSettingCategory(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsSettingCategoryService.deleteWbsSettingCategory(id));
    }

    /*###################  WbsSetting ###################*/
    @GetMapping("/wbsSetting")
    ResponseEntity<?> findAllWbsSetting(@RequestParam(name = "id", required = false) Long id,
                                        @RequestParam(name = "sortKey", required = false) Long sortKey,
                                        @RequestParam(name = "code", required = false) String code,
                                        @RequestParam(name = "name", required = false) String name,
                                        @RequestParam(name = "nameAr", required = false) String nameAr,
                                        @RequestParam(name = "settingCategoryId", required = false) Long settingCategoryId,
                                        @RequestParam(name = "inputType", required = false) String inputType,
                                        @RequestParam(name = "defaultValue", required = false) String defaultValue,
                                        @RequestParam(name = "description", required = false) String description,
                                        @RequestParam(name = "dateCreation", required = false) LocalDateTime dateCreation,
                                        @RequestParam(name = "dateUpdate", required = false) LocalDateTime dateUpdate) throws WimBaseException {
        WbsSettingCriteria wbsSettingCriteria = WbsSettingCriteria.builder().
                id(id).
                sortKey(sortKey).
                code(code).
                name(name).
                nameAr(nameAr).
                inputType(inputType).
                defaultValue(defaultValue).
                settingCategoryId(settingCategoryId).
                description(description).
                dateCreation(dateCreation).
                dateUpdate(dateUpdate).
                build();
        return ResponseEntity.ok(wbsSettingService.findWbsSettingByCriteria(wbsSettingCriteria));
    }

    @GetMapping("wbsSetting/{id}")
    ResponseEntity<?> findWbsSettingById(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsSettingService.findWbsSettingById(id));
    }

    @PostMapping("wbsSetting")
    ResponseEntity<?> persisteWbsSetting(@RequestBody WbsSettingDto wbsSettingDto) throws WimBaseException {
        return ResponseEntity.ok(wbsSettingService.persisteWbsSetting(wbsSettingDto));
    }

    @PutMapping("wbsSetting/{id}")
    ResponseEntity<?> updateWbsSetting(@PathVariable Long id, @RequestBody WbsSettingDto wbsSettingDto) throws WimBaseException {
        return ResponseEntity.ok(wbsSettingService.updateWbsSettingDto(id, wbsSettingDto));
    }

    @DeleteMapping("wbsSetting/{id}")
    ResponseEntity<?> deleteWbsSetting(@PathVariable Long id) throws WimBaseException {
        return ResponseEntity.ok(wbsSettingService.deleteWbsSettingDto(id));
    }

    /*###################################  initializeSetting   ###################################*/
    @GetMapping(value = "/initializeSettingCollections")
    public ResponseEntity<?> initializeSetting() throws WimBaseException {
        return ResponseEntity.ok().body(settingConfigService.initializeSetting());
    }
    @GetMapping(value = "/exportWbsWebsiteAllComposants")
    public ResponseEntity<?> exportWbsWebsiteAllComposants(@RequestParam(name = "websiteId") Long websiteId) throws Exception {
        return ResponseEntity.ok().body(settingConfigService.exportWbsWebsiteAllComposants(websiteId));
    }

}
