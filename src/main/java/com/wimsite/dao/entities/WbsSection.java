package com.wimsaas.wimapp.wimsite.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;




@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "wbs_section")
public class WbsSection implements Serializable {

    @Transient
    public static final String SEQUENCE_NAME="wbs_section";
    @Id
    @Field("id")
    private Long id;
    @Field("code")
    private String code;
    @Field("name")
    private String name;
    @Field("name_ar")
    private String nameAr;

}


