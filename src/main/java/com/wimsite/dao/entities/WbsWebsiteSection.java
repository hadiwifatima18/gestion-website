package com.wimsaas.wimapp.wimsite.dao.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONObject;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "wbs_website_section")
public class WbsWebsiteSection implements Serializable {
    @Transient
    public static final String SEQUENCE_NAME = "wbs_website_section";
    @Id
    @Field("id")
    private Long id;
    @Field("layout_id")
    private Long layoutId;
    @Field("sort_key")
    private Double sortKey;
    @Field("website_id")
    private Long websiteId;
    @Field("website_page_id")
    private Long websitePageId;
    @Field("section_id")
    private Long sectionId;
    @Field("hash")
    private String hash;
    @Field("data")
    private String data;
    @Field("data_ar")
    private String dataAr;
    @Field("data_en")
    private String dataEn;
    @JsonIgnore
    public JSONObject getDataAsJson() {
        return new JSONObject(data);
    }
    @JsonProperty
    public void setDataFromJson(JSONObject jsonObject) {
        this.data = jsonObject.toString();
    }
}
