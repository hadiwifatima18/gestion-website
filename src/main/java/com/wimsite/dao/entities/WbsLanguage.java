package com.wimsaas.wimapp.wimsite.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "wbs_language")
public class WbsLanguage implements Serializable {

    @Id
    @Field("code")
    private String code;
    @Field("name")
    private String name;
    @Field("name_ar")
    private String nameAr;

}



