package com.wimsaas.wimapp.wimsite.dao.entities.db;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

/*
@Document(collection = "wbs_db_sequence")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WbsMongodbSequnce  implements Serializable {
    private static final Long serialVersionUID = 1L;
    @Field("id")
    private String id;
    private int seq;
}
*/