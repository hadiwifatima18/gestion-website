package com.wimsaas.wimapp.wimsite.dao.entities;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "wbs_setting")
public class WbsSetting {
    @Transient
    public static final String SEQUENCE_NAME="wbs_setting";
    @Id
    @Field("id")
    private Long id;
    @Field("code")
    private String code;
    @Field("name")
    private String name;
    @Field("name_ar")
    private String nameAr;
    @Field("default_value")
    private String defaultValue;
    @Field("active")
    private boolean active;
    @Field("description")
    private String description;
    @Field("date_creation")
    private LocalDateTime dateCreation;
    @Field("date_update")
    private LocalDateTime dateUpdate;
    @Field("sort_key")
    private Long sortKey;
    @Field("setting_category_id")
    private Long settingCategoryId;
    @Field("input_type")
    private String inputType;
}
