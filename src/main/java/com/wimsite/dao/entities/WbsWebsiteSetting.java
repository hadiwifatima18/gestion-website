package com.wimsaas.wimapp.wimsite.dao.entities;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "wbs_website_setting")
public class WbsWebsiteSetting {
    @Transient
    public static final String SEQUENCE_NAME="wbs_website_setting";
    @Id
    @Field("id")
    private Long id;
    @Field("website_id")
    private Long websiteId;
    @Field("setting_id")
    private Long settingId;
    @Field("value")
    private String value;
    @Field("user_creation")
    private String userCreation;
    @Field("user_update")
    private String userUpdate;
    @Field("date_creation")
    private LocalDateTime dateCreation;
    @Field("date_update")
    private LocalDateTime dateUpdate;
}
