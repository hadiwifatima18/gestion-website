package com.wimsaas.wimapp.wimsite.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "wbs_website_contact")
public class WbsWebsiteContact implements Serializable {
    @Transient
    public static final String SEQUENCE_NAME="wbs_website_contact";
    @Id
    @Field("id")
    private Long id;
    @Field("instagram")
    private String instagram;
    @Field("facebook")
    private String facebook;
    @Field("linked_in")
    private String linkedIn;
    @Field("twitter")
    private String twitter;
    @Field("youtube")
    private String youtube;
    @Field("phone_number")
    private String phoneNumbre;
    @Field("address")
    private String address;
    @Field("email")
    private String email;
    @Field("website_id")
    private Long websiteId;
}