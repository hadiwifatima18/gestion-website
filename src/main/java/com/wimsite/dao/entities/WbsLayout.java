package com.wimsaas.wimapp.wimsite.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "wbs_layout")
public class WbsLayout implements Serializable {

    @Transient
    public static final String SEQUENCE_NAME = "wbs_layout";
    @Id
    @Field("id")
    private Long id;
    @Field("code")
    private String code;
    @Field("section_id")
    private Long sectionId;
    @Field("image")
    private String image;
    @Field("data")
    private String data;
    @Field("data_ar")
    private String dataAr;
    @Field("data_en")
    private String dataEn;
}

