package com.wimsaas.wimapp.wimsite.dao.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "wbs_website_page")
public class WbsWebsitePage implements Serializable {

    @Transient
    public static final String SEQUENCE_NAME="wbs_website_page";
    @Id
    @Field("id")
    private Long id;
    @Field("name")
    private String name;
    @Field("name_ar")
    private String nameAr;
    @Field("path")
    private String path;
    @Field("sort_key")
    private Double sortKey;
    @Field("home_page")
    private boolean homePage;
    @Field("seo_title")
    private String seoTitle;
    @Field("seo_description")
    private String seoDescription;
    @Field("seo_keywords")
    private String seoKeywords;
    @Field("add_menu")
    private boolean addMenu;
    @Field("hash")
    private String hash;
    @Field("website_section_id")
    private Long websiteSectionId;
    @Field("website_id")
    private Long websiteId;

}


