package com.wimsaas.wimapp.wimsite.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "wbs_font")
public class WbsFont {
    @Transient
    public static final String SEQUENCE_NAME = "wbs_font";
    @Id
    @Field("id")
    private Long id;
    @Field("url")
    private String url;
    @Field("name")
    private String name;
    @Field("thumbnail")
    private String thumbnail;
    @Field("arabic")
    private Boolean arabic;}
