package com.wimsaas.wimapp.wimsite.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import java.io.Serializable;
import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "wbs_website")
public class WbsWebsite implements Serializable{
    @Transient
    public static final String SEQUENCE_NAME="wbs_website";
    @Id
    @Field("id")
    private Long id;
    @Field("name")
    private String name;
    @Field("domain_type")
    private String domainType;
    @Field("domain_name")
    private String domainName;
    @Field("subdomain_name")
    private String subdomainName;
    @Field("logo")
    private String logo;
    @Field("favicon")
    private String favicon;
    @Field("folder_hash")
    private String folderHash;
    @Field("hash")
    private String hash;
    @Field("language_code")
    private String languageCode;
    @Field("latin_font_id")
    private Long fontLatinId;
    @Field("arabic_font_id")
    private Long fontArabicId;
    @Field("status_id")
    private Long statusId;
    @Field("init-by_json_file")
    private String initByJsonFile;
    @Field("init_by_website_id")
    private Long initByWebsiteId;
    @Field("date_creation")
    private LocalDateTime dateCreation;
    @Field("by_json_file")
    private Boolean byJsonFile;
    @Field("user_creation")
    private String userCreation;
    @Field("user_update")
    private String userUpdate;
    @Field("date_update")
    private LocalDateTime dateUpdate;
    @Field("store_id")
    private Long storeId;
    @Field("structure_id")
    private Long structureId;
    @Field("module_id")
    private Long moduleId;

}
