package com.wimsaas.wimapp.wimsite.dao.repositories;

import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteMenu;
import org.bson.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface WbsWebsiteMenuRepository extends MongoRepository<WbsWebsiteMenu,Long> {
    @Query(" ?0 ")
    List<WbsWebsiteMenu> findByQuery(Document query);

    List<WbsWebsiteMenu> findByWebsiteId(Long websiteId);
    List<WbsWebsiteMenu> findByWebsitePageId(Long websitePageId);

}


