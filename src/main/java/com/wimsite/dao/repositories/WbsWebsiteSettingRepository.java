package com.wimsaas.wimapp.wimsite.dao.repositories;

import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteSetting;
import org.bson.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WbsWebsiteSettingRepository extends MongoRepository<WbsWebsiteSetting,Long> {
    @Query(" ?0 ")
    List<WbsWebsiteSetting> findByQuery(Document query);
}
