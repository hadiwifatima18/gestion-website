package com.wimsaas.wimapp.wimsite.dao.repositories;

import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsite;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteDto;
import org.bson.Document;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface WbsWebsiteRepository extends MongoRepository<WbsWebsite,Long> {
    @Query(" ?0 ")
    List<WbsWebsite> findByQuery(Document query);

    long count();
}






















