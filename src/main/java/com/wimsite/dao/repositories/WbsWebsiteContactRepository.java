package com.wimsaas.wimapp.wimsite.dao.repositories;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteContact;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsitePage;
import org.bson.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface WbsWebsiteContactRepository extends MongoRepository<WbsWebsiteContact,Long>{
    @Query(" ?0 ")
    List<WbsWebsiteContact> findByQuery(Document query);
    List<WbsWebsiteContact> findByWebsiteId(Long websiteId);
}



