package com.wimsaas.wimapp.wimsite.dao.repositories;

import com.wimsaas.wimapp.wimsite.dao.entities.WbsSection;
import org.bson.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface WbsSectionRepository extends MongoRepository<WbsSection,Long>{
    @Query(" ?0 ")
    List<WbsSection> findByQuery(Document query);
}


