package com.wimsaas.wimapp.wimsite.dao.repositories;

import com.wimsaas.wimapp.wimsite.dao.entities.WbsSetting;
import org.bson.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WbsSettingRepository extends MongoRepository<WbsSetting,Long> {
    @Query(" ?0 ")
    List<WbsSetting> findByQuery(Document query);
}
