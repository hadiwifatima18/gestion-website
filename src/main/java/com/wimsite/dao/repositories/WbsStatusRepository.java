package com.wimsaas.wimapp.wimsite.dao.repositories;

import com.wimsaas.wimapp.wimsite.dao.entities.WbsStatus;
import org.bson.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WbsStatusRepository extends MongoRepository<WbsStatus,Long> {
    @Query(" ?0 ")
    List<WbsStatus> findByQuery(Document query);
}


