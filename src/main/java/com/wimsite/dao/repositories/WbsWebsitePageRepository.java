package com.wimsaas.wimapp.wimsite.dao.repositories;


import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteMenu;
import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsitePage;
import org.bson.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WbsWebsitePageRepository extends MongoRepository<WbsWebsitePage,Long> {
    @Query(" ?0 ")
    List<WbsWebsitePage> findByQuery(Document query);
    WbsWebsitePage findByPathAndWebsiteId(String path,Long websiteId);

}