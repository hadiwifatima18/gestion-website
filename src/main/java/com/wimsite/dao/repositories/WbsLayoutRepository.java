package com.wimsaas.wimapp.wimsite.dao.repositories;

import com.wimsaas.wimapp.wimsite.dao.entities.WbsLayout;
import org.bson.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface WbsLayoutRepository extends MongoRepository<WbsLayout,Long> {
    @Query(" ?0 ")
    Collection<WbsLayout> findByQuery(Document queryObject);
    int countBySectionId(Long sectionId);
}
