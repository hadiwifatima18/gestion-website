package com.wimsaas.wimapp.wimsite.dao.repositories;

import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsite;
import com.wimsaas.wimapp.wimsite.services.dto.WbsWebsiteDto;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface StatisticsRepository extends MongoRepository<WbsWebsite,Long> {

    @Aggregation(pipeline = {
            "{$match: { date_creation: { $gte: ?0, $lte: ?1 } ,'structure_id': ?2}}",
            "{$lookup: { from: 'wbs_statut', localField: 'status_id', foreignField: '_id', as: 'websiteInfo' }}",
            // "{$unwind: '$websiteInfo'}",
            "{$group: { _id: '$status_id', websiteCount: { $sum: 1 }}}",
            "{$project: { statusId: '$_id', websiteCount: 1 }}"
    })
    List<WbsWebsiteDto> nbrOfWebsiteForEachStatus(LocalDateTime startDate, LocalDateTime endDate, Long structureId);

    @Aggregation(pipeline = {
            "{$match: { date_creation: { $gte: ?0, $lte: ?1 }}}",
            "{$lookup: { from: 'wbs_statut', localField: 'status_id', foreignField: '_id', as: 'websiteInfo' }}",
            // "{$unwind: '$websiteInfo'}",
            "{$group: { _id: '$status_id', websiteCount: { $sum: 1 }}}",
            "{$project: { statusId: '$_id', websiteCount: 1 }}"
    })
    List<WbsWebsiteDto> nbrOfWebsiteWithoutStructureIdForEachStatus(LocalDateTime startDate, LocalDateTime endDate);

    @Aggregation(pipeline = {
            "{$match: { date_creation: { $gte: ?0, $lte: ?1 }, structure_id: ?2 }}",
            "{$group: { _id: '$domain_type', websiteCount: { $sum: 1 }}}",
            "{$project: { domainType: '$_id', websiteCount: 1, _id: 1}}"
    })
    List<WbsWebsiteDto> nbrOfWebsiteForEachDomainType(LocalDateTime startDate, LocalDateTime endDate, Long structureId);

    @Aggregation(pipeline = {
            "{$match: { date_creation: { $gte: ?0, $lte: ?1 } }}",
            "{$group: { _id: '$domain_type', websiteCount: { $sum: 1 }}}",
            "{$project: { domainType: '$_id', websiteCount: 1, _id: 1}}"
    })
    List<WbsWebsiteDto> nbrOfWebsiteWithoutStructureIdForEachDomainType(LocalDateTime startDate, LocalDateTime endDate);
    @Aggregation(pipeline = {
            "{ $match: { date_creation: { $gte:?0, $lte:?1 } ,'structure_id': ?2} }",
            "{ $group: { _id: { $month: '$date_creation' }, websiteCount: { $sum: 1 }}}",
            "{ $sort: { '_id': 1 } }",
            "{ $project: { month: '$_id', websiteCount: 1, '_id': 0 } }"
    })
    List<WbsWebsiteDto> nbrOfWebsiteByMonths(LocalDateTime startDate, LocalDateTime endDate,Long structureId);

    @Aggregation(pipeline = {
            "{ $match: { date_creation: { $gte:?0, $lte:?1 } } }",
            "{ $group: { _id: { $month: '$date_creation' }, websiteCount: { $sum: 1 }}}",
            "{ $sort: { '_id': 1 } }",
            "{ $project: { month: '$_id', websiteCount: 1, '_id': 0 } }"
    })
    List<WbsWebsiteDto> nbrOfWebsiteWithoutStructureIdByMonths(LocalDateTime startDate, LocalDateTime endDate);

    @Aggregation(pipeline = {
            "{ $match: { date_creation: { $gte:?0, $lte:?1 } ,'structure_id': ?2} }",
            "{ $group: { _id: {  day: { $dayOfMonth: '$date_creation' } }, websiteCount: { $sum: 1 } } }",
            "{ $sort: {  '_id.day': 1 } }",
            "{ $project: {  day: '$_id.day', websiteCount: 1, '_id': 0 } }"
    })
    List<WbsWebsiteDto> nbrOfWebsiteByDays(LocalDateTime startDate, LocalDateTime endDate,Long structureId);

    @Aggregation(pipeline = {
            "{ $match: { date_creation: { $gte:?0, $lte:?1 }} }",
            "{ $group: { _id: {  day: { $dayOfMonth: '$date_creation' } }, websiteCount: { $sum: 1 } } }",
            "{ $sort: {  '_id.day': 1 } }",
            "{ $project: {  day: '$_id.day', websiteCount: 1, '_id': 0 } }"
    })
    List<WbsWebsiteDto> nbrOfWebsiteWithoutStructureIdByDays(LocalDateTime startDate, LocalDateTime endDate);
}

