package com.wimsaas.wimapp.wimsite.dao.repositories;

import com.wimsaas.wimapp.wimsite.dao.entities.WbsWebsiteSection;
import org.bson.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface WbsWebsiteSectionRepository extends MongoRepository<WbsWebsiteSection,Long> {
    @Query(" ?0 ")
    List<WbsWebsiteSection> findByQuery(Document query);
    WbsWebsiteSection findByHash(String hash);
    long count();
}
