package com.wimsaas.wimapp.wimsite.utils;

import com.wimsaas.starter.exceptions.business.WimBaseException;
import org.apache.commons.validator.routines.DomainValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WimSiteValidator {

   public static boolean isValidSubDomainName(String subDomainName) {
       if (subDomainName == null || subDomainName.trim().isEmpty()) {
           return true;
       }
       String subDomainRegex = "^[a-zA-Z0-9]+$";
       return subDomainName.matches(subDomainRegex);
   }
   public static boolean isValidDomainName(String domainName) {
        DomainValidator domainValidator = DomainValidator.getInstance();
        return domainValidator.isValid(domainName);
   }
    public static boolean isValidEmailAddress(String emailAddress) {

        String emailRegex = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$";
        return emailAddress != null && emailAddress.matches(emailRegex);
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        String phoneRegex = "^212[0-9]{9}";
        return phoneNumber != null && phoneNumber.matches(phoneRegex);
    }
}
