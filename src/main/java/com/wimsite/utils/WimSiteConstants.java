package com.wimsaas.wimapp.wimsite.utils;

public class WimSiteConstants {

    /*  #########################  WBS_STATUS  #########################  */

    public static final Long WBS_STATUS_PREPARE_ID = 1L;
    public static final Long WBS_STATUS_ACTIVE_ID = 2L;
    public static final Long WBS_STATUS_MAINTENANCE_ID = 3L;
    public static final Long WBS_STATUS_DESACTIVE_ID = 4L;
    public static final Long WBS_STATUS_SUPPRIMEE_ID = 5L;

    public static final String SPACE_WIMSTE_PREFIX = "wimsite";

}
