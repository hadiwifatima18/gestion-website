package com.wimsaas.wimapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
@ComponentScan({
        "com.wimsaas.wimapp",
        "com.wimsaas.security",
        "com.wimsaas.starter"
})
//@EnableFeignClients
public class WimAppServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(WimAppServiceApplication.class, args);
    }
}